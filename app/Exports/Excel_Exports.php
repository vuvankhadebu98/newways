<?php

namespace App\Exports;

use App\Model\Product;
use Maatwebsite\Excel\Concerns\FromCollection;

class Excel_Exports implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Product::all();
    }
}
