<?php

namespace App\Http\Controllers\admin\brand;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\brand;
use Validate;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    // tìm kiếm.............................................................................
    public function search_brand(Request $request){
        $all_brand = brand::where('name','like','%'.$request->key.'%')->get();
        return view('admin.brand.search_brand',compact('all_brand'));
    }
	// add_brand........................................................................
    public function add_brand(){
    	return view('admin.brand.add_brand');
    }
    // all_brand........................................................................
    public function all_brand(){
    	$all_brand  = brand::paginate(5); 
        $manager_brand = view('admin.brand.all_brand')->with('all_brand', $all_brand);
        return view('admin.admin_newways')->with('admin.brand.all_brand',$manager_brand);
    }
    // edit_categogry............................................................................
    public function edit_brand($id){
        $edit_brand = DB::table('brands')->where('id',$id)->get();
        $manager_brand = view('admin.brand.edit_brand')->with('edit_brand', $edit_brand);
        return view('admin.admin_newways')->with('admin.brand.edit_brand',$manager_brand);      
    }
    //unactive_brand-------------------------------------------------------
    public function unactive_brand($id){
        DB::table('brands')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt blog thành công !');
        return Redirect::to('all-brand');
    }
    //active_brand..........................................................
    public function active_brand($id){
        DB::table('brands')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt blog thành công !');
        return Redirect::to('all-brand');
    }
    // save_brand...........................................................
    public function save_brand(Request $request){
        $this->validate($request, [
            'name'=>'required|min:2',
            'img'=>'required',
            'meta_keywords'=>'required',
            'meta_desc'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên thương hiệu',
             'img.required'=>'bạn chưa chọn ảnh',
             'name.min'=>'Mật khẩu phải lớn hơn 2 kí tự', 
             'meta_keywords.required'=>'Bạn chưa nhập mã thương hiệu',  
             'meta_desc.required'=>'Bạn chưa nhập mô tả thương hiệu',
            ]
        );
        $brand = new brand;
        $brand->name   = $request->name;
        $brand->meta_keywords   = $request->meta_keywords;
        $brand->meta_desc   = $request->meta_desc;
        $brand->status  = $request->status;
        $get_image = $request->file('img');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_brand',$new_image);
            $brand->img = $new_image;
        }else{
            $brand->img = '';
        }
        $brand->save();
        Session::put('message','thêm danh mục thành công');
        return Redirect::to('add-brand');
    }

    // update_tag..........................................................
    public function update_brand(Request $request,$id){
        $brand = brand::find($id);
        $brand->name   = $request->name;
        $brand->meta_keywords   = $request->meta_keywords;
        $brand->meta_desc   = $request->meta_desc;
        $brand->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_brand',$new_image);
            $brand->img = $new_image;
        }
        $brand->update();
        Session::put('message','cập nhật danh mục sản phẩm thành công');
        return Redirect::to('all-brand');
    }
     // delete_tag.........................................................
    public function delete_brand($id){
        DB::table('brands')->where('id',$id)->delete();
        Session::put('message','xóa danh mục thành công !');
        return Redirect::to('all-brand');
    }
}
