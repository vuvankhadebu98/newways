<?php

namespace App\Http\Controllers\admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Category;
use Validate;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    // tìm kiếm.............................................................................
    public function search_category(Request $request){
        $all_category = Category::where('name','like','%'.$request->key.'%')->get();
        return view('admin.category.search_category',compact('all_category'));
    }
	// add_category........................................................................
    public function add_category(){
    	return view('admin.category.add_category');
    }
    // all_category........................................................................
    public function all_category(){
    	$all_category  = Category::paginate(5); 
        $manager_category = view('admin.category.all_category')->with('all_category', $all_category);
        return view('admin.admin_newways')->with('admin.category.all_category',$manager_category);
    }
    // edit_categogry............................................................................
    public function edit_category($id){
        $edit_category = DB::table('categorys')->where('id',$id)->get();
        $manager_category = view('admin.category.edit_category')->with('edit_category', $edit_category);
        return view('admin.admin_newways')->with('admin.category.edit_category',$manager_category);      
    }
    //unactive_category-------------------------------------------------------
    public function unactive_category($id){
        DB::table('categorys')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt blog thành công !');
        return Redirect::to('all-category');
    }
    //active_category..........................................................
    public function active_category($id){
        DB::table('categorys')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt blog thành công !');
        return Redirect::to('all-category');
    }
    // save_category...........................................................
    public function save_category(Request $request){
        $this->validate($request, [
            'name'=>'required|min:2',
            'img'=>'required',
            'meta_keywords'=>'required',
            'meta_desc'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên tag',
             'img.required'=>'bạn chưa chọn ảnh',
             'name.min'=>'Mật khẩu phải lớn hơn 2 kí tự',
             'meta_keywords.required'=>'Bạn chưa nhập mã danh mục',  
             'meta_desc.required'=>'Bạn chưa nhập mô tả danh mục',   
            ]
        );
        $category = new category;
        $category->name   = $request->name;
        $category->meta_keywords   = $request->meta_keywords;
        $category->meta_desc   = $request->meta_desc;
        $category->status  = $request->status;
        $get_image = $request->file('img');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_category',$new_image);
            $category->img = $new_image;
        }else{
            $category->img = '';
        }
        $category->save();
        Session::put('message','thêm danh mục thành công');
        return Redirect::to('add-category');
    }

    // update_tag..........................................................
    public function update_category(Request $request,$id){
        $category = Category::find($id);
        $category->name   = $request->name;
        $category->meta_keywords   = $request->meta_keywords;
        $category->meta_desc   = $request->meta_desc;
        $category->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_category',$new_image);
            $category->img = $new_image;
        }
        $category->update();
        Session::put('message','cập nhật danh mục sản phẩm thành công');
        return Redirect::to('all-category');
    }
     // delete_tag.........................................................
    public function delete_category($id){
        DB::table('categorys')->where('id',$id)->delete();
        Session::put('message','xóa danh mục thành công !');
        return Redirect::to('all-category');
    }


}
