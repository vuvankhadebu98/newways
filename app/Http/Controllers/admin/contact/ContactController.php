<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Contact;
use Validate;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    // list_contact..........................................................................
    public function list_contact(){
    	$all_contact  = Contact::paginate(5); 
        return view('admin.contact.list_contact')->with(compact('all_contact'));
    }
    // lọc liên hệ đã xử lý.............................................................
    public function select_active_contact(){
        $all_contact = Contact::where('status',1)->get();
        return view('admin.contact.ajax_contact.ajax_contact_active',compact('all_contact'));
    }
    // lọc liên hệ chưa xử lý.............................................................
    public function select_unactive_contact(){
        $all_contact = Contact::where('status',0)->get();
        return view('admin.contact.ajax_contact.ajax_contact_unactive',compact('all_contact'));
    }
    // tìm kiếm.............................................................................
    public function search_contact(Request $request){
        $all_contact = contact::where('name','like','%'.$request->key.'%')->get();
        return view('admin.contact.search_contact',compact('all_contact'));
    }
    //unactive_product-------------------------------------------------------....
    public function unactive_contact($id){
        DB::table('contacts')->where('id', $id)->update(['status'=>1]);
        Session::put('message','liên hệ xử lý thành công !');
        return Redirect::to('list-contact');
    }
    //active_product..........................................................
    public function active_contact($id){
        DB::table('contacts')->where('id', $id)->update(['status'=>0]);
        Session::put('message','chưa xử lý liên hệ thành công !');
        return Redirect::to('list-contact');
    }
    // xóa liên hệ khi đã được xử lý
    public function delete_contact($id){
        DB::table('contacts')->where('id',$id)->delete();
        Session::put('message','xóa liên hệ thành công !');
        return Redirect::to('list-contact');
    }
}
