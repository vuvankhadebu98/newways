<?php

namespace App\Http\Controllers\admin\coupon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Coupon;
use Validate;
use Illuminate\Support\Facades\Auth;

class CouponController extends Controller
{
	// add_coupon..........................................................................
    public function add_coupon(){
    	return view('admin.coupon.add_coupon');
    }
    // all_coupon..........................................................................
    public function all_coupon(){
    	$all_coupon  = Coupon::paginate(5); 
        return view('admin.coupon.all_coupon')->with(compact('all_coupon'));
    }
    // edit_categogry............................................................................
    public function edit_coupon($id){
        $edit_coupon = DB::table('coupons')->where('id',$id)->get();
        return view('admin.coupon.edit_coupon')->with(compact('edit_coupon'));      
    }
    // tìm kiếm.............................................................................
    public function search_coupon(Request $request){
        $all_coupon = Coupon::where('name','like','%'.$request->key.'%')->get();
        return view('admin.coupon.search_coupon',compact('all_coupon'));
    }
    //unactive_brand-------------------------------------------------------
    public function unactive_coupon($id){
        DB::table('coupons')->where('id', $id)->update(['status'=>2]);
        Session::put('message','kích hoạt mã giảm giá thành công !');
        return Redirect::to('all-coupon');
    }
    //active_brand..........................................................
    public function active_coupon($id){
        DB::table('coupons')->where('id', $id)->update(['status'=>1]);
        Session::put('message','không kích hoạt mã giảm giá thành công !');
        return Redirect::to('all-coupon');
    }
    // save_coupon.......................................................
    public function save_coupon(Request $request){
        $this->validate($request, [
            'name'=>'required|min:2|max:100',
            'code'=>'required|min:3|max:20',
            'qty'=>'required|max:5',
            'check'=>'required',
            'number'=>'required|max:15',

        ], [
             'name.required'=>'Bạn chưa nhập tên mã giảm giá',
             'code.required'=>'bạn chưa nhập mã giảm giá',
             'qty.required'=>'bạn chưa nhập số lượng mã',
             'check.required'=>'bạn chưa lựa chọn kiểu mã giảm giá',
             'number.required'=>"bạn chưa nhập số hoặc phần trăm mã giảm giá",
             'name.min'=>'Mật khẩu phải lớn hơn 2 kí tự',
             'name.max'=>'không thể nhập quá 100 ký tự !',
             'code.min'=>'bạn phải nhập ít nhất 3 ký tự',
             'code.max'=>'bạn không được nhập quá 20 ký tự',
             'qty.max'=>'Bạn không được nhập quá 5 ký tự',
             'number.max'=>'bạn khộng được nhập quá 15 ký tự',
            ]
        );
    	$data = $request->all();
    	$coupon = new coupon;
        $coupon->name = $data['name'];
        $coupon->code = $data['code'];
        $coupon->qty = $data['qty'];
        $coupon->check = $data['check'];
        $coupon->number = $data['number'];
        $coupon->save();

        Session::put('message','thêm mã giảm giá thành công');
        return Redirect::to('add-coupon');
    }
    //update_coupon...........................................................
    public function update_coupon(Request $request,$id){
        $coupon = coupon::find($id);
        $coupon->name = $request->name;
        $coupon->code = $request->code;
        $coupon->qty = $request->qty;
        $coupon->check = $request->check;
        $coupon->number = $request->number;
        $coupon->update();

        Session::put('message','cập nhật mã giảm giá thành công');
        return Redirect::to('all-coupon');
    }
    // delete_coupon.........................................................
    public function delete_coupon($id){
        DB::table('coupons')->where('id',$id)->delete();
        Session::put('message','xóa mã giảm giá thành công !');
        return Redirect::to('all-coupon');
    }
}
