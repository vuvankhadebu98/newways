<?php

namespace App\Http\Controllers\admin\delivery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\City;
use App\Model\Province;
use App\Model\Wards;
use App\Model\Feeship;

class DeliveryController extends Controller
{
	// add_delivery................................................................
	public function add_delivery(Request $request){
		$city = City::orderby('matp','ASC')->get();
		return view('admin.delivery.add_delivery')->with(compact('city'));
	}

    // save_delivery(ajax)..............................................................
    public function save_delivery(Request $request){
        $data = $request->all();
        $feeship = new Feeship();
        $feeship->matp = $data['city'];
        $feeship->maqh = $data['province'];
        $feeship->xaid = $data['wards'];
        $feeship->feeship = $data['feeship'];
        $feeship->save();
    }

    // update_delivery(ajax)..........................................................................
    public function update_delivery(Request $request){
        $data = $request->all();
        $feeship = Feeship::find($data['feeship_id']);
        $feeship_price = rtrim($data['feeship_price'],'.');
        $feeship->feeship = $feeship_price;
        $feeship->save();
    }

    // select_feeship(ajax).....................................................................
    public function select_feeship(){
        $feeship = Feeship::orderby('id','DESC')->get();
        $output = '';
        $output .= '<div class="table-responsive"> 
            <table class="table table-bordered">
                <thread>
                    <tr>
                        <th>Tên Thành Phố</th>
                        <th>Tên Quận Huyện</th>
                        <th>Tên Xã Phường</th>
                        <th>Phí Ship</th>
                    </tr>
                </thread>
                <tbody>
                ';
                foreach($feeship as $key => $fee){
                $output .='
                    <tr>
                        <td>'.$fee->city->name.'</td>
                        <td>'.$fee->province->name.'</td>
                        <td>'.$fee->wards->name.'</td>
                        <td contenteditable data-feeship_id="'.$fee->id.'" class="fee_feeship_edit">'.number_format($fee->feeship,0,',','.').'</td>
                    </tr>
                    ';
                }
                $output .='
                </tbody>
            </table>
        </div>
        ';    

        echo $output;
    
    }

    // select_delivery(ajax)...........................................................
    public function select_delivery(Request $request){
    	$data = $request->all();
    	if ($data['action']){
    		$output = '';
    		if ($data['action']=='city'){
	    		$select_province = Province::where('matp',$data['ma_id'])->orderby('maqh','ASC')->get();
	    		$output .= '<option>---Chọn Quận Huyện---</option>'; 
	    		foreach($select_province as $key => $prov){
	    			$output .='<option value="'.$prov->maqh.'">'.$prov->name.'</option>';
	    		}
    		}
    		else
    		{
    			$select_wards = Wards::where('maqh',$data['ma_id'])->orderby('xaid','ASC')->get();
    			$output .= '<option>---Chọn Xã Phường---</option>'; 
	    		foreach($select_wards as $key => $ward){
	    			$output .='<option value="'.$ward->xaid.'">'.$ward->name.'</option>';
	    		}
    		}
    	}
    	return $output;
    }
}
