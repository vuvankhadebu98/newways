<?php

namespace App\Http\Controllers\admin\news;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\News;
use Validate;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
     // tìm kiếm....................................
    public function search_news(Request $request){
        $all_news = news::where('author','like','%'.$request->key.'%')
        ->orWhere('name','like','%'.$request->key.'%')->get();

        return view('admin.news.search_news',compact('all_news'));
    }
    // // tim kiem danh muc bang ajax..........................................
    // public function load_brand_news(Request $request){
    //     $user = DB::table('users')->orderby('id','desc')->get();
    //     $all_news = news::where('brand_id',$request->get('value'))->get();
    //     return view('admin.news.ajax_news.ajax_brand_news',compact('all_news','user'));
    // }
    // add_news........................................................
    public function add_news(){
        return view('admin.news.add_news');
	}

    //all_news.............................................................
    public function all_news(){
        $all_news  = News::paginate(5);
        return view('admin.news.all_news',compact('all_news'));
    }

    //edit_news............................................................
    public function edit_news($id){
        $edit_news = DB::table('news')->where('id',$id)->get();
        return view('admin.news.edit_news',compact('edit_news'));      
    }

    //unactive_news-------------------------------------------------------....
    public function unactive_news($id){
        DB::table('news')->where('id', $id)->update(['status'=>1]);
        Session::put('message','hiển thị tin tức thành công !');
        return Redirect::to('all-news');
    }
    //active_news..........................................................
    public function active_news($id){
        DB::table('news')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không hiển thị tin tức thành công !');
        return Redirect::to('all-news');
    }
    
    // save_blog...........................................................
    public function save_news(Request $request){

        $this->validate($request, [
            'author'=>'required',
            'name'=>'required|min:3|max:100',
            'img'=>'required',
            'desc'=>'required',
            'content'=>'required', 
        ], [
             'author.required'=>'Bạn chưa nhập tên người đăng',
             'name.required'=>'Bạn chưa nhập tên sản phẩm',
             'name.min'=>'Tên phải ít nhất 3 ký tự',
             'name.max'=>'Tên không được quá 100 ký tự',
             'img.required'=>'Bạn chưa chọn ảnh tin tức',
             'desc.required'=>'Bạn chưa nhập mô tả tin tức',
             'content.required'=>'Bạn chưa nhập nội dung tin tức',
            ]
        );

        $news = new news;
        $news->author   = $request->author;
        $news->name   = $request->name;
        $news->desc  = $request->desc;
        $news->content  = $request->content;
        $news->status   = $request->status;      
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_news',$new_image);
            $news->img = $new_image;
        }else{
            $news->img = '';
        }
        $news->save();
        Session::put('message','Thêm tin tức thành công');
        return Redirect::to('add-news');
    }

    // update_prudct..........................................................
    public function update_news(Request $request,$id){ 
        $this->validate($request, [
            'author'=>'required',
            'name'=>'required|min:3|max:100',
            'desc'=>'required',
            'content'=>'required', 
        ], [
             'author.required'=>'Bạn chưa nhập tên người đăng',
             'name.required'=>'Bạn chưa nhập tên sản phẩm',
             'name.min'=>'Tên phải ít nhất 3 ký tự',
             'name.max'=>'Tên không được quá 100 ký tự',
             'desc.required'=>'Bạn chưa nhập mô tả tin tức',
             'content.required'=>'Bạn chưa nhập nội dung tin tức',
            ]
        );
        $news = news::find($id);
        $news->author   = $request->author;
        $news->name   = $request->name;
        $news->desc  = $request->desc;
        $news->content  = $request->content;
        $news->status   = $request->status;      
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_news',$new_image);
            $news->img = $new_image;
        }
        $news->update();
        Session::put('message','Cập tin tức thành công');
        return Redirect::to('all-news');
    }
     // delete_news.........................................................
    public function delete_news($id){
        DB::table('newss')->where('id',$id)->delete();
        Session::put('message','xóa tin tứcthành công !');
        return Redirect::to('all-news');
    }
}
