<?php

namespace App\Http\Controllers\admin\order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use Validate;
use Illuminate\Support\Facades\Auth;
use App\Model\Feeship;
use App\Model\Shipping;
use App\Model\Order;
use App\Model\Order_Detail;
use App\Model\Customer;
use App\Model\Coupon;
use App\Model\Product;
use PDF;

class OrderController extends Controller
{

	// view_order.............................................................................
    public function view_order(){
    	$all_order  = Order::orderby('id','desc')->paginate(5);
        // ->orderby('created_at','desc')->get()
        return view('admin.order.view_order')->with(compact('all_order'));
    }
    // lọc order bằng ajax active
    public function select_active_order(Request $request){
        $all_order = Order::where('status',2)->get();
        return view('admin.order.ajax_order.ajax_order_active',compact('all_order'));
    }
    // lọc order bằng ajax unactive
    public function select_unactive_order(Request $request){
        $all_order = Order::where('status',1)->get();
        return view('admin.order.ajax_order.ajax_order_unactive',compact('all_order'));
    }
    // lọc order bằng ajax no_active
    public function select_no_active_order(Request $request){
        $all_order = Order::where('status',3)->get();
        return view('admin.order.ajax_order.ajax_order_no_active',compact('all_order'));
    }
    // tìm kiếm.............................................................................
    public function search_order(Request $request){
        $all_order = Order::where('code',$request->key)->get();
        return view('admin.order.search_order',compact('all_order'));
    }
    // show_order..........................................................
    public function show_order($code){
        $order_detail = Order_Detail::with('product')->where('order_code',$code)->get();
        $order = Order::where('code',$code)->get();
        foreach ($order as $key => $ord) {
            $customer_id = $ord->customer_id;
            $shipping_id = $ord->shipping_id;
            $order_status = $ord->status;
        }
        $customer = Customer::where('id',$customer_id)->first();
        $shipping = Shipping::where('id',$shipping_id)->first();

        $order_details = Order_Detail::with('product')->where('order_code',$code)->get();
        foreach ($order_details as $key => $order_d) {
            $product_coupon = $order_d->product_coupon;
        }
        if ($product_coupon!='Không có mã giảm giá') {
            $coupon = Coupon::where('code',$product_coupon)->first();
            $coupon_check = $coupon->check;
            $coupon_number = $coupon->number;
        }else{
            $coupon_check = 2 ;
            $coupon_number = 0;
        }
        return view('admin.order.show_order')->with(compact('order_detail','customer','shipping','order_details',
        'coupon_check','coupon_number','order','order_status'));
    }

    // in đơn hàng(print_order)
    public function print_order($checkout_code){
        $pdf = \App::make('dompdf.wrapper');
        $pdf-> loadHTML($this->print_order_convert($checkout_code));
        return $pdf->stream();
    }
    public function print_order_convert($checkout_code){
        $order_detail = Order_Detail::where('order_code',$checkout_code)->get();
        $order = Order::where('code',$checkout_code)->get();
        foreach ($order as $key => $ord) {
            $customer_id = $ord->customer_id;
            $shipping_id = $ord->shipping_id;
        }
        $customer = Customer::where('id',$customer_id)->first();
        $shipping = Shipping::where('id',$shipping_id)->first(); 
        
        $order_details = Order_Detail::with('product')->where('order_code',$checkout_code)->get();

        foreach ($order_details as $key => $order_d) {
            $product_coupon = $order_d->product_coupon;
        }
        if ($product_coupon!='Không có mã giảm giá') {
            $coupon = Coupon::where('code',$product_coupon)->first();
            $coupon_check = $coupon->check;
            $coupon_number = $coupon->number;
            if ($coupon_check ==1) {
                $coupon_echo = $coupon_number.'%';
            }elseif($coupon_check==2){
                $coupon_echo = number_format($coupon_number,0,',','.').' đ';
            }
        }else{
            $coupon_check = 2 ;
            $coupon_number = 0;
            $coupon_echo = '0';
        }

        $output = '';

        $output.= '<style>body{
            font-family:DejaVu Sans;
        }
        .table-styling{
            border:1px solid #000;
        }
        .table-styling tbody tr td{
            border:1px solid #000;
            text-align: center;
        }
        </style>
        <h3><center>Công ty TNHH một thành viên Newways</center></h3>
        <h4><center>Độc lập - Tự do - Hạnh phúc</center></h4>

        <h4>Người đặt hàng</h4>
        <table class="table-styling">
            <thead>  
                <tr>
                    <th width="200px">Tên khách hàng</th>
                    <th width="200px">Số điện thoại</th>
                    <th width="290px">Email</th>
                </tr>
            </thead>
            <tbody>';
        $output.= '
                <tr>
                    <td>'.$customer->name.'</td>
                    <td>'.$customer->phone.'</td>
                    <td>'.$customer->email.'</td>
                </tr>';
        $output.= '
            </tbody>            
        </table>

        <h4>Người nhận hàng</h4>
        <table class="table-styling">
            <thead>  
                <tr>
                    <th>Tên người nhận</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>';
        $output.= '
                <tr>
                    <td>'.$shipping->name.'</td>
                    <td>'.$shipping->address.'</td>
                    <td>'.$shipping->phone.'</td>
                    <td>'.$shipping->email.'</td>
                </tr>';
        $output.= '
            </tbody>            
        </table>

        <h4>Đơn Hàng</h4>
        <table class="table-styling">
            <thead>  
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Mã giảm giá</th>
                    <th>Phí vận chuyển</th>
                    <th>Số lượng</th>
                    <th>Giá sản phẩm</th>
                    <th>Thành tiền</th>
                </tr>
            </thead>
            <tbody>';
            $total = 0;
            foreach($order_details as $key => $product_or){
                $subtotal = $product_or->product_price*$product_or->product_qty;
                $total += $subtotal;
                if ($product_or->product_coupon!='Không có mã giảm giá') {
                    $product_coupon = $product_or->product_coupon;
                }else{
                    $product_coupon = 'không có mã';
                }
        $output.= '
                <tr>
                    <td>'.$product_or->product_name.'</td>
                    <td>'.$product_coupon.'</td>
                    <td>'.number_format($product_or->product_feeship,0,',','.').' đ'.'</td>
                    <td>'.$product_or->product_qty.'</td>
                    <td>'.number_format($product_or->product_price,0,',','.').' đ'.'</td>
                    <td>'.number_format($subtotal,0,',','.').' đ'.'</td>
                </tr>';
            }
            if($coupon_check == 1){
                $total_after_coupon = ($total*$coupon_number)/100;
                $total_coupon = $total - $total_after_coupon;
            }else{
                $total_coupon = $total - $coupon_number;
            }
        $output.= '<tr>
            <td colspan= 6>
                <p>Tổng giảm:'.$coupon_echo.'</p>
                <p>phí vận chuyển: '.number_format($product_or->product_feeship,0,',','.').' đ'.'</p>
                <p>Thanh toán: '.number_format($total_coupon + $product_or->product_feeship,0,',','.').' đ'.'</p>
            </td>
        </tr>';    
        $output.= '
            </tbody>            
        </table>

        <h4></h4>
        <table>
            <thead>  
                <tr>
                    <th width="200px">Người vận chuyển</th>
                    <th width="800px">Người nhận</th>
                </tr>
            </thead>
            <tbody>';
        $output.= '
            </tbody>            
        </table>
        ';
        return $output;
    }

    //update_order_qty.............................................................................
    public function update_order_qty(Request $request){
        // update_order
        $data = $request->all();
        $order = Order::find($data['order_id']);
        $order->status = $data['order_status'];
        $order->save();
        if($order->status==2){
            foreach ($data['order_product_id'] as $key => $product_id) {
                $product = Product::find($product_id);
                $product_quantity =  $product->qty;
                $product_sold = $product->sold;
                foreach ($data['quantity'] as $key2 => $quaty) {
                    if ($key == $key2) {
                        $pro_remain = $product_quantity - $quaty;
                        $product->qty = $pro_remain;
                        $product->sold = $product_sold + $quaty;
                        $product->save();
                    }
                }
            }
        }elseif ($order->status!=2 && $order->status!=3) {
            foreach ($data['order_product_id'] as $key => $product_id) {
                $product = Product::find($product_id);
                $product_quantity =  $product->qty;
                $product_sold = $product->sold;
                foreach ($data['quantity'] as $key2 => $quaty) {
                    if ($key == $key2) {
                        $pro_remain = $product_quantity + $quaty;
                        $product->qty = $pro_remain;
                        $product->sold = $product_sold - $quaty;
                        $product->save();
                    }
                }
            }
        }
    } 
    // update_qty........................................................................................
    public function update_qty(Request $request){
        $data = $request->all();
        $order_details = Order_Detail::where('product_id',$data['order_product_id'])
        ->where('order_code',$data['order_code'])->first();
        $order_details->product_qty = $data['order_qty'];
        $order_details->save();
    }
}
