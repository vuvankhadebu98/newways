<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Imports\Excel_Imports;
use App\Exports\Excel_Exports;
use Excel;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Product;
use App\Model\Admin;
use App\Model\Category;
use APP\Model\Brand;
use Validate;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
     // tìm kiếm....................................
    public function search_product(Request $request){
        $brand = DB::table('brands')->orderby('id','desc')->get();
        $category = DB::table('categorys')->orderby('id','desc')->get();
        $admin = DB::table('admin')->orderby('admin_id','desc')->get();
        $all_product = Product::where('name','like','%'.$request->key.'%')
        ->orWhere('price',$request->key)->orwhere('id',$request->key)
        ->get();

        return view('admin.product.search_product',compact('all_product','category','admin','brand'));
    }
    // tim kiem danh muc bang ajax..........................................
    public function load_brand_product(Request $request){
        $admin = DB::table('admin')->orderby('admin_id','desc')->get();
        $all_product = Product::where('brand_id',$request->get('value'))->get();
        return view('admin.product.ajax_product.ajax_brand_product',compact('all_product','admin'));
    }
    // add_product........................................................
    public function add_product(){
    	$admin = DB::table('admin')->orderby('admin_id','desc')->get();
        $category = DB::table('categorys')->orderby('id','desc')->get(); 
        return view('admin.product.add_product')->with('admin',$admin)
        ->with('category',$category);
	}

    //all_product.............................................................
    public function all_product(){
        $all_product  = Product::orderby('id','desc')->paginate(5);
        $admin = DB::table('admin')->orderby('admin_id','desc')->get();
        $brand = DB::table('brands')->orderby('id','desc')->get();
        $manager_product = view('admin.product.all_product')->with('all_product', $all_product)
        ->with('admin',$admin)->with('brand',$brand);

        return view('admin.admin_newways')->with('admin.product.all_product',$manager_product);
    }

    //edit_product............................................................
    public function edit_product($id){
        $admin = DB::table('admin')->orderby('admin_id','desc')->get();
        $category = DB::table('categorys')->orderby('id','desc')->get();
        $brand = DB::table('brands')->orderby('id','desc')->get();
        $edit_product = DB::table('products')->where('id',$id)->get();

        $manager_product = view('admin.product.edit_product')->with('edit_product', $edit_product)
        ->with('admin',$admin)->with('category',$category)->with('brand',$brand);
        return view('admin.admin_newways')->with('admin.product.edit_product',$manager_product);      
    }

    //unactive_product-------------------------------------------------------....
    public function unactive_product($id){
        DB::table('products')->where('id', $id)->update(['status'=>1]);
        Session::put('message','kích hoạt sản phẩm thành công !');
        return Redirect::to('all-product');
    }
    //active_product..........................................................
    public function active_product($id){
        DB::table('products')->where('id', $id)->update(['status'=>0]);
        Session::put('message','không kích hoạt sản phẩm thành công !');
        return Redirect::to('all-product');
    }
    
    // save_blog...........................................................
    public function save_product(Request $request){
        $this->validate($request, [
            'name'=>'required|min:3|max:100',
            'desc'=>'required',
            'content'=>'required',
            'price'=>'required|max:10',
            'img'=>'required',
            'qty'=>'required|max:3',
        ], [
             'name.required'=>'Bạn chưa nhập tên sản phẩm',
             'name.min'=>'Tên phải ít nhất 3 ký tự',
             'name.max'=>'Tên không được quá 100 ký tự',
             'desc.required'=>'Bạn chưa nhập mô tả sản phẩm',
             'content.required'=>'Bạn chưa nhập nội dung sản phẩm',
             'price.required'=>'Bạn chưa nhập giá sản phẩm',
             'price.max'=>'Bạn không được nhập quá 10 ký tự',
             'img.required'=>'Bạn chưa chọn ảnh',
             'qty.required'=>'Bạn chưa nhập số lượng sản phẩm',
             'qty.max'=>'Bạn chỉ được nhập tối thiểu là 3 ký tự',
            ]
        );
        $product = new product;
        $product->admin_id   = $request->admin_id;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->name   = $request->name;
        $product->desc  = $request->desc;
        $product->content  = $request->content;
        $product->price  = $request->price;
        $product->qty  = $request->qty;
        $product->check  = $request->check;
        $product->check_new  = $request->check_new;
        $product->status   = $request->status;      
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_product',$new_image);
            $product->img = $new_image;
        }else{
            $product->img = '';
        }
        $product->save();
        Session::put('message','Thêm sản phẩm thành công');
        return Redirect::to('add-product');
    }

    // update_prudct..........................................................
    public function update_product(Request $request,$id){ 
        $product = Product::find($id);
        $product->admin_id   = $request->admin_id;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->name   = $request->name;
        $product->desc  = $request->desc;
        $product->content  = $request->content;
        $product->price  = $request->price;
        $product->qty  = $request->qty;
        $product->check  = $request->check;
        $product->check_new  = $request->check_new;
        $product->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_product',$new_image);
            $product->img = $new_image;
        }
        $product->update();
        Session::put('message','Cập Nhật sản phẩm thành công');
        return Redirect::to('all-product');
    }
     // delete_product.........................................................
    public function delete_product($id){
        DB::table('products')->where('id',$id)->delete();
        Session::put('message','xóa sản phẩm thành công !');
        return Redirect::to('all-product');
    }
    // export_csv..................................................................
    public function export_csv(){
        return Excel::download(new Excel_Exports , 'product.xlsx');
    }
    // import_csv....................................................................
    public function import_csv(Request $request){
        $path = $request->file('file')->getRealPath();
        Excel::import(new Excel_Imports, $path);
        return back();

    }
}
