<?php

namespace App\Http\Controllers\admin\slider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Slider;
use Validate;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
	// tìm kiếm.............................................................................
    public function search_slider(Request $request){
        $all_slider = Slider::where('name','like','%'.$request->key.'%')->get();
        return view('admin.slider.search_slider',compact('all_slider'));
    }
    // add_slider.............................................................................
    public function add_slider(){
    	return view('admin.slider.add_slider');
    }
    // list_slider............................................................................
    public function list_slider()
    {
    	$all_slider = Slider::orderBy('id','DESC')->paginate(5);
    	return view('admin.slider.list_slider')->with(compact('all_slider'));
    }
    // edit_slider...........................................................................
    public function edit_slider($id){
        $edit_slider = Slider::where('id',$id)->get();
        return view('admin.slider.edit_slider')->with(compact('edit_slider'));      
    }
    //unactive_slider-------------------------------------------------------....
    public function unactive_slider($id){
        DB::table('slider')->where('id', $id)->update(['status'=>1]);
        Session::put('message','Hiển thị slider thành công !');
        return Redirect::to('list-slider');
    }
    //active_product..........................................................
    public function active_slider($id){
        DB::table('slider')->where('id', $id)->update(['status'=>0]);
        Session::put('message','Ẩn slider thành công !');
        return Redirect::to('list-slider');
    }
    // save_slider............................................................................
    public function save_slider(Request $request){
    	$this->validate($request, [
            'name'=>'required|min:2|Max:100',
            'img'=>'required',
            'desc'=>'required|max:150',
        ], [
             'name.required'=>'Bạn chưa nhập tên slider',
             'img.required'=>'bạn chưa chọn ảnh',
             'name.min'=>'Tên phải lớn hơn 2 kí tự',
             'name.max'=>'Bạn chỉ được nhập tối thiểu 100 ký tự',
             'desc.required'=>'Bạn chưa nhập mô tả slider',
             'desc.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
            ]
        );
        $slider = new Slider;
        $slider->name   = $request->name;
        $slider->desc   = $request->desc;
        $slider->status  = $request->status;
        $get_image = $request->file('img');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_slider',$new_image);
            $slider->img = $new_image;
        }else{
            $slider->img = '';
        }
        $slider->save();
        Session::put('message','Thêm Slider thành công');
        return Redirect::to('add-slider');
    }

    //update_slider...............................................................................
    public function update_slider(Request $request, $id){
    	$this->validate($request, [
            'name'=>'required|min:2|Max:100',
            'desc'=>'required|max:150',
        ], [
             'name.required'=>'Bạn chưa nhập tên slider',
             'name.min'=>'Tên phải lớn hơn 2 kí tự',
             'name.max'=>'Bạn chỉ được nhập tối thiểu 100 ký tự',
             'desc.required'=>'Bạn chưa nhập mô tả slider',
             'desc.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
            ]
        );
    	$slider = Slider::find($id);
        $slider->name   = $request->name;
        $slider->desc   = $request->desc;
        $slider->status   = $request->status;        
        $get_image = $request->file('img');
        
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_slider',$new_image);
            $slider->img = $new_image;
        }
        $slider->update();
        Session::put('message','Cập nhật Slider thành công');
        return Redirect::to('list-slider');
    } 
    // delete_slider.........................................................
    public function delete_slider($id){
        Slider::where('id',$id)->delete();
        Session::put('message','xóa Slider thành công !');
        return Redirect::to('list-slider');
    }
}
