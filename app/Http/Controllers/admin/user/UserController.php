<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Admin;
use App\Model\Login;//model login
use Validate;
use App\Model\Social; //sử dụng model Social
use Socialite; //sử dụng Socialite
use App\Rules\Captcha;
use Validator;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
     // tìm kiếm.............................................................................
    public function search_user(Request $request){
        $all_user = Admin::where('admin_name','like','%'.$request->key.'%')
        ->orWhere('admin_id',$request->key)->orWhere('admin_email',$request->key)->get();
        return view('admin.user.search_user',compact('all_user'));
    }
    // add_user..........................................................
    public function add_user(){
        return view('admin.user.add_user');
    }
    // all_user...........................................................
    public function all_user(){
        $all_user = Admin::orderby('admin_id','desc')->paginate(5);
        return view('admin.user.all_user')->with(compact('all_user'));
    }
    // edit_user.............................................................
    public function edit_user($admin_id){
        $edit_user = Admin::where('admin_id',$admin_id)->get();
        return view('admin.user.edit_user')->with(compact('edit_user'));
    }
    // save_category...........................................................
    public function save_user(Request $request){
        $this->validate($request, [
            'admin_name'=>'required|min:2|max:50',
            'admin_email'=>'required',
            'admin_password'=>'required|min:6|max:20',
            'admin_img'=>'required',
            'admin_phone'=>'required',
        ], [
             'admin_name.required'=>'Bạn chưa nhập tên tài khoản',
             'admin_name.min'=>'Tên phải lớn hơn 2 kí tự',
             'admin_name.max'=>'Tên chỉ được tối thiểu là 50 ký tự',
             'admin_email.required'=>'Bạn chưa nhập email',
             'admin_password.required'=>'Bạn chưa nhập mật khẩu',
             'admin_password.min'=>'Mật khẩu tối thiểu phải có 6 ký tự',
             'admin_password.max'=>'Mật khẩu tối đa chỉ được 20 ký tự',
             'admin_img.required'=>'bạn chưa chọn ảnh đại diện',
             'admin_phone.required'=>'Bạn chưa nhập số điện thoại',     
            ]
        );
        $user = new Admin;
        $user->admin_name   = $request->admin_name;
        $user->admin_email   = $request->admin_email;
        $user->admin_password   = md5($request->admin_password);
        $user->admin_phone  = $request->admin_phone;
        $get_image = $request->file('admin_img');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_user',$new_image);
            $user->admin_img = $new_image;
        }else{
            $user->admin_img = '';
        }
        $user->save();
        Session::put('message','thêm tài khoản thành công');
        return Redirect::to('add-user');
    }
    // update_tag..........................................................
    public function update_user(Request $request,$admin_id){
         $this->validate($request, [
            'admin_name'=>'required|min:2|max:50',
            'admin_email'=>'required',
            'admin_password'=>'required',
            'admin_phone'=>'required',
        ], [
             'admin_name.required'=>'Bạn chưa nhập tên tài khoản',
             'admin_name.min'=>'Tên phải lớn hơn 2 kí tự',
             'admin_name.max'=>'Tên chỉ được tối thiểu là 50 ký tự',
             'admin_email.required'=>'Bạn chưa nhập email',
             'admin_password.required'=>'Bạn chưa nhập mật khẩu',
             'admin_phone.required'=>'Bạn chưa nhập số điện thoại',     
            ]
        );
        $user = Admin::find($admin_id);
        $user->admin_name   = $request->admin_name;
        $user->admin_email   = $request->admin_email;
        $user->admin_password   = md5($request->admin_password);
        $user->admin_phone  = $request->admin_phone;
        $get_image = $request->file('admin_img');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/img_user',$new_image);
            $user->admin_img = $new_image;
        }
        $user->update();
        Session::put('message','Cập nhật khoản thành công');
        return Redirect::to('all-user');
    }
     // delete_tag.........................................................
    public function delete_user($admin_id){
        Admin::where('admin_id',$admin_id)->delete();
        Session::put('message','xóa Tài khoản thành công !');
        return Redirect::to('all-user');
    }
	// login-user...............................................................
    public function login_user(){
    	return view('admin.login');
    }
    // trang admin quản trị.......................................................
    public function tong_quan(){
    	return view('admin.tongquan.tong_quan');
    }  
    // trang chủ ..............................
    public function admin_login(Request $request){
        $this->validate($request, [
            'admin_email'=>'required',
            'admin_password'=>'required',
            'g-recaptcha-response'=> new Captcha(),
        ], [
             'admin_email.required'=>'Bạn chưa nhập email tài khoản',
             'admin_password.required'=>'bạn chưa nhập mật khẩu', 
            ]
        );
        $data = $request->all();
    	$admin_email = $data['admin_email'];
    	$admin_password = md5($data['admin_password']);
    	$login = Login::where('admin_email',$admin_email)->where('admin_password',$admin_password)->first();
        $login_count = $login->count();
    	if ($login_count) {
    		Session::put('admin_name',$login->admin_name);
    		Session::put('admin_id',$login->admin_id);
    		return Redirect::to('/tong-quan');
    	}
    	else
    	{
    		Session::put('message','Tài khoản hoặc mật khẩu không đúng. Vui lòng nhập lại !');
    		return Redirect::to('login-user');
    	}
    }

    // login bằng facebook
    public function login_facebook(){
        return Socialite::driver('facebook')->redirect();
    }
    // 
    public function callback_facebook(){
        $provider = Socialite::driver('facebook')->user();
        $account = Social::where('provider','facebook')->where('provider_user_id',$provider->getId())->first();
        if($account){
            //login in vao trang quan tri  
            $account_name = Login::where('admin_id',$account->user)->first();
            Session::put('admin_name',$account_name->admin_name);
            Session::put('admin_id',$account_name->admin_id);
            return redirect('/tong-quan')->with('message', 'Đăng nhập Admin thành công');
        }else{

            $social = new Social([
                'provider_user_id' => $provider->getId(),
                'provider' => 'facebook'
            ]);

            $orang = Login::where('admin_email',$provider->getEmail())->first();

            if(!$orang){
                $orang = Login::create([
                    'admin_name' => $provider->getName(),
                    'admin_email' => $provider->getEmail(),
                    'admin_password' => '',
                    'admin_phone'=>''
                ]);
            }
            $social->login()->associate($orang);
            $social->save();

            $account_name = Login::where('admin_id',$account->user)->first();
            Session::put('admin_name',$account_name->admin_name);
            Session::put('admin_id',$account_name->admin_id);

            return redirect('/tong-quan')->with('message', 'Đăng nhập Admin thành công');
        } 
    }
    // login_google
    public function login_google(){
        return Socialite::driver('google')->redirect();
    }
    public function callback_google(){
        $users = Socialite::driver('google')->stateless()->user(); 
        // return $users->id;
        $authUser = $this->findOrCreateUser($users,'google');
        $account_name = Login::where('admin_id',$authUser->user)->first();
        Session::put('admin_name',$account_name->admin_name);
        Session::put('admin_id',$account_name->admin_id);
        return redirect('/tong-quan')->with('message', 'Đăng nhập Admin thành công');
      
       
    }
    public function findOrCreateUser($users,$provider){
        $authUser = Social::where('provider_user_id', $users->id)->first();
        if($authUser){

            return $authUser;
        }
      
        $hieu = new Social([
            'provider_user_id' => $users->id,
            'provider' => strtoupper($provider)
        ]);

        $orang = Login::where('admin_email',$users->email)->first();

            if(!$orang){
                $orang = Login::create([
                    'admin_name' => $users->name,
                    'admin_email' => $users->email,
                    'admin_password' => '',

                    'admin_phone' => '',
                    'admin_status' => 1
                ]);
            }
        $hieu->login()->associate($orang);
        $hieu->save();

        $account_name = Login::where('admin_id',$hieu->user)->first();
        Session::put('admin_login',$account_name->admin_name);
        Session::put('admin_id',$account_name->admin_id);
        return redirect('/tong-quan')->with('message', 'Đăng nhập Admin thành công');
    }

    // kiển tra xem đã đăng nhập chưa
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('/tong-quan');
        }else{
            return Redirect::to('login-user');
        }
    }
    // đăng xuất tài khoản .......................................
    public function logout(){
        // $this->AuthLogin();
    	Session::put('admin_email',null);
    	Session::put('admin_id',null);
    	return Redirect::to('/login-user');
    }
}
