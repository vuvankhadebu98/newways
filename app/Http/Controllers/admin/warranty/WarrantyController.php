<?php

namespace App\Http\Controllers\admin\warranty;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\Warranty;
use Validate;
use Illuminate\Support\Facades\Auth;

class WarrantyController extends Controller
{
    // tìm kiếm.............................................................................
    public function search_warranty(Request $request){
        $all_warranty = Warranty::where('name','like','%'.$request->key.'%')->get();
        return view('admin.warranty.search_warranty',compact('all_warranty'));
    }
    // add_warranty.............................................................................
    public function add_warranty(){
    	return view('admin.warranty.add_warranty');
    }
    // list_warranty............................................................................
    public function all_warranty()
    {
    	$all_warranty = Warranty::orderBy('id','DESC')->paginate(5);
    	return view('admin.warranty.all_warranty')->with(compact('all_warranty'));
    }
    // edit_warranty...........................................................................
    public function edit_warranty($id){
        $edit_warranty = Warranty::where('id',$id)->get();
        return view('admin.warranty.edit_warranty')->with(compact('edit_warranty'));      
    }
    //unactive_warranty-------------------------------------------------------....
    public function unactive_warranty($id){
        DB::table('warranty')->where('id', $id)->update(['status'=>1]);
        Session::put('message','Hiển thị chính sách thành công !');
        return Redirect::to('all-warranty');
    }
    //active_product..........................................................
    public function active_warranty($id){
        DB::table('warranty')->where('id', $id)->update(['status'=>0]);
        Session::put('message','Ẩn chính sách thành công !');
        return Redirect::to('all-warranty');
    }
    // save_warranty............................................................................
    public function save_warranty(Request $request){
    	$this->validate($request, [
            'name'=>'required|min:2|Max:100',
            'desc'=>'required|max:150',
            'content'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên chính sách',
             'name.min'=>'Tên phải lớn hơn 2 kí tự',
             'name.max'=>'Bạn chỉ được nhập tối thiểu 100 ký tự',
             'desc.required'=>'Bạn chưa nhập mô tả chính sách',
             'desc.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung bảo hành',
            ]
        );
        $warranty = new Warranty;
        $warranty->name   = $request->name;
        $warranty->desc   = $request->desc;
        $warranty->content   = $request->content;
        $warranty->status  = $request->status;
        $warranty->save();
        Session::put('message','Thêm Bảo thành công');
        return Redirect::to('add-warranty');
    }

    //update_warranty...............................................................................
    public function update_warranty(Request $request, $id){
    	$this->validate($request, [
            'name'=>'required|min:2|Max:100',
            'desc'=>'required|max:150',
            'content'=>'required',
        ], [
             'name.required'=>'Bạn chưa nhập tên chính sách',
             'name.min'=>'Tên phải lớn hơn 2 kí tự',
             'name.max'=>'Bạn chỉ được nhập tối thiểu 100 ký tự',
             'desc.required'=>'Bạn chưa nhập mô tả chính sách',
             'desc.max'=>'Bạn chỉ được nhập tối thiểu 150 ký tự',
             'content.required'=>'Bạn chưa nhập nội dung bảo hành',
            ]
        );
    	$warranty = Warranty::find($id);
        $warranty->name   = $request->name;
        $warranty->desc   = $request->desc;
        $warranty->content   = $request->content;
        $warranty->status   = $request->status;
        $warranty->update();
        Session::put('message','Cập nhật Bảo Hành thành công');
        return Redirect::to('all-warranty');
    } 
    // delete_warranty.........................................................
    public function delete_warranty($id){
        Warranty::where('id',$id)->delete();
        Session::put('message','xóa Bảo Hành thành công !');
        return Redirect::to('all-warranty');
    }
}
