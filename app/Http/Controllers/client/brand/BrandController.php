<?php

namespace App\Http\Controllers\client\brand;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();


class BrandController extends Controller
{
    public function show_brand_home(Request $request , $id){
        
        $brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get();
        $brand_by_id = DB::table('products')->where('brand_id',$id)->get();

        foreach($brand as $key => $val){
            // seo
            $meta_desc = $val->meta_desc;
            $meta_keywords = $val->meta_keywords;
            $meta_title =  $val->name;
            $url_canonical = $request->url();
            //end_seo
        }
            
        $all_product = DB::table('products')->where('status','1')->orderby('id','desc')->limit(7)->get();
        $brand_name = DB::table('brands')->where('brands.id',$id)->limit(1)->get();

        return view('client.brand.show_brand')->with(compact('brand','brand_by_id','meta_desc','meta_keywords','meta_title','url_canonical','all_product','brand_name'));
    }
}
