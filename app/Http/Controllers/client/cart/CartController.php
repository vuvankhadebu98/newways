<?php

namespace App\Http\Controllers\client\cart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Cart;
use App\Model\Coupon;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class CartController extends Controller

{
    // check_coupon...........................................................................
    public function check_coupon(Request $request){
        $data = $request->all();
        $coupon = Coupon::where('code',$data['coupon'])->first();
        if ($coupon == true) {
            $count_coupon = $coupon->count();
            if ($count_coupon > 0) {
                $coupon_session = Session::get('coupon');
                if ($coupon_session == true) {
                    $is_avaiable = 0;
                    if ($is_avaiable == 0) {
                        $cou[] = array(
                            'code' => $coupon->code,
                            'check' => $coupon->check,
                            'number' => $coupon->number,
                        );
                        Session::put('coupon',$cou);
                    }
                }
                else
                {
                    $cou[] = array(
                        'code' => $coupon->code,
                        'check' => $coupon->check,
                        'number' => $coupon->number,
                    );
                    Session::put('coupon',$cou);
                }
                Session::save();
                return Redirect()->back()->with('message','Thêm Mã Giảm Giá Thành Công');
            }
        }
        else
        {
            return Redirect()->back()->with('error','Mã Giảm Giá Không Đúng Hoặc Không Dùng Được');
        }
    }
    // delete_coupon.............................................................................
    public function unset_coupon(){
        $coupon = Session::get('coupon');
        if ($coupon==true) {
            Session::forget('coupon');
            return Redirect()->back()->with('message','Xóa Mã Giảm Giá Thành Công');
        }
    }
	
	// save_cart...............................................................................
    public function save_cart(Request $request){

    	$qty = $request->qty;
    	$product_hidden = $request->product_hidden;
    	$product_info = DB::table('products')->where('id',$product_hidden)->first();

    	// // $data['id'] = $product_info->id;
    	// // $data['qty'] = $qty;
    	// // $data['name'] = $product_info->name;
    	// // $data['price'] = $product_info->price;
    	// // $data['weight'] = '123';
    	// // $data['options']['image'] = $product_info->img;
    	// // Cart::add($data);
    	// // return Redirect::to('show-cart');
    	$category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
    	return view('client.cart.show_cart')->with('category',$category);
        // Cart::destroy();
    }
    // show_cart.....................................................................................
    public function show_cart(Request $request){

        $meta_desc = "Giỏ hàng của bạn";
        $meta_keywords = "Giỏ hàng";
        $meta_title = "Giỏ hàng";
        $url_canonical = $request->url();

    	$category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
        $all_product = DB::table('products')->where('status','1')->orderby('id','desc')->limit(6)->get();

    	return view('client.cart.show_cart')->with('category',$category)
        ->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)
        ->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('all_product',$all_product);
    }

    // add_cart_ajax.................................................................................
    public function add_cart_ajax(Request $request){
        $data = $request->all();
        $session_id = substr(md5(microtime()), rand(0,26),5);
        $cart = Session::get('cart');

        if ($cart == true) {
            $is_avaiable = 0;
            foreach($cart as $key =>$val){
                if ($request->cart_product_id == $val['product_id']) {
                    $is_avaiable++;
                }         
            }
            if ($is_avaiable == 0) {
                $cart[] = array(
                'session_id' => $session_id,
                'product_name' => $request->cart_product_name,
                'product_id' => $request->cart_product_id,
                'product_img' => $request->cart_product_img,
                'product_quantity' => $request->cart_product_qantity,
                'product_qty' => $request->cart_product_qty,
                'product_price' => $request->cart_product_price,
                );
                Session::put('cart',$cart);
            }
        }
        else
        {
            $cart[] = array(
            'session_id' => $session_id,
            'product_name' => $request->cart_product_name,
            'product_id' => $request->cart_product_id,
            'product_img' => $request->cart_product_img,
            'product_quantity' => $request->cart_product_qantity,
            'product_qty' => $request->cart_product_qty,
            'product_price' => $request->cart_product_price,
            );
        }
        Session::put('cart',$cart);
        Session::save();
    }

    //update_cart.................................................................................................
    public function update_cart(Request $request){
        $data = $request->all();
        $cart = Session::get('cart');
        if ($cart==true) {
            foreach ($data['cart_quantity'] as $key => $qty) {
                foreach ($cart as $session => $val) {
                    if ($val['session_id']== $key) {
                        $cart[$session]['product_qty'] = $qty;
                    }
                }
            }
            Session::put('cart',$cart);
            return Redirect()->back()->with('message','Cập Nhật Số Lượng Đơn Hàng Thành Công');
        }
        else
        {
            Session::put('cart',$cart);
            return Redirect()->back()->with('message','Cập Nhật Số Lượng Đơn Hàng Thất Bại');
        }
    } 

    // delete_cart.................................................................................................
    public function delete_cart($session_id){
        $cart = Session::get('cart');
        if ($cart == true) {
            foreach ($cart as $key => $val) {
                if ($val['session_id'] == $session_id) {
                    unset($cart[$key]);
                }
            }
            Session::put('cart',$cart);
            return Redirect()->back()->with('message','Xóa Đơn Hàng Thành Công');
        }
        else
        {
            return Redirect()->back()->with('message','Xóa Đơn Hàng Thất Bại');
        }
    }

    // delete_all_order................................................................................................
    public function delete_all_order(){
        $cart = Session::get('cart');
        if ($cart==true) {
            Session::forget('cart');
            Session::forget('coupon');
            return Redirect()->back()->with('message','Xóa Tất Cả Giỏ Hàng Thành Công');
        }
    }
}
