<?php

namespace App\Http\Controllers\client\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function show_category_home(Request $request , $id){
    	
        $category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
        $category_by_id = DB::table('products')->where('category_id',$id)->get();

        foreach($category as $key => $val){
        	// seo
	        $meta_desc = $val->meta_desc;
	        $meta_keywords = $val->meta_keywords;
	        $meta_title =  $val->name;
	        $url_canonical = $request->url();
	        //end_seo
        }
        	
        $all_product = DB::table('products')->where('status','1')->orderby('id','desc')->limit(7)->get();
        $category_name = DB::table('categorys')->where('categorys.id',$id)->limit(1)->get();

        return view('client.category.show_category')->with(compact('category','category_by_id','meta_desc','meta_keywords','meta_title','url_canonical','all_product','category_name'));
    }
}
