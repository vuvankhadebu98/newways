<?php

namespace App\Http\Controllers\client\checkout;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Model\City;
use App\Model\Province;
use App\Model\wards;
use App\Model\Feeship;
use App\Model\Shipping;
use App\Model\Order;
use App\Model\Order_Detail;

class CheckoutController extends Controller
{	
	// trang đăng nhập và đăng ký.......................................................................
    public function login_checkout(){
    	$category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
        $brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get();
    	return view('client.login.login_check',compact('category','brand'));
    }

    // add-customer....................................................................................
    public function add_customer(Request $request){
    	$customer = array();
    	$customer['name'] = $request->name;
    	$customer['sex'] = $request->sex;
    	$customer['email'] = $request->email;
    	$customer['password'] = md5($request->password);
    	$customer['phone'] = $request->phone;
    	$customer['address'] = $request->address;

    	$customer_id = DB::table('customers')->insertGetId($customer);

    	Session::put('id',$customer_id);
    	Session::put('name',$request->name);
    	return Redirect::to('/checkout');	
    }
    //checkout.......................................................................................
    public function checkout(){
    	$category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
        $brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get();
        $city = City::orderby('matp','ASC')->get();

    	return view('client.checkout.show_checkout',compact('category','brand','city'));
    }

    // save_checkout_customer............................................................................
    public function save_checkout_customer(Request $request){
    	$shipping = array();
    	$shipping['name'] = $request->name;
    	$shipping['address'] = $request->address;
    	$shipping['phone'] = $request->phone;
    	$shipping['email'] = $request->email;
    	$shipping['notes'] = $request->notes;

    	$shipping_id = DB::table('shipping')->insertGetId($shipping);

    	Session::put('id',$shipping_id);
    	return Redirect::to('/payment');
    }

    // trang thanh toán...........................................................................
    // // payment...........................................................................
    // public function payment(){
    //     $category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
    //     $brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get();
    //     return view('client.checkout.payment',compact('category','brand'));
    // }

    // order_place...............................................................................
    public function order_place(Request $request){
        // insert payment_method
        $payment = array();
        $payment['method'] = $request->payment_option;
        $payment['status'] = 'Đang chờ xử lý';
        $payment_id = DB::table('payment')->insertGetId($payment);

        //insert orders.........................................................................
        $order = array();
        $order['customer_id'] = Session::get('id');
        $order['shipping_id'] = Session::get('id');
        $order['payment_id'] = $payment_id;
        $order['total'] = $total;
        $order['status'] = 'Đang chờ xử lý';
        $order_id = DB::table('orders')->insertGetId($order);

        // insert order_detail.................................................................
        // @foreach(Session::get('cart') as $v_cart){
        //     $order_detail = array();
        //     $order_detail['order_id'] = $order_id;
        //     $order_detail['product_id'] = $v_cart->id;
        //     $order_detail['product_name'] = $v_cart->name;
        //     $order_detail['product_price'] = $v_cart->price;
        //     $order_detail['product_qty'] = $v_cart->qty;
        //     $order_detail_id = DB::table('order_detail')->insert($order_detail);
        // }
        // if ($payment['method'] == 1) {
        //     echo 'Thanh toán bằng thẻ ATM';
        // }
        // elseif ($payment['method'] == 2) {
        //     Cart::destroy();
        //     $category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();
        //     $brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get();
        //     return view('client.checkout.handcash',compact('category','brand'));
        // }
        // elseif ($payment['method'] == 3) {
        //     echo 'Thanh toán bằng thẻ VISA';
        // }
    
        return Redirect::to('/payment'); 
    }
    // phí vận chuyển.............................................................................
    public function select_delivery_home(Request $request){
        $data = $request->all();
        if ($data['action']){
            $output = '';
            if ($data['action']=='city'){
                $select_province = Province::where('matp',$data['ma_id'])->orderby('maqh','ASC')->get();
                $output .= '<option>---Chọn Quận Huyện---</option>'; 
                foreach($select_province as $key => $prov){
                    $output .='<option value="'.$prov->maqh.'">'.$prov->name.'</option>';
                }
            }
            else
            {
                $select_wards = Wards::where('maqh',$data['ma_id'])->orderby('xaid','ASC')->get();
                $output .= '<option>---Chọn Xã Phường---</option>'; 
                foreach($select_wards as $key => $ward){
                    $output .='<option value="'.$ward->xaid.'">'.$ward->name.'</option>';
                }
            }
        }
        return $output;
    }
    // tính phí vận chuyển ................................................................
    public function calculate_fee(Request $request){
        $data = $request->all();
        if ($data['matp']) {
            $feeship = Feeship::where('matp',$data['matp'])->where('maqh',$data['maqh'])
            ->where('xaid',$data['xaid'])->get();
            if ($feeship) {
                $count_feeship = $feeship->count();
                if ($count_feeship > 0) {
                    foreach ($feeship as $key => $fee) {
                        Session::put('fee',$fee->feeship);
                        Session::save();
                    }
                }else{
                    Session::put('fee',20000);
                    Session::save();
                }
            }
        }
    }
    // xác nhận đơn hàng.(confirm_order)
    public function confirm_order(Request $request){
        $data = $request->all();
        $shipping = new Shipping();
        $shipping->name = $data['name'];
        $shipping->email = $data['email'];
        $shipping->address = $data['address'];
        $shipping->phone = $data['phone'];
        $shipping->notes = $data['notes'];
        $shipping->method = $data['method'];
        $shipping->save();
        
        $shipping_id = $shipping->id;

        $checkout_code = substr(md5(microtime()),rand(0,26),5);

        $order = new Order;
        $order->customer_id = Session::get('id');
        $order->shipping_id = $shipping_id;
        $order->code = $checkout_code;
        $order->status = 1;
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $order->created_at = now();

        $order->save();

        if (Session::get('cart')) {
            foreach(Session::get('cart') as $key => $cart){
                $order_detail = new Order_Detail;
                $order_detail->order_code = $checkout_code;
                $order_detail->product_id = $cart['product_id'];
                $order_detail->product_name = $cart['product_name'];
                $order_detail->product_price = $cart['product_price'];
                $order_detail->product_qty = $cart['product_qty'];
                $order_detail->product_coupon = $data['order_coupon'];
                $order_detail->product_feeship = $data['order_fee'];
                $order_detail->save();
            }
        }
        Session::forget('coupon');
        Session::forget('fee');
        Session::forget('cart');
    }

    // xóa phí vận chuyển(delete_fee) ........................................................
    public function delete_fee(Request $request){
        Session::forget('fee');
        return Redirect()->back();
    }

    // logout_checkout........................................................................
    public function logout_checkout(){
    	Session::flush();
    	return Redirect::to('login-checkout');
    }

    // login_customer...............................................................................
    public function login_customer(Request $request){
    	$email = $request->email;
    	$password = md5($request->password);
    	$result = DB::table('customers')->where('email',$email)->where('password',$password)->first();
    	if ($request) {
    		Session::put('id',$result->id);
    		return Redirect::to('checkout');
    	}
    	else{
    		return Redirect::to('login-checkout');
    	}	
    }
}
