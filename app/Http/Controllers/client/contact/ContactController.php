<?php

namespace App\Http\Controllers\client\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\Coupon;
use App\Model\Contact;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();
use Mail;
use Carbon;

class ContactController extends Controller
{
	// list_contact.................................................................
	public function list_contact(){
		return view('client.contact.list_contact');
	}
    // send_mail..................................................................................
    public function save_contact(Request $request){
   	// send mail
	    $data = $request->except('_token');
        // $data['created_at'] = $data['updated_at'] = Carbon::now();
        Contact::insert($data);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'title'=> $request->title,
            'content'=>$request->content,
        ];
        Mail::send('client.contact.contact_mail',$data,function($message) use ($data){
            $message->from($data['email'],'Liên hệ');
            $message->to('vuvankhadebu98@gmail.com','MR:vankha');
            $message->subject('Liên hệ Từ Khách Hàng');
        });

        return redirect()->back()->with('thongbao','Gửi liên hệ thành công'); 
    }
}
