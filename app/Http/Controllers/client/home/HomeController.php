<?php

namespace App\Http\Controllers\client\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Model\Slider;
use App\Model\Product;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class HomeController extends Controller
{


    // trang chủ
    public function index(Request $request){
        // seo
        // $meta_desc = "Chuyên bán các máy tính tiêu chuẩn chất lượng. Các mặt hàng chất lượng và đi đầu trong các thương hiệu";
        // $meta_keywords = "Bán các dòng máy tính với tầm giá từ thấp cho đến cao";
        // $meta_title = "NEW_WAYS - Bán các mặt hàng về máy tính" ;
        // $url_canonical = $request->url();
        //end_seo
        $product_slider = Product::where('status',1)->orderby('id','desc')->take(4)->get();
        $slider = Slider::where('status',1)->orderby('id','desc')->take(3)->get();
    	$category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get(); 
    	$brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get(); 
        $all_product = DB::table('products')->where('status','1')->orderby('id','desc')->limit(6)->get();
        $product_all_home = Product::where('status','1')->orderby('id','desc')->paginate(12);

    	return view('client.home')->with('category',$category)->with('brand',$brand)
        ->with('all_product',$all_product)->with('slider',$slider)->with('product_slider',$product_slider)->with('product_all_home',$product_all_home);
    }

    // search...............................................................................................
    public function search(Request $request){
    	$keywords = $request->keywords_submit;
    	$category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get(); 
    	$brand = DB::table('brands')->where('status','1')->orderby('id','desc')->get(); 
    	$all_product = DB::table('products')->where('status','1')->orderby('id','desc')->limit(6)->get();
    	$search_product = DB::table('products')->where('name','like','%'. $keywords .'%')->get();

    	return view('client.product.search')->with('category',$category)
    	->with('brand',$brand)->with('search_product',$search_product)->with('all_product',$all_product);
    }
    // lỗi 404............................................................................................
    public function error_home(){
        return view('errors.404');
    }
}
