<?php

namespace App\Http\Controllers\client\news;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\News;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();
Use Carbon;

class NewsController extends Controller
{	
	// hiển thị trang tin tức...................................................
    public function list_news(){
    	return view('client.news.list_news');
    }
    // show chi tiết tin tức......................................................
    public function show_detail_news($id){
        // $all_news = DB::table('news')->where('status','1')
        // ->orderby('id','desc')->limit(9)->whereNotIn('news.id',[$id])->get();
        $deltails_news = DB::table('news')->where('id',$id)->where('status','1')->get();
        return view('client.news.show_detail_news')->with('deltails_news',$deltails_news);
    }
}
