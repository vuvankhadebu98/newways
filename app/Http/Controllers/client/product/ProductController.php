<?php

namespace App\Http\Controllers\client\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class ProductController extends Controller
{
	// chi-tiet-san-pham.....................................................
    public function details_product(Request $request, $id){

        $category = DB::table('categorys')->where('status','1')->orderby('id','desc')->get();

        $all_product = DB::table('products')->where('status','1')
        ->orderby('id','desc')->limit(5)->whereNotIn('products.id',[$id])->get();
        $deltails_product = DB::table('products')->where('id',$id)->get();

        foreach($deltails_product as $key => $value){
        	$category_id  = $value->category_id;
            // seo
            $meta_desc = $value->desc;
            $meta_keywords = $value->desc;
            $meta_title = $value->name;
            $url_canonical = $request->url();
            // endseo
        }
        $show_product = DB::table('products')->where('status','1')->orderby('id','desc')->limit(5)->get();
        // $related_product = DB::table('products')
        // ->where('products.id',$id)->whereNotIn('products.id',[$id])->get();
        // ->join('categorys','categorys.id','=','products.category_id')
        // ->where('categorys.id',$category_id)->whereNotIn('products.id',[$id])->get();
        
        return view('client.product.show_details')->with('category',$category)
        ->with('all_product',$all_product)->with('deltails_product',$deltails_product)->with('show_product',$show_product);
        // ->with('related_product',$related_product);
    }
}
