<?php

namespace App\Http\Controllers\client\warranty;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\Warranty;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();
Use Carbon;

class WarrantyController extends Controller
{
     // show chi tiết tin tức......................................................
    public function detail_warranty($id){
        $deltails_warranty = Warranty::where('id',$id)->where('status','1')->get();
        return view('client.warranty.detail_warranty')->with(compact('deltails_warranty'));
    }
}
