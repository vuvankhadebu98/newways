<?php

namespace App\Imports;

use App\Model\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class Excel_Imports implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'user_id'=> $row[0],
            'brand_id'=> $row[1],
            'category_id'=> $row[2],
            'name'=> $row[3],
            'desc'=> $row[4],
            'content'=> $row[5],
            'price'=> $row[6],
            'qty'=> $row[8],
            'img'=> $row[9],
            'check'=> $row[10],
            'check_new'=> $row[11],
            'status'=> $row[12],
        ]);
    }
}
