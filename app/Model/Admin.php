<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected  $table = 'admin';
    protected $primaryKey = 'admin_id';
    protected $fillable = [
    	'admin_email','admin_name','admin_img','admin_password','admin_phone'
    ];
}
