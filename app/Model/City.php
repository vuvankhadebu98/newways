<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected  $table = 'tinhthanhpho';
    protected $primaryKey = 'matp';
    protected $fillable = [
    	'name','type',
    ];
}
