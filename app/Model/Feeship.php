<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feeship extends Model
{
    protected  $table = 'feeship';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'matp','maqh','xaid','feeship'
    ];

    public function city(){
    	return $this->belongsTo('App\Model\City', 'matp');
    }
    public function province(){
    	return $this->belongsTo('App\Model\Province', 'maqh');
    }
    public function wards(){
    	return $this->belongsTo('App\Model\Wards', 'xaid');
    }
}
