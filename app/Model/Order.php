<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = false;
    protected  $table = 'orders';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'customer_id','shipping_id','code','status','created_at'
    ];
    // public function customer
    public function customer()
    {
    	return $this->hasOne('App\Model\Customer','id','customer_id');
    }
    public function shipping()
    {
    	return $this->hasOne('App\Model\Shipping','id','shipping_id');
    }
    public function order_detail()
    {
        return $this->hasOne('App\Model\Order_Detail','order_id','id');
    }
}
