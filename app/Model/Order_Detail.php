<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order_Detail extends Model
{
    protected  $table = 'order_detail';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'order_code','product_id','product_name','product_price','product_qty','product_coupon','product_feeship'
    ];

    public function product()
    {
    	return $this->belongsTo('App\Model\Product','product_id');
    }
}
