<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $table = 'products';
    protected $fillable = [
        'user_id','brand_id','category_id','name','desc','content','price','qty','sold','img','check','check_new','status',
    ];

    public function admin()
    {
    	return $this->hasOne('App\Model\admin','admin_id','admin_id');
    }
    public function category()
    {
    	return $this->hasOne('App\Model\Category','id','category_id');
    }
    public function brand()
    {
        return $this->hasOne('App\Model\Brand','id','brand_id');
    }
}
