<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected  $table = 'quanhuyen';
    protected $primaryKey = 'maqh';
    protected $fillable = [
    	'name','type','matp'
    ];
}
