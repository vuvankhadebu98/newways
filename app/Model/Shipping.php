<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{	
	public $timestamps = false;
    protected  $table = 'shipping';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'name','address','phone','email','notes','method',
    ];
}
