<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wards extends Model
{
    protected  $table = 'xaphuongthitran';
    protected $primaryKey = 'xaid';
    protected $fillable = [
    	'name','type','maqh'
    ];
}
