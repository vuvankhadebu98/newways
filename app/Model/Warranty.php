<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    protected $table = 'warranty';
    public $timestamps = false;
    protected $fillable = [
          'name','status','img','desc','content'
    ];
    protected $primaryKey = 'id';
}
