<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\Category;
use App\Model\Brand;
use App\Model\Order;
use App\Model\Product;
use App\Model\News;
use App\Model\User;
use App\Model\Province;
use App\Model\Slider;
use App\Model\Warranty;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['brand_home'] = Brand::all();
        $data['category'] = Category::all();
        $data['news_home'] = News::where('status',1)->orderBy('id','desc')->get();
        // $data['slider_home'] = Slider::where('status',1)->orderby('id','desc')->take(3)->get();
        $data['product_dell'] = Product::where('brand_id',1)->where('status',1)->orderby('id','desc')->limit(6)->get();
        $data['product_hp'] = Product::where('brand_id',3)->where('status',1)->orderby('id','desc')->limit(6)->get();
        $data['product_macbook'] = Product::where('brand_id',2)->where('status',1)->orderby('id','desc')->limit(6)->get();
        $data['product_asus'] = Product::where('brand_id',4)->where('status',1)->orderby('id','desc')->limit(6)->get();
        $data['product_lenovo'] = Product::where('brand_id',5)->where('status',1)->orderby('id','desc')->limit(6)->get();
        $data['news_home_2'] = News::where('status','1')->orderby('id','desc')->limit(6)->get();
        $data['warranty_home'] = Warranty::where('status','1')->get();
        view()->share($data);
    }
}
