<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('admin_id');
            $table->integer('category_id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('desc');
            $table->text('content');            
            $table->string('price');
            $table->integer('qty');
            $table->integer('sold')->default(0);
            $table->string('img');
            $table->integer('check');
            $table->integer('check_new');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
