<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->Increments('id'); //Id  
            $table->string('name');// Họ
            $table->string('img');// Ảnh
            $table->string('email');
            $table->string('password'); // Mật khẩu // Email
            $table->string('phone',15); // Số điện thoại
            $table->integer('sex'); //Giới tính 
            $table->string('address'); // Tỉnh
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
