<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- seo --}}
    <meta name="description" content="">
    <meta name="keywords" content="complete protein">
    <meta name="robots" content="INDEX,FOLLOW">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="public/backend/assets/images/favicon.png">
    <link rel="canonical" href="http://localhost/newways/">
    {{-- end seo --}}
    <title>Admin | NEWAYS</title>
    <!-- Custom CSS -->

    <base href="{{ asset('admin') }}">
    <link href="public/backend/assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="public/backend/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="public/backend/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="public/backend/dist/css/style.min.css" rel="stylesheet">
    
</head>
<body>    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div> 
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6" style="margin-top: -1px;background: #22ca80;">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <!-- Logo icon -->
                        <a href="{{URL::to('tong-quan')}}">
                            <b class="logo-icon">
                                <!-- Dark Logo icon -->
                                <img src="public/backend/assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo icon -->
                                <img src="public/backend/assets/images/logo-icon.png" alt="homepage" class="light-logo" />
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <span class="logo-text">
                                <!-- dark Logo text -->
                                <img src="public/backend/assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="public/backend/assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" style=" background: #dc3939">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1">
                        <!-- Notification -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle pl-md-3 position-relative" href="javascript:void(0)"
                                id="bell" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <span><i data-feather="bell" class="svg-icon"></i></span>
                                <span class="badge badge-primary notify-no rounded-circle">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="message-center notifications position-relative">
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <div class="btn btn-danger rounded-circle btn-circle"><i
                                                        data-feather="airplay" class="text-white"></i></div>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Luanch Admin</h6>
                                                    <span class="font-12 text-nowrap d-block text-muted">Just see
                                                        the my new
                                                        admin!</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-success text-white rounded-circle btn-circle"><i
                                                        data-feather="calendar" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Event today</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">Just
                                                        a reminder that you have event</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-info rounded-circle btn-circle"><i
                                                        data-feather="settings" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Settings</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">You
                                                        can customize this template
                                                        as you want</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-primary rounded-circle btn-circle"><i
                                                        data-feather="box" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Pavan kumar</h6> <span
                                                        class="font-12 text-nowrap d-block text-muted">Just
                                                        see the my admin!</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link pt-3 text-center text-dark" href="javascript:void(0);">
                                            <strong>Check all notifications</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Notification -->
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="settings" class="svg-icon"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <div class="customize-input">
                                    <select
                                        class="custom-select form-control bg-white custom-radius custom-shadow border-0">
                                        <option selected>Việt Nam</option>
                                        <option value="1">English</option>
                                    </select>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <form>
                                    <div class="customize-input-timkiem">
                                        <input class="form-control custom-shadow custom-radius border-0 bg-white"
                                            type="search" placeholder="Search" aria-label="Search">
                                        <i class="form-control-icon" data-feather="search"></i>
                                    </div>
                                </form>
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="public/backend/assets/images/users/profile-pic.jpg" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ml-2 d-none d-lg-inline-block">
                                    <span>Hello,</span> 
                                    <span class="text-dark">
                                        <?php 
                                            $username = Session::get('admin_name');
                                            if ($username) {
                                                echo $username ;
                                            }
                                        ?>   
                                    </span> 
                                    <i data-feather="chevron-down"class="svg-icon"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="user"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="credit-card"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="mail"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="settings"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{URL::to('logout')}}">
                                    <i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Đăng Xuất</a>
                                <div class="dropdown-divider"></div>
                                <div class="pl-4 p-3"><a href="javascript:void(0)" class="btn btn-sm btn-info">View
                                        Profile</a></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> 
                            <a class="sidebar-link sidebar-link" href="{{URL::to('tong-quan')}}"
                                aria-expanded="false">
                                <i class=" fas fa-university"></i>
                                <span class="hide-menu">Tổng Quan</span>
                            </a>
                        </li>
                        <li class="list-divider"></li>
                        {{-- quản lý newways --}}
                        <li class="nav-small-cap">
                            <span class="hide-menu">Quản Lý Newways</span>
                        </li>
                        {{-- Thương hiệu sản phẩm --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false">
                                <i class="fas fa-bookmark"></i>
                                <span class="hide-menu">Thương Hiệu </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('add-brand')}}" class="sidebar-link">
                                        <span class="hide-menu">Thêm Thương Hiệu</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{URL::to('all-brand')}}" class="sidebar-link">
                                        <span class="hide-menu">Danh Sách Thương Hiệu</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- danh mục Sản phẩm --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false">
                                <i data-feather="grid" class="feather-icon"></i>
                                <span class="hide-menu">Danh Mục Sản Phẩm </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('add-category')}}" class="sidebar-link">
                                        <span class="hide-menu">Thêm Danh Mục</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{URL::to('all-category')}}" class="sidebar-link">
                                        <span class="hide-menu">Danh Sách Danh Mục</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- sản phẩm --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false">
                                <i class="fa fa-laptop"></i>
                                <span class="hide-menu">Sản Phẩm</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('add-product')}}" class="sidebar-link">
                                        <span class="hide-menu">Thêm Sản Phẩm</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{URL::to('all-product')}}" class="sidebar-link">
                                        <span class="hide-menu">Danh Sách Sản Phẩm</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- tin tức --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false">
                                <i class="fas fa-location-arrow"></i>
                                <span class="hide-menu">Tin Tức</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('add-news')}}" class="sidebar-link">
                                        <span class="hide-menu">Thêm Tin Tức</span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{URL::to('all-news')}}" class="sidebar-link">
                                        <span class="hide-menu">Danh Sách Tin Tức</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- quản lý tài khoản --}}
                        {{-- tin tức --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false">
                                <i class="fas fa-users"></i>
                                <span class="hide-menu">Tài Khoản</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('all-user')}}" class="sidebar-link">
                                        <span class="hide-menu">Quản Lý Tài Khoản</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="list-divider"></li>

                        <li class="nav-small-cap"><span class="hide-menu">Thống Kê</span></li>
                        {{-- đơn hàng --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"aria-expanded="false">
                                <i data-feather="box" class="feather-icon"></i>
                                <span class="hide-menu">Đơn Hàng </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('view-order')}}" class="sidebar-link">
                                        <span class="hide-menu"> Thống Kê Đơn Hàng
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- mã giảm giắ --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"aria-expanded="false">
                                <i class="fas fa-warehouse"></i>
                                <span class="hide-menu">Mã Giảm Giá</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('add-coupon')}}" class="sidebar-link">
                                        <span class="hide-menu">Thêm Mã Giảm giá
                                        </span>
                                    </a>
                                </li>
                                <li class="sidebar-item">
                                    <a href="{{URL::to('all-coupon')}}" class="sidebar-link">
                                        <span class="hide-menu"> Thống Kê Mã Giảm giá
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        {{-- quản lý vận chuyển --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link has-arrow" href="javascript:void(0)"aria-expanded="false">
                                <i class="fas fa-truck"></i>
                                <span class="hide-menu">Vận Chuyển</span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item">
                                    <a href="{{URL::to('add-delivery')}}" class="sidebar-link">
                                        <span class="hide-menu">Thống Kê Vận Chuyển
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap">
                            <span class="hide-menu">Các thứ khác</span></li>
                        {{-- Liên hệ từ khách hàng --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link sidebar-link" href="{{URL::to('list-contact')}}"
                                aria-expanded="false">
                                <i class="fas fa-tty"></i>  
                                </i><span class="hide-menu">Liên Hệ</span>
                            </a>
                        </li>
                        {{-- slider--}}
                         <li class="sidebar-item"> 
                            <a class="sidebar-link sidebar-link" href="{{URL::to('list-slider')}}"
                                aria-expanded="false">
                                <i class="fas fa-share-alt-square"></i>  
                                </i><span class="hide-menu">Quản Lý slider</span>
                            </a>
                        </li>
                        {{-- quản lý bảo hành --}}
                        <li class="sidebar-item"> 
                            <a class="sidebar-link sidebar-link" href="{{URL::to('all-warranty')}}"
                                aria-expanded="false">
                                <i class="fas fa-cogs"></i>  
                                </i><span class="hide-menu">Quản Lý Chính Sách</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            @yield('admin_content')
            {{--  --}}

            <footer class="footer text-center text-muted">
                Mọi quyền được bảo lưu bởi NEWWAYS. Được thiết kế và phát triển bởi <a
                    href="https://wrappixel.com">VanKha_Channel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
   
    <script src="public/backend/assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="public/backend/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="public/backend/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="public/backend/dist/js/app-style-switcher.js"></script>
    <script src="public/backend/dist/js/feather.min.js"></script>
    <script src="public/backend/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="public/backend/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="public/backend/dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <script src="public/backend/assets/extra-libs/c3/d3.min.js"></script>
    <script src="public/backend/assets/extra-libs/c3/c3.min.js"></script>
    <script src="public/backend/assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="public/backend/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="public/backend/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="public/backend/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
    <script src="public/backend/dist/js/pages/dashboards/dashboard1.min.js"></script>
    <script src="{{asset('public/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('public/ckfinder/ckfinder.js')}}"></script>
    {{-- cập nhật số lượng đơn hàng --}}
    <script type="text/javascript">
        $('.update_qty_order').click(function(){
            var order_product_id = $(this).data('product_id');
            var order_qty = $('.order_qty_'+order_product_id).val();
            var order_code = $('.order_code').val();
            var _token = $('input[name="_token"]').val();
            // alert(order_product_id);
            // alert(order_qty);
            // alert(order_code);
            $.ajax({
                url : '{{url('/update-qty')}}',
                method: 'POST',
                data:{_token:_token, order_product_id:order_product_id,order_qty:order_qty,order_code:order_code},
                success:function(data){
                    alert('Thay đổi tình trạng đơn hàng thành công');
                    location.reload();
                }
            });
        });
    </script>    
    {{-- cập nhật tình trạng đợn hàng--}}
    <script type="text/javascript">
        $('.order_detail_product').change(function(){
            var order_status = $(this).val();
            var order_id = $(this).children(":selected").attr("id");
            var _token = $('input[name="_token"]').val();
            // alert(status);
            // alert(id);
            // alert(_token);
            // lấy ra số lượng..............
            quantity = [];
            $("input[name='product_qty_order']").each(function(){
                quantity.push($(this).val());
            });
            // lấy ra product_id 
            order_product_id = [];
            $("input[name='order_product_id']").each(function(){
                order_product_id.push($(this).val());
            });
            j = 0;
            for(i = 0;i < order_product_id.length;i++){
                // số lượng khách hàng đặt
                var order_qty = $('.order_qty_' + order_product_id[i]).val();
                // số lượng tồn kho.
                var order_qty_max = $('.order_qty_max_' + order_product_id[i]).val();
                // alert(order_qty);
                // alert(order_qty_max);
                if (parseInt(order_qty) > parseInt(order_qty_max)) {
                    j = j + 1;
                    if (j == 1) {
                        alert('Số lượng bán trong kho không đủ');
                    }
                    $('.color_qty_'+order_product_id[i]).css('background','rgb(229 178 39)');
                }
            }
            if (j == 0){
                $.ajax({
                    url : '{{url('/update-order-qty')}}',
                    method: 'POST',
                    data:{_token:_token, order_status:order_status,order_id:order_id,quantity:quantity,
                        order_product_id:order_product_id},
                    success:function(data){
                        alert('cập nhật số lượng thành công');
                        location.reload();
                    }
                }); 
            }      
        });
    </script>  
    {{-- feeship --}}
    <script type="text/javascript">
        $(document).ready(function(){
            // hiển thị danh sách feeship........................
            fetch_delivery();
            function fetch_delivery(){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url : '{{url('/select-feeship')}}',
                    method: 'POST',
                    data:{_token:_token},
                    success:function(data){
                        $('#load_delivery').html(data);    
                    }
                });
            }
            // cập nhật feeship.................
            $(document).on('blur','.fee_feeship_edit',function(){

                var feeship_id = $(this).data('feeship_id');
                var feeship_price = $(this).text();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url : '{{url('/update-delivery')}}',
                    method: 'GET',
                    data:{feeship_id:feeship_id, feeship_price:feeship_price, _token:_token},
                    success:function(data){
                        fetch_delivery();    
                    }
                });
            });
            // thêm mới feeship...............
            $('.add_delivery').click(function(){

                var city = $('.city').val();
                var province = $('.province').val();
                var wards = $('.wards').val();
                var feeship = $('.feeship').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url : '{{url('/save-delivery')}}',
                    method: 'POST',
                    data:{city:city,province:province,wards:wards,feeship:feeship,_token:_token},
                    success:function(data){
                        fetch_delivery();    
                    }
                });
            });
            // chọn tỉnh,quận huyện,xã phường.........
            $('.choose').on('change',function(){
                var action = $(this).attr('id');
                var ma_id = $(this).val();
                var _token = $('input[name="_token"]').val();
                var result = '';
                if (action=='city'){
                    result = 'province';
                }
                else {
                    result = 'wards';
                }
                $.ajax({
                    url : '{{url('/select-delivery')}}',
                    method: 'POST',
                    data:{action:action,ma_id:ma_id,_token:_token},
                    success:function(data){
                        $('#'+result).html(data);
                    }
                });
            });
        })
    </script>

    @yield('js')
    
</body>
</html>