@extends('admin.admin_newways')
@section('admin_content')

<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px">
            <span class="card-title"> Danh Sách Thương Hiệu Sản Phẩm</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 20px"> 
                        <a href="{{URL::to('/all-brand')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 0px"> 
                        <a href="{{URL::to('/add-brand')}}" class="btn" aria-expanded="false"> Thêm Mới Thương hiệu </a> 
                     </div>
                     <div class="header-navsearch" style="margin: 20px 20px 20px 0px"> 
                        <a href="#" class=" "></a>
                        <form class="form-inline mr-auto-add" action="{{route('search-brand')}}" method="post">
                        @csrf
                          <div class="nav-search"> 
                              <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                              <button class="btn btn-primary-add" type="submit">
                                 <i class="fa fa-search"></i>
                              </button> 
                          </div>
                        </form>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 60px;">Mã</th>
                                 <th class="sorting" style="width: 115px;">Tên Thương Hiệu</th>
                                 <th class="sorting" style="width: 100px;">Ảnh Thương Hiệu</th>
                                 <th class="sorting" style="width: 131px;">Trạng Thái</th>
                                 <th class="sorting" style="width: 105px;">Lựa Chọn</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($all_brand as $key => $show_brand)
                              <tr role="row" class="odd">
                                 {{-- id --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_brand->id}}
                                    </span>
                                 </td>
                                 {{-- tên danh mục --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$show_brand->name}}
                                    </span>
                                 </td>
                                 {{-- ảnh danh mục --}}
                                 <td style="width: 80px;text-align: center;">
                                    <img class="img" style="height: 40px" src="public/img_brand/{{$show_brand->img}}" 
                                    alt="hình ảnh"> 
                                 </td>
                                 {{-- trạng thái --}}
                                 <td style="text-align:center;">
                                    <span>
                                       <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;">
                                             <?php
                                               if($show_brand->status == 0)
                                               {
                                               ?>
                                               <a class="btn btn-xs btn-danger blog" 
                                                href="{{URL::to('unactive-brand/'.$show_brand->id)}}">
                                                  Ẩn
                                                </a>
                                               <?php
                                               }else{
                                               ?>
                                               <a class="btn btn-xs btn-green blog" 
                                                href="{{URL::to('active-brand/'.$show_brand->id)}}">
                                                Hiển Thị
                                                </a>
                                               <?php
                                               }
                                             ?>
                                          </font>
                                       </font>
                                    </span>
                                 </td>
                                 {{-- sửa xóa --}}
                                 <td class="r" style="text-align: center;width: 160px;">
                                    <a class="btn btn-xs btn-info blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    href="{{URL::to('/edit-brand/'.$show_brand->id)}}">
                                       <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                    </a>
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                    href="{{URL::to('/delete-brand/'.$show_brand->id)}}">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">  
                  {{$all_brand->links()}}           
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection('admin_content')