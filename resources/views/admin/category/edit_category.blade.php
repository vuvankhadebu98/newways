@extends('admin.admin_newways')
@section('admin_content')

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Cập Nhật Danh Mục</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-category')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Danh Mục</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-5 align-self-center">
                <div class="customize-input float-right">
                    <select class="custom-select custom-select-set form-control bg-white border-0 custom-shadow custom-radius">
                        <option selected="">Aug 19</option>
                        <option value="1">July 19</option>
                        <option value="2">Jun 19</option>
                    </select>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        @foreach($edit_category as $key => $edit_category)
        <form role="form" action="{{URL::to('/update-category/'.$edit_category->id)}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Danh Mục</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name"
                    value="{{$edit_category->name}}">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{-- ảnh danh mục sản phẩm --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh Danh Mục Sản Phẩm</font>
                   </label> 
                    <input type="file" name="img" class="form-control" id="img">
                    <img src="{{URL::to('/public/img_category/'.$edit_category->img)}}" 
                    height="90" width="120" style="margin-left: 5px">
                    <p class="help is-danger">{{ $errors->first('img') }}</p>
                </div>
                {{-- Từ Khóa --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Từ Khóa Danh Mục</font>
                   </label> 
                    <textarea name="meta_keywords" id="meta_keywords" 
                    style="resize: none;"rows="5"class="form-control">
                    {{$edit_category->meta_keywords}}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('meta_keywords') }}</p>
                </div>
                {{-- mô tả danh mục --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mô Tả Danh Mục</font>
                   </label> 
                    <textarea name="meta_desc" id="meta_desc" 
                    style="resize: none;"rows="8"class="form-control">
                    {{$edit_category->meta_desc}}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('meta_desc') }}</p>
                </div>
                {{-- Trạng thái --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Trạng Thái</font>
                   </label> 
                   <select name="status" class="form-control m-bot15">
                       <option value="0" {{$edit_category->status == 0 ? "selected" : ""}}>Ẩn</option>
                       <option value="1" {{$edit_category->status == 1 ? "selected" : ""}}>Hiển Thị</option>   
                   </select>
                </div>
                {{-- cập nhật --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="update_category" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Cập Nhật Danh Mục</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
        @endforeach
    </div>   

@endsection('admin_content')
@section('js')
    <script>
        CKEDITOR.replace('meta_desc');
         CKEDITOR.replace('meta_keywords');      
    </script>
@endsection