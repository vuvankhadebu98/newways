@foreach($all_contact as $key => $show_contact)
  <tr role="row" class="odd">
     {{-- tên người liên hệ --}}
     <td style="text-align: center;">
        <span >
           {{$show_contact->name}}
        </span>
     </td>
     {{-- email người liên hệ --}}
     <td style="text-align: center;">
        <span>
           {{$show_contact->email}}
        </span>
     </td>

     {{-- nội dung --}}
     <td style="text-align: center;">
        <span>
           {{$show_contact->content}}
        </span>                                 
     </td>
     {{-- trạng thái --}}
     <td style="text-align:center;">
        <span>
           <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">
                 <?php
                   if($show_contact->status == 0)
                   {
                   ?>
                   <a class="btn btn-xs btn-danger blog" 
                    href="{{URL::to('unactive-contact/'.$show_contact->id)}}">
                      Chưa xử lý
                    </a>
                   <?php
                   }else{
                   ?>
                   <a class="btn btn-xs btn-green blog" 
                    href="{{URL::to('active-contact/'.$show_contact->id)}}">
                    Đã xử lý
                    </a>
                   <?php
                   }
                 ?>
              </font>
           </font>
        </span>
     </td>
     {{-- xóa --}}
     <td class="r" style="text-align: center;width: 160px;">
       <?php
         if($show_contact->status == 0)
         {
        ?>
        <a class="btn btn-xs btn-danger blog" 
        style="font-size: 18px;width: 70px; height: 34px;"
        onclick="myFunction()">
           <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
        </a>
        <?php
         }else{
         ?>
        <a class="btn btn-xs btn-danger blog" 
        style="font-size: 18px;width: 70px; height: 34px;"
        onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
        href="{{URL::to('/delete-contact/'.$show_contact->id)}}">
           <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
        </a>
        <?php
         }
       ?>
     </td>
  </tr>
  @endforeach