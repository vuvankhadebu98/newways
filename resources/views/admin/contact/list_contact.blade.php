@extends('admin.admin_newways')
@section('admin_content')

<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px">
            <span class="card-title"> Danh Sách Mã Giảm Giá</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  <div class="row">
                    {{-- tất cả contact --}}
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 20px"> 
                        <a href="{{URL::to('/list-contact')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     {{-- lọc lấy các liên hệ đã sử lý --}}
                     <label class=" search-tag">
                        <p class="btn btn-primary" id="contact_active" onclick="return contact_active(this);" 
                        style="margin-left: -8px;margin-top: 20px;height: 40px;">liện hệ đã xử lý</p>
                     </label>
                     {{-- lọc liên hệ chưa xử lý --}}
                     <label class=" search-tag">
                        <p class="btn btn-primary-2" id="contact_unactive" onclick="return contact_unactive(this);" 
                        style="margin-left: 10px; margin-top: 20px;height: 40px; ">liên hệ chưa xử lý</p>
                     </label>
                     {{-- tìm kiếm  --}}
                     <div class="header-navsearch" style="margin: 20px 20px 20px 10px"> 
                        <a href="#" class=" "></a>
                        <form class="form-inline mr-auto-add" action="{{route('search-contact')}}" method="post">
                        @csrf
                          <div class="nav-search"> 
                              <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                              <button class="btn btn-primary-add" type="submit">
                                 <i class="fa fa-search"></i>
                              </button> 
                          </div>
                        </form>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 115px;">Tên Người Liên Hệ</th>
                                 <th class="sorting" style="width: 131px;">Email Người liên hệ</th>
                                 <th class="sorting" style="width: 131px;">Nội Dung Người liên Hệ</th>
                                  <th class="sorting" style="width: 131px;">Trạng Thái</th>
                                 <th class="sorting" style="width: 105px;">Lựa Chọn</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($all_contact as $key => $show_contact)
                              <tr role="row" class="odd">
                                 {{-- tên người liên hệ --}}
                                 <td style="text-align: center;">
                                    <span >
                                       {{$show_contact->name}}
                                    </span>
                                 </td>
                                 {{-- email người liên hệ --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_contact->email}}
                                    </span>
                                 </td>

                                 {{-- nội dung --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_contact->content}}
                                    </span>                                 
                                 </td>
                                 {{-- trạng thái --}}
                                 <td style="text-align:center;">
                                    <span>
                                       <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;">
                                             <?php
                                               if($show_contact->status == 0)
                                               {
                                               ?>
                                               <a class="btn btn-xs btn-danger blog" 
                                                href="{{URL::to('unactive-contact/'.$show_contact->id)}}">
                                                  Chưa xử lý
                                                </a>
                                               <?php
                                               }else{
                                               ?>
                                               <a class="btn btn-xs btn-green blog" 
                                                href="{{URL::to('active-contact/'.$show_contact->id)}}">
                                                Đã xử lý
                                                </a>
                                               <?php
                                               }
                                             ?>
                                          </font>
                                       </font>
                                    </span>
                                 </td>
                                 {{-- xóa --}}
                                 <td class="r" style="text-align: center;width: 160px;">
                                   <?php
                                     if($show_contact->status == 0)
                                     {
                                    ?>
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    onclick="myFunction()">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                    <?php
                                     }else{
                                     ?>
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                    href="{{URL::to('/delete-contact/'.$show_contact->id)}}">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                    <?php
                                     }
                                   ?>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">  
                  {{$all_contact->links()}}           
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
  {{-- lọc các liên hệ đã sử lý --}}
   function contact_active(e) {
      $.ajax({
      url: '{{route('load.contact.active')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   // lọc các liên hê chưa được sử lý
   function contact_unactive(e) {
      $.ajax({
      url: '{{route('load.contact.unactive')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
</script> 
{{-- xử lý chưa xác Nhân liên hệ --}}
<script>
function myFunction() {
  alert("bạn chưa xử lý liên hệ nên không thể xóa ! Vui Lòng xử lý liên hệ.");
}
</script> 
@endsection('admin_content')