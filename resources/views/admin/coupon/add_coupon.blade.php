@extends('admin.admin_newways')
@section('admin_content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Thêm Mới Mã Giảm Giá</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-coupon')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách mã Giảm Giá</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        <form role="form" action="{{URL::to('/save-coupon')}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
              {{-- tên mã giảm giá --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Mã Giảm Giá</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{--mã giảm giá --}}
                 <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mã Giảm Giá</font>
                   </label> 
                    <input type="text" name="code" class="form-control" id="code">
                    <p class="help is-danger">{{ $errors->first('code') }}</p>
                </div>
                {{-- số lượng mã --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Số Lượng Mã</font>
                   </label> 
                    <input type="number" name="qty" class="form-control" id="qty">
                    <p class="help is-danger">{{ $errors->first('qty') }}</p>
                </div>
                {{-- tính năng mã --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tính Năng Mã</font>
                   </label> 
                    <select name="check" class="form-control m-bot15">
                       <option value="0">----Lựa Chọn----</option>
                       <option value="1">Giảm Theo %</option>
                       <option value="2">Giảm Theo Tiền</option>
                   </select>
                </div>
                {{-- nhập số phần trăm mã --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Nhập Số Phần Trăng hoặc tiền giảm</font>
                  </label> 
                  <input type="number" name="number" class="form-control" id="number">
                  <p class="help is-danger">{{ $errors->first('coupon_id') }}</p>
                </div>
                {{-- thêm và hủy --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="add_coupon" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Thêm Mã Giảm Giá </font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
    </div>   

@endsection