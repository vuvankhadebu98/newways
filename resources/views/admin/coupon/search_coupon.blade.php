@extends('admin.admin_newways')
@section('admin_content')

<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px">
            <span class="card-title"> Danh Sách Mã Giảm Giá</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 20px"> 
                        <a href="{{URL::to('/all-coupon')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 0px"> 
                        <a href="{{URL::to('/add-coupon')}}" class="btn" aria-expanded="false"> Thêm Mới Mã Giảm Giá </a> 
                     </div>
                     <div class="header-navsearch" style="margin: 20px 20px 20px 0px"> 
                        <a href="#" class=" "></a>
                        <form class="form-inline mr-auto-add" action="{{route('search-coupon')}}" method="post">
                        @csrf
                          <div class="nav-search"> 
                              <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                              <button class="btn btn-primary-add" type="submit">
                                 <i class="fa fa-search"></i>
                              </button> 
                          </div>
                        </form>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 60px;">Mã</th>
                                 <th class="sorting" style="width: 115px;">Tên Mã Giảm Giá</th>
                                 <th class="sorting" style="width: 100px;">Mã Giảm Giá</th>
                                 <th class="sorting" style="width: 131px;">Số lượng Mã</th>
                                 <th class="sorting" style="width: 105px;">Tính Năng Mã</th>
                                 <th class="sorting" style="width: 131px;">Số Phần Trăm Hoặc Tiền Giảm</th>
                                 <th class="sorting" style="width: 105px;">Lựa Chọn</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($all_coupon as $key => $show_coupon)
                              <tr role="row" class="odd">
                                 {{-- id --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_coupon->id}}
                                    </span>
                                 </td>
                                 {{-- tên mã giảm giá --}}
                                 <td style="text-align: center;">
                                    <span >
                                       {{$show_coupon->name}}
                                    </span>
                                 </td>
                                 {{-- mã giảm giá --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$show_coupon->code}}
                                    </span>
                                 </td>
                                 {{-- số lượng mã --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_coupon->qty}}
                                    </span>
                                 </td>
                                 {{-- Tính năng mã--}}
                                 <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">
                                         <?php
                                           if($show_coupon->check == 1)
                                           {
                                           ?>
                                            <span class="hienthi" id="0" style="color: #0DD902;">Giảm Theo %</span>
                                           <?php
                                           }elseif($show_coupon->check == 2){
                                           ?>
                                            <span class="hienthi" id="1" style="color: #09B5C8;">Giảm Theo Tiền</span>
                                           <?php
                                           }
                                         ?> 
                                      </font>
                                   </font>
                                 </td>
                                 {{-- số phần trăm hoặc tiền giảm --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       <?php
                                           if($show_coupon->check == 1)
                                           {
                                           ?>
                                            <span class="hienthi" id="0" style="color: #0DD902;">Giảm {{$show_coupon->number}} %</span>
                                           <?php
                                           }elseif($show_coupon->check == 2){
                                           ?>
                                            <span class="hienthi" id="1" style="color: #09B5C8;">Giảm {{number_format($show_coupon->number),0,',','.'}} vnđ</span>
                                           <?php
                                           }
                                         ?> 
                                    </span>
                                 </td>
                                 {{-- xóa --}}
                                 <td class="r" style="text-align: center;width: 160px;">
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 30px;"
                                    onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                    href="{{URL::to('/delete-coupon/'.$show_coupon->id)}}">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">       
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection('admin_content')