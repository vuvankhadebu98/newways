@extends('admin.admin_newways')
@section('admin_content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Thêm Mới Vận Chuyển</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-delivery')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Vận Chuyển</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        <form>
          @csrf
            <div class="card-body" style="margin-top: 20px">
              {{-- Thên thành phố --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Chọn Thành Phố</font>
                   </label> 
                   <select name="city" id="city" class="form-control m-bot15 choose city">
                       <option value="">--Chọn Tỉnh Thành Phố--</option>
                       @foreach($city as $key => $ci)
                          <option value="{{$ci->matp}}">{{$ci->name}}</option> 
                       @endforeach  
                   </select>
                </div>
                {{-- chọn quận huyện --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Chọn Quận Huyện</font>
                   </label> 
                   <select name="province" id="province" class="form-control m-bot15 choose province">
                       <option value="">--Chọn Quận Huyện--</option>
                   </select>
                </div>
                {{-- chọn xã phường --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Chọn Xã Phường</font>
                   </label> 
                   <select name="wards" id="wards" class="form-control m-bot15 wards">
                       <option value="">--Chọn Xã Phường--</option>  
                   </select>
                </div>
                {{-- phí vận chuyển --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Phí Vận Chuyển</font>
                   </label> 
                    <input type="text" name="feeship" class="form-control feeship" id="name">
                    <p class="help is-danger">{{ $errors->first('feeship') }}</p>
                </div>
                {{-- thêm và hủy --}}
                <div class="card-body">
                    <div class="text-right-feeship">
                        <button type="button" name="add_delivery" 
                            class="btn btn-primary waves-effect waves-light add_delivery">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Thêm Phí Vận Chuyển</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
    </div>
    <div class="card" style="margin-top: -10px" id="load_delivery">
      {{-- danh sách feeship --}}
    </div>   
@endsection