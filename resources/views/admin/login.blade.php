<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#162946">
    <meta name="theme-color" content="#e67605">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"> <!-- Title -->
    <title>Login : TK</title>
    <link href="public/backend/assets/plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet"> <!-- Sidemenu Css -->
    <link href="public/backend/assets/css/sidemenu.css" rel="stylesheet"> <!-- Switcher css -->
    <link href="public/backend/assets/switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all">
    <!-- Dashboard css -->
    <link href="public/backend/assets/css/style.css" rel="stylesheet">
    <link href="public/backend/assets/css/admin-custom.css" rel="stylesheet"> <!-- c3.js Charts Plugin -->
    <link href="public/backend/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet">
    <!---Font icons-->
    <link href="public/backend/assets/css/icons.css" rel="stylesheet"> <!-- Color-Skins -->
    <link id="theme" rel="stylesheet" type="text/css" media="all" href="public/backend/assets/colorskins/color-skins/color13.css">
    <link rel="stylesheet" href="public/backend/assets/colorskins/demo.css">
  
    <meta http-equiv="imagetoolbar" content="no">
    <style type="text/css">
        <!-- input,textarea{-webkit-touch-callout:default;-webkit-user-select:auto;-khtml-user-select:auto;-moz-user-select:text;-ms-user-select:text;user-select:text} *{-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:-moz-none;-ms-user-select:none;user-select:none} 
        -->
    </style>
    <style type="text/css" media="print">
        <!-- body{display:none} 
        -->
    </style>
    <!--[if gte IE 5]><frame></frame><![endif]-->
    <style type="text/css">
        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }

    </style>
</head>

<body class="construction-image">
   
    <div id="global-loader" style="display: none;"> <img src="public/backend/assets/images/logolaptop.jpg" class="loader-img " alt="">
    </div>
    <!--/Loader-->
    <!--Page-->
    <div class="page page-h">
        <div class="page-content z-index-10">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-12 col-md-12 d-block mx-auto">
                        <div class="card box-shadow-0 mb-0">
                            <div class="card-header">
                                <h3 class="card-title">Đăng Nhập</h3>
                            </div>
                            <?php 
                                $message = Session::get('message');
                                if ($message) {
                                    echo '<span class="text-alert" 
                                    style="text-align:center;color: red;">'. $message .'</span>';
                                    Session::put('message', null);
                                }
                            ?>
                            <form action="{{URL::to('/admin-login')}}" method="post">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group"> 
                                        <label class="form-label text-dark">Tài khoản</label>
                                        <input type="email" class="form-control" name="admin_email" placeholder="Nhập tên tài khoản">
                                        <p class="help is-danger">{{ $errors->first('admin_email') }}</p> 
                                    </div>
                                    <div class="form-group"> 
                                        <label class="form-label text-dark">Mật khẩu</label> 
                                        <input type="password" class="form-control" id="exampleInputPassword1"
                                            placeholder="Nhập mật khẩu" name="admin_password"> 
                                        <p class="help is-danger">{{ $errors->first('admin_password') }}</p>
                                    </div>
                                    <div class="form-group"> 
                                        <label class="custom-control custom-checkbox"> 
                                            <a href="forgot-password.html" class="float-right small text-dark mt-1">Quên mật khẩu</a>
                                            <input type="checkbox" class="custom-control-input">
                                            <span class="custom-control-label text-dark">Nhớ đăng nhập</span> 
                                        </label>
                                    </div>
                                    {{-- đăng nhập --}}
                                    <div class="form-footer mt-2"> 
                                        <input type="submit" value="Đăng nhập" class="btn btn-primary btn-block"></input>
                                    </div>
                                    <div class="form-footer mt-2"> 
                                        <a href="{{URL::to('/register-author')}}" class="btn btn-success btn-block">
                                         Đăng Ký
                                        </a>
                                    </div>
                                    {{-- đăng nhập bằng các cái khác --}}
                                    <div class="form-footer mt-1"> 
                                        <a href="{{URL::to('login-facebook')}}" class="btn btn-secondary-1">
                                            <i class="fa fa-facebook"></i>&nbsp; Facebook</a>
                                        <a href="#" class="btn btn-info-1">
                                            <i class="fa fa-twitter"></i>&nbsp; Twitter</a>
                                        <a href="{{URL::to('login-google')}}" class="btn btn-danger-1">
                                            <i class="fa fa-google"></i>&nbsp; Google</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--/Page-->
    <script src="public/backend/assets/js/vendors/jquery-3.2.1.min.js"></script>

    <script src="public/backend/assets/plugins/bootstrap-4.3.1-dist/js/popper.min.js"></script>

    <script src="public/backend/assets/plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
   
    <script src="public/backend/assets/js/vendors/jquery.sparkline.min.js"></script>
  
    <script src="public/backend/assets/js/vendors/circle-progress.min.js"></script>
    
    <script src="public/backend/assets/plugins/rating/jquery.rating-stars.js"></script>
    
    <script src="public/backend/assets/plugins/toggle-sidebar/sidemenu.js"></script>
    
    <script src="public/backend/assets/plugins/counters/counterup.min.js"></script>
   
    <script src="public/backend/assets/plugins/counters/waypoints.min.js"></script>
   
    <script src="public/backend/assets/js/admin-custom.js"></script>
   
    <script src="public/backend/assets/switcher/js/switcher.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>

</html>
