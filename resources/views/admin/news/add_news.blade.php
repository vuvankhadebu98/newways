@extends('admin.admin_newways')
@section('admin_content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Thêm Mới Tin Tức</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-news')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Tin Tức</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        <form role="form" action="{{URL::to('/save-news')}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
                {{-- author --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Họ và tên(người đăng)</font>
                   </label> 
                    <input type="text" name="author" class="form-control" id="author">
                    <p class="help is-danger">{{ $errors->first('author') }}</p>
                </div>
                {{-- name --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Tin Tức</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{-- ảnh tin tức --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh tin tức</font>
                   </label> 
                    <input type="file" name="img" class="form-control" id="img">
                    <p class="help is-danger">{{ $errors->first('img') }}</p>
                </div>
                {{-- mô tả tin tức --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mô Tả Tin Tức</font>
                   </label> 
                    <textarea class="form-control" rows="5" id="editor1" 
                        placeholder="nhập mô tả.." name="desc" type="text" value="">
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('desc') }}</p>
                </div>
                {{-- nội dung tin tức --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Nội Dung Tin Tức</font>
                   </label> 
                    <textarea name="content" id="content" 
                    style="resize: none;"rows="8"class="form-control">
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('content') }}</p>
                </div>
                {{-- trạng thái tin túc --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Trạng Thái</font>
                   </label> 
                   <select name="status" class="form-control m-bot15">
                       <option value="0">Ẩn</option>
                       <option value="1">Hiển Thị</option>   
                   </select>
                </div>
                {{-- thêm mới --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="add_news" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Thêm Tin Tức</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
    </div>   

@endsection
@section('js')
    <script>
        CKEDITOR.replace('content');   
        CKEDITOR.replace('desc');   
    </script>
@endsection