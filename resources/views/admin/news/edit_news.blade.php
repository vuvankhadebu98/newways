@extends('admin.admin_newways')
@section('admin_content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Thêm Mới Tin Tức</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-news')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Tin Tức</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        @foreach($edit_news as $key => $edit_ns)
        <form role="form" action="{{URL::to('/update-news/'.$edit_ns->id)}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
                {{-- author --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Họ và tên(người đăng)</font>
                   </label> 
                    <input type="text" name="author" class="form-control" id="author" value="{{$edit_ns->author}}">
                    <p class="help is-danger">{{ $errors->first('author') }}</p>
                </div>
                {{-- name --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Tin Tức</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name" value="{{$edit_ns->name}}">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{-- ảnh tin tức --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh tin tức</font>
                   </label> 
                    <input type="file" name="img" class="form-control" id="img">
                    <img src="{{URL::to('/img_news/'.$edit_ns->img)}}" 
                    height="70" width="100" style="margin-left: 5px">
                </div>
                {{-- mô tả tin tức --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mô Tả Tin Tức</font>
                   </label> 
                    <textarea class="form-control" rows="5" id="editor1" 
                        placeholder="nhập mô tả.." name="desc" type="text" value="">
                        {{$edit_ns->desc}}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('desc') }}</p>
                </div>
                {{-- nội dung tin tức --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Nội Dung Tin Tức</font>
                   </label> 
                    <textarea name="content" id="content" 
                    style="resize: none;"rows="8"class="form-control">
                    {{$edit_ns->content}}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('content') }}</p>
                </div>
                {{-- trạng thái tin túc --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Trạng Thái</font>
                   </label> 
                   <select name="status" class="form-control m-bot15">
                       <option value="0"{{$edit_ns->status == 0 ? "selected" : ""}}>Ẩn</option>
                       <option value="1"{{$edit_ns->status == 1 ? "selected" : ""}}>Hiển Thị</option>   
                   </select>
                </div>
                {{-- cập nhật --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="update_news" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Cập Nhật Tin Tức</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
        @endforeach
    </div>   

@endsection
@section('js')
    <script>
        CKEDITOR.replace('content');   
        CKEDITOR.replace('desc');   
    </script>
@endsection