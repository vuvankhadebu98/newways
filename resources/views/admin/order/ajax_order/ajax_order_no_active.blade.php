@php 
$i = 0;
@endphp
  @foreach($all_order as $key => $order)
  @php
  $i++;
  @endphp
  <tr role="row" class="odd">
    {{-- stt --}}
     <td style="text-align: center;">
          <span style="color: red;">
           {{$i}}
        </span>
     </td>
     {{-- code --}}
     <td style="text-align: center;">
        <span>
           {{$order->code}}
        </span>
     </td>
     {{-- trạng thái --}}
     <td style="text-align:center;">
        <span>
           <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">
                 <?php
               if($order->status == 1)
               {
               ?>
                <span class="hienthi" id="1" style="color: #ea2020;">Chưa xử lý</span>
               <?php
               }elseif($order->status == 2){
               ?>
                <span class="hienthi" id="2" style="color: #04B404;">Đã xử lý - Đã giao hàng</span>
               <?php
               }elseif($order->status == 3){
               ?>
                <span class="hienthi" id="3" style="color: #BDBDBD;">Hủy đơn hàng - Tạm giữ</span>
               <?php
               }
             ?>
              </font>
           </font>
        </span>
     </td>
     {{-- ngày đặt hàng --}}
     <td style="text-align: center;">
        <span>
           {{$order->created_at}}
        </span>
     </td>
     {{-- sửa xóa --}}
     <td class="r" style="text-align: center;width: 160px;">
        <a class="btn btn-xs btn-info blog" 
        style="font-size: 18px;width: 100px; height: 34px;"
        href="{{URL::to('/show-order/'.$order->code)}}">
           <i class="fa fa-eye"style="margin-top: 6px"></i> Chi Tiết
        </a>
        <a class="btn btn-xs btn-danger blog" 
        style="font-size: 18px;width: 70px; height: 34px;"
        onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
        href="{{URL::to('/delete-order/'.$order->code)}}">
           <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
        </a>
     </td>
  </tr>
  @endforeach