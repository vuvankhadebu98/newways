@extends('admin.admin_newways')
@section('admin_content')

{{-- Thông Tin Khách Hàng --}}
<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px;margin-bottom: 20px;text-align: center;">
            <span class="card-title">
            <p>Thông Tin Khách Hàng Đăng Nhập</p>
        	</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  {{-- chi tiết Đơn hàng --}}
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 60px;">Tên Khách Hàng</th>
                                 <th class="sorting" style="width: 115px;">Số Điện Thoại</th>
                                 <th class="sorting" style="width: 131px;">Địa Chỉ</th>
                                 <th class="sorting" style="width: 105px;">email</th>
                              </tr>
                           </thead>
                           <tbody>                           
                              <tr role="row" class="odd">
                                 {{-- Tên Khách Hàng --}}
                                  <td style="text-align: center;">
                                    {{$customer->name}}
                                 </td>
                                  {{-- Số Điện Thoại --}}
                                  <td style="text-align: center;">
                                    {{$customer->phone}}
                                 </td>
                                 {{-- Địa Chỉ --}}
                                 <td style="text-align: center;">
                                    <span >
                                       {{$customer->address}}
                                    </span>
                                 </td>
                                 {{-- email --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$customer->email}}
                                    </span>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">      
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
{{-- Thông tin vận chuyển --}}
<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px;margin-bottom: 20px;text-align: center;">
            <span class="card-title">
            	<p>Thông Tin Người Nhận Hàng</p>
            </span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  {{-- chi tiết Đơn hàng --}}
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 60px;">Tên Người Nhận Hàng</th>
                                 <th class="sorting" style="width: 60px;">Số Điện Thoại</th>
                                 <th class="sorting" style="width: 115px;">Địa Chỉ</th>
                                 <th class="sorting" style="width: 105px;">Email</th>
                                 <th class="sorting" style="width: 105px;">Ghi Chú</th>
                                 <th class="sorting" style="width: 105px;">Hình Thức Thanh Toán</th>
                              </tr>
                           </thead>
                           <tbody>                              
                              <tr role="row" class="odd">
                                 {{-- Tên người vận Chuyền --}}
                                  <td style="text-align: center;">
                                     {{$shipping->name}}
                                  </td>
                                  {{-- Số điện thoại--}}
                                  <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                       <font style="vertical-align: inherit;color: #C70D03;">
                                          {{$shipping->phone}}
                                       </font>
                                    </font>
                                 </td>
                                 {{-- Địa Chỉ --}}
                                 <td style="text-align: center;">
                                    <span >
                                       {{$shipping->address}}
                                    </span>
                                 </td>
                                 {{-- Email --}}
                               	 <td style="text-align: center;">
                                    <span >
                                       {{$shipping->email}}
                                    </span>
                                 </td>
                                 {{-- Ghi chú --}}
                                 <td style="text-align: center;">
                                    <span >
                                       {{$shipping->notes}}
                                    </span>
                                 </td>
                                 {{-- thanh toán --}}
                                 <td style="text-align: center;">
                                   <?php
                                     if($shipping->method == 0)
                                     {
                                     ?>
                                      <span class="hienthi" id="0" style="color: #0DD902;">Bằng Thẻ </span>
                                     <?php
                                     }elseif($shipping->method == 1){
                                     ?>
                                      <span class="hienthi" id="1" style="color: #09B5C8;">Bằng Tiền Mặt</span>
                                     <?php
                                     }
                                   ?> 
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">      
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px;margin-bottom: 20px;text-align: center;">
            <span class="card-title">
            	<p>Chi Tết Đơn Hàng</p>
            </span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  {{-- chi tiết Đơn hàng --}}
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 30px;">STT</th> 
                                 <th class="sorting" style="width: 60px;">Tên Sản Phẩm</th>
                                 <th class="sorting" style="width: 60px;">Số Lượng Kho Còn</th>
                                 <th class="sorting" style="width: 60px;">Mã Giảm Giá</th>
                                 <th class="sorting" style="width: 60px;">Phí Vận Chuyển</th>
                                 <th class="sorting" style="width: 20px;">Số Lượng</th>
                                 <th class="sorting" style="width: 115px;">Giá</th>
                                 <th class="sorting" style="width: 131px;">Tổng Tiền</th>
                              </tr>
                           </thead>
                           <tbody>
                           @php
                            $i = 0;
                            $total = 0;
                           @endphp    
                           @foreach($order_details as $key => $order_detail)
                           @php 
                            $i++;
                            $subtotal = $order_detail->product_qty*$order_detail->product_price;
                            $total += $subtotal; 
                           @endphp                          
                              <tr role="row" class="odd color_qty_{{$order_detail->product_id}}">
                                 <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                       <font style="vertical-align: inherit;color: #C70D03;">
                                          {{$i}}
                                       </font>
                                    </font>
                                 </td>
                                 {{-- Tên Sản Phẩm --}}
                                  <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                       <font style="vertical-align: inherit;">
                                          {{$order_detail->product_name}}
                                       </font>
                                    </font>
                                 </td>
                                 {{-- Số Lượng kho còn --}}
                                  <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                       <font style="vertical-align: inherit;">
                                          {{$order_detail->product->qty}}
                                       </font>
                                    </font>
                                 </td>
                                 {{-- Mã giảm giá --}}
                                  <td style="text-align: center;">
                                    @if($order_detail->product_coupon!='Không có mã giảm giá')
                                      {{$order_detail->product_coupon}}
                                    @else
                                      Không có mã giảm giá
                                    @endif
                                 </td>
                                 {{-- phí vận chuyển --}}
                                 <td>
                                   {{number_format($order_detail->product_feeship)}} đ
                                 </td>
                                  {{-- Số Lượng --}}
                                  <td style="text-align: center;">
                                      <input type="number" class="order_qty_{{$order_detail->product_id}}" 
                                      {{$order_status==2 ? 'disabled' : ''}} min=1 value="{{$order_detail->product_qty}}" name="product_qty_order"
                                      style="width: 80px"> 
                                      <input type="hidden" name="order_qty_max" class="order_qty_max_{{$order_detail->product_id}}" 
                                      value="{{$order_detail->product->qty}}">
                                      <input type="hidden" name="order_code" class="order_code" 
                                      value="{{$order_detail->order_code}}">
                                      <input type="hidden" name="order_product_id" class="order_product_id" 
                                      value="{{$order_detail->product_id}}">
                                      @if($order_status != 2)
                                      <button class="btn btn-default update_qty_order" data-product_id="{{$order_detail->product_id}}" 
                                        name="update_qty_order">Cập nhật</button>
                                      @endif
                                 </td>
                                 {{-- giá --}}
                                 <td style="text-align: center;">
                                    <span >
                                       {{number_format($order_detail->product_price,0,',','.')}} đ
                                    </span>
                                 </td>
                                 {{-- Tổng Giá --}}
                                 <td style="text-align:center;">
                                    <span>
                                       <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;color: #C70D03">
                                             {{number_format($subtotal,0,',','.')}} đ
                                          </font>
                                       </font>
                                    </span>
                                    
                                 </td>
                              </tr>
                            @endforeach
                            <tr>
                              <td colspan="8" style="text-align: left;">
                                @php
                                $total_coupon = 0;
                                @endphp
                                @if($coupon_check ==1)
                                  @php
                                    $total_after_coupon = ($total*$coupon_number)/100;
                                    echo 'Tổng Giảm: '.number_format($total_after_coupon,0,',','.').' đ';
                                    $total_coupon = $total - $total_after_coupon + $order_detail->product_feeship;
                                  @endphp  
                                @else
                                  @php
                                  echo 'Tổng Giảm: '.number_format($coupon_number,0,',','.').' đ';
                                    $total_coupon = $total - $coupon_number + $order_detail->product_feeship;
                                  @endphp
                                @endif

                                <br>
                                Phí vận chuyển : 
                                  <span>
                                    {{number_format($order_detail->product_feeship,0,',','.')}} đ
                                  </span>
                                <br>
                                Tổng thanh toán: 
                                <span style="color: #f77e28;">
                                  {{number_format($total_coupon,0,',','.')}} đ
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="8" style="text-align: left;">
                                @foreach($order as $key =>$or)
                                  @if($or->status==1)
                                    <form action="">
                                      @csrf
                                      <select class="form-control col-lg-3 order_detail_product" name="" id="">
                                        <option value="">--chọn hình thức đơn hàng--</option>
                                        <option id="{{$or->id}}" selected value="1">Chưa xử lý</option>
                                        <option id="{{$or->id}}" value="2">Đã xử lý - Đã giao hàng</option>
                                        <option id="{{$or->id}}" value="3">Hủy đơn hàng - Tạm giữ</option>
                                      </select>
                                    </form>
                                  @elseif($or->status==2)
                                    <form action="">
                                      @csrf
                                      <select class="form-control col-lg-3 order_detail_product" name="" id="">
                                        <option value="">--chọn hình thức đơn hàng--</option>
                                        <option id="{{$or->id}}" value="1">Chưa xử lý</option>
                                        <option id="{{$or->id}}" selected value="2">Đã xử lý - Đã giao hàng</option>
                                        <option id="{{$or->id}}" value="3">Hủy đơn hàng - Tạm giữ</option>
                                      </select>
                                    </form>
                                  @else
                                    <form action="">
                                      @csrf
                                      <select class="form-control col-lg-3 order_detail_product" name="" id="">
                                        <option value="">--chọn hình thức đơn hàng--</option>
                                        <option id="{{$or->id}}" value="1">Chưa xử lý</option>
                                        <option id="{{$or->id}}" value="2">Đã xử lý - Đã giao hàng</option>
                                        <option id="{{$or->id}}" selected value="3">Hủy đơn hàng - Tạm giữ</option>
                                      </select>
                                    </form>
                                  @endif
                                @endforeach  
                              </td>
                            </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">
                  </div>
               </div>    
            </div>
            <a class="btn btn-xs btn-info blog print-order" target="_blank" 
              href="{{URL::to('/print-order/'.$order_detail->order_code)}}">
              <i class="fa fa-eye"style="margin-top: 6px"></i>
              In đơn hàng
            </a> 
         </div>
      </div>
   </div>
</div>
@endsection()