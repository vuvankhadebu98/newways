@extends('admin.admin_newways')
@section('admin_content')

<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px">
            <span class="card-title"> Danh Sách Đơn Hàng</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 20px"> 
                        <a href="{{URL::to('view-order')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     {{-- lọc đơn hàng chưa duyệt --}}
                     <label class=" search-order">
                        <p class="btn btn-primary-2" id="unactive_order" onclick="return unactive_order(this);" 
                          style="margin-left: -5px; margin-top: 21px; ">Đơn hàng chưa duyệt</p>
                     </label>
                     {{-- lọc đơn hàng đã duyệt--}}
                     <label class=" search-order">
                        <p class="btn btn-primary" id="active_order" onclick="return active_order(this);" 
                          style=" margin-left: 15px; margin-top: 21px; ">Đơn hàng đã duyệt</p>
                     </label>
                     {{-- lọc đơn hàng hủy chưa duyệt --}}
                     <label class=" search-order">
                        <p class="btn btn-primary-3" id="no_active_order" onclick="return no_active_order(this);" 
                          style="margin-left: 15px; margin-top: 21px; ">Đơn hàng bị hủy</p>
                     </label>
                     {{-- tìm kiếm --}}
                     <div class="header-navsearch" style="margin: 20px 20px 20px 15px"> 
                        <a href="#" class=" "></a>
                        <form class="form-inline mr-auto-add" action="{{route('search-order')}}" method="post">
                        @csrf
                          <div class="nav-search"> 
                              <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                              <button class="btn btn-primary-add" type="submit">
                                 <i class="fa fa-search"></i>
                              </button> 
                          </div>
                        </form>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 20px;">STT</th>
                                 <th class="sorting" style="width: 60px;">Mã Đơn Hàng</th>
                                 <th class="sorting" style="width: 60px;">Tình Trạng Đơn Hàng</th>
                                 <th class="sorting" style="width: 60px;">Ngày Đặt Hàng</th>
                                 <th class="sorting" style="width: 60px;">Lựa Chọn</th>
                              </tr>
                           </thead>
                           <tbody>
                            @php 
                            $i = 0;
                            @endphp
                              @foreach($all_order as $key => $order)
                              @php
                              $i++;
                              @endphp
                              <tr role="row" class="odd">
                                {{-- stt --}}
                                 <td style="text-align: center;">
                                      <span style="color: red;">
                                       {{$i}}
                                    </span>
                                 </td>
                                 {{-- code --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$order->code}}
                                    </span>
                                 </td>
                                 {{-- trạng thái --}}
                                 <td style="text-align:center;">
                                    <span>
                                       <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;">
                                             <?php
                                           if($order->status == 1)
                                           {
                                           ?>
                                            <span class="hienthi" id="1" style="color: #ea2020;">Chưa xử lý</span>
                                           <?php
                                           }elseif($order->status == 2){
                                           ?>
                                            <span class="hienthi" id="2" style="color: #04B404;">Đã xử lý - Đã giao hàng</span>
                                           <?php
                                           }elseif($order->status == 3){
                                           ?>
                                            <span class="hienthi" id="3" style="color: #BDBDBD;">Hủy đơn hàng - Tạm giữ</span>
                                           <?php
                                           }
                                         ?>
                                          </font>
                                       </font>
                                    </span>
                                 </td>
                                 {{-- ngày đặt hàng --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$order->created_at}}
                                    </span>
                                 </td>
                                 {{-- sửa xóa --}}
                                 <td class="r" style="text-align: center;width: 160px;">
                                    <a class="btn btn-xs btn-info blog" 
                                    style="font-size: 18px;width: 100px; height: 34px;"
                                    href="{{URL::to('/show-order/'.$order->code)}}">
                                       <i class="fa fa-eye"style="margin-top: 6px"></i> Chi Tiết
                                    </a>
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                    href="{{URL::to('/delete-order/'.$order->code)}}">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">  
                  {{$all_order->links()}}           
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
  {{-- active order --}}
    function active_order(e) {
      $.ajax({
      url: '{{route('load.order.active')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   // unactive order.................
   function unactive_order(e) {
      $.ajax({
      url: '{{route('load.order.unactive')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
   // no_active order
   function no_active_order(e) {
      $.ajax({
      url: '{{route('load.order.no.active')}}',
      method: 'GET',
      success: function(data){
         $("table#example tbody").empty();
         $("table#example tbody").html(data);
         console.log(data);
         }
      });
   }
</script>
@endsection('admin_content')