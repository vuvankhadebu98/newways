
  @foreach($all_product as $key => $show_product)
  <tr role="row" class="odd">
     {{-- id --}}
     <td style="text-align: center;">
        <span>
           {{$show_product->id}}
        </span>
     </td>
     {{-- admin_id --}}
      <td style="text-align: center;">
        <font style="vertical-align: inherit;">
           <font style="vertical-align: inherit;color: #C70D03;">
            {{$show_product->admin->admin_name}}
           </font>
        </font>
     </td>
     {{-- brand_id --}}
      <td style="text-align: center;">
        <font style="vertical-align: inherit;">
           <font style="vertical-align: inherit;color: #C70D03;">
              {{$show_product->brand->name}}
           </font>
        </font>
     </td>
     {{-- tên sản phẩm --}}
     <td style="text-align: center;">
        <span style="color: #C70D03">
           {{$show_product->name}}
        </span>
     </td>
     {{-- mô tả --}}
     <td style="text-align: center;">
        <span>
           {{$show_product->desc}}
        </span>
     </td>
     {{-- ảnh danh mục --}}
     <td style="width: 160px;text-align: center;">
        <img class="img" src="public/img_product/{{$show_product->img}}" 
        alt="hình ảnh"> 
     </td>
     {{-- giá --}}
     <td style="text-align: center;">
        <span style="color: #C70D03">
           {{$show_product->price}}
        </span>
     </td>
     {{-- số lượng --}}
       <td style="text-align: center;">
          <span style="color: #C70D03">
             {{$show_product->qty}}
          </span>
       </td>
     {{-- kiểm tra --}}
     <td style="text-align: center;">
        <font style="vertical-align: inherit;">
          <font style="vertical-align: inherit;">
             <?php
               if($show_product->check == 0)
               {
               ?>
                <span class="hienthi" id="0" style="color: #0DD902;">Hết Hàng</span>
               <?php
               }elseif($show_product->check == 1){
               ?>
                <span class="hienthi" id="1" style="color: #09B5C8;">Còn Hàng</span>
               <?php
               }
             ?> 
          </font>
       </font>
     </td>
     {{-- tình Trạng Hàng --}}
     <td style="text-align: center;">
        <font style="vertical-align: inherit;">
          <font style="vertical-align: inherit;">
             <?php
               if($show_product->check_new == 0)
               {
               ?>
                <span class="hienthi" id="0" style="color: #0DD902;">Đập Hộp</span>
               <?php
               }elseif($show_product->check_new == 1){
               ?>
                <span class="hienthi" id="1" style="color: #09B5C8;">Mới</span>
               <?php
               }elseif($show_product->check_new == 2){
               ?>
                <span class="hienthi" id="2" style="color: #FFA54F;">99%</span>
               <?php
               }
             ?> 
          </font>
       </font>
     </td>
     {{-- trạng thái --}}
     <td style="text-align:center;">
        <span>
           <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">
                 <?php
                   if($show_product->status == 0)
                   {
                   ?>
                   <a class="btn btn-xs btn-danger blog" 
                    href="{{URL::to('unactive-product/'.$show_product->id)}}">
                      Ẩn
                    </a>
                   <?php
                   }else{
                   ?>
                   <a class="btn btn-xs btn-green blog" 
                    href="{{URL::to('active-product/'.$show_product->id)}}">
                    Hiển Thị
                    </a>
                   <?php
                   }
                 ?>
              </font>
           </font>
        </span>
     </td>
     {{-- sửa xóa --}}
     <td class="r" style="text-align: center;width: 160px;">
        <a class="btn btn-xs btn-info blog" 
        style="font-size: 18px;width: 70px; height: 34px;"
        href="{{URL::to('/edit-product/'.$show_product->id)}}">
           <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
        </a>
        <a class="btn btn-xs btn-danger blog" 
        style="font-size: 18px;width: 70px; height: 34px;"
        onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
        href="{{URL::to('/delete-product/'.$show_product->id)}}">
           <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
        </a>
     </td>
  </tr>
  @endforeach