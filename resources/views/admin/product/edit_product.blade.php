@extends('admin.admin_newways')
@section('admin_content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Cập Nhật Sản Phẩm</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-category')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Sản Phẩm</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
      @foreach($edit_product as $key => $edit_product)
        <form role="form" action="{{URL::to('/update-product/'.$edit_product->id)}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
                {{-- user_id --}}
                <div class="form-group">
                   <label class="form-label" name="exampleInputPassword1">Tên Tài khoản</label>
                   <select name="admin_id" class="form-control m-bot15">
                   @foreach($admin as $key => $ad)                   
                      @if($ad->admin_id == $edit_product->admin_id)
                        <option selected  value="{{$ad->admin_id}}">{{$ad->admin_name}}</option>
                      @else
                        <option value="{{$ad->admin_id}}">{{$ad->admin_name}}</option>
                      @endif   
                   @endforeach       
                   </select>
                </div>
                {{-- brand_id --}}
                <div class="form-group">
                   <label class="form-label" name="exampleInputPassword1">Tên Danh Mục</label>
                   <select name="brand_id" class="form-control m-bot15">
                   @foreach($brand as $key => $brand_id)                   
                      @if($brand_id->id == $edit_product->brand_id)
                        <option selected  value="{{$brand_id->id}}">{{$brand_id->name}}</option>
                      @else
                        <option value="{{$brand_id->id}}">{{$brand_id->name}}</option>
                      @endif   
                   @endforeach       
                   </select>
                </div>
                {{-- category_id --}}
                <div class="form-group">
                   <label class="form-label" name="exampleInputPassword1">Tên Danh Mục</label>
                   <select name="category_id" class="form-control m-bot15">
                   @foreach($category as $key => $category_id)                   
                      @if($category_id->id == $edit_product->category_id)
                        <option selected  value="{{$category_id->id}}">{{$category_id->name}}</option>
                      @else
                        <option value="{{$category_id->id}}">{{$category_id->name}}</option>
                      @endif   
                   @endforeach       
                   </select>
                </div>
                {{-- name --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Sản Phẩm</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name" value="{{$edit_product->name}}">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{-- ảnh danh mục sản phẩm --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh Sản Phẩm</font>
                   </label> 
                    <input type="file" name="img" class="form-control" id="img">
                    <img src="{{URL::to('/public/img_product/'.$edit_product->img)}}" 
                    height="90" width="120" style="margin-left: 5px">
                    <p class="help is-danger">{{ $errors->first('img') }}</p>
                </div>
                {{-- mô tả sản phẩm --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mô Tả Sản Phẩm</font>
                   </label> 
                    <input type="text" name="desc" class="form-control" id="desc" value="{{$edit_product->desc}}">
                    <p class="help is-danger">{{ $errors->first('desc') }}</p>
                </div>
                {{-- giá sản phẩm --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Giá Sản Phẩm</font>
                   </label> 
                    <input type="number" name="price" class="form-control" id="price" value="{{$edit_product->price}}">
                    <p class="help is-danger">{{ $errors->first('price') }}</p>
                </div>
                {{-- nội dung sản phẩm --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Nội Dung Sản Phẩm</font>
                   </label> 
                    <textarea name="content" id="content" 
                    style="resize: none;"rows="8"class="form-control">{{$edit_product->content}}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('content') }}</p>
                </div>
                {{-- số lượng --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Số Lượng</font>
                   </label> 
                    <input type="number" name="qty" class="form-control" value="{{$edit_product->qty}}">
                    <p class="help is-danger">{{ $errors->first('qty') }}</p>
                </div>
                {{-- kiểm tra sản phẩm --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Kiểm Tra</font>
                   </label> 
                   <select name="check" class="form-control m-bot15">
                       <option value="0" {{$edit_product->check == 0 ? "selected" : ""}}>Hết Hàng</option>
                       <option value="1" {{$edit_product->check == 1 ? "selected" : ""}}>Còn Hàng</option>   
                   </select>
                </div>
                {{-- tình trạng hạng --}}
                {{-- kiểm tra sản phẩm --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Kiểm Tra</font>
                   </label> 
                   <select name="check_new" class="form-control m-bot15">
                       <option value="0" {{$edit_product->check_new == 0 ? "selected" : ""}}>Đập Hộp</option>
                       <option value="1" {{$edit_product->check_new == 1 ? "selected" : ""}}>Mới</option>
                       <option value="1" {{$edit_product->check_new == 2 ? "selected" : ""}}>99%</option>   
                   </select>
                </div>
                {{-- trạng thái sản phẩm --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Trạng Thái</font>
                   </label> 
                   <select name="status" class="form-control m-bot15">
                       <option value="0" {{$edit_product->check == 0 ? "selected" : ""}}>Ẩn</option>
                       <option value="1" {{$edit_product->check == 1 ? "selected" : ""}}>Hiển Thị</option>   
                   </select>
                </div>
                {{-- cập nhật --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="update_product" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Cập Nhật Sản Phẩm</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
      @endforeach
    </div>   

@endsection
@section('js')
    <script>
        CKEDITOR.replace('content');      
    </script>
@endsection