@extends('admin.admin_newways')
@section('admin_content')

<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px">
            <span class="card-title"> Danh Sách Sản Phẩm</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 20px"> 
                        <a href="{{URL::to('/all-product')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 0px"> 
                        <a href="{{URL::to('/add-product')}}" class="btn" aria-expanded="false"> Thêm Mới Sản Phẩm</a> 
                     </div>
                     {{-- tìm kiếm theo thương Hiệu của sản phẩm --}}
                     <label class="col-sm-2" style="margin-top: 20px">
                        <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm" id="select-brand">
                           <option>----Thương Hiệu----</option>
                           @foreach($brand as $key => $br)
                            <option value="{{$br->id}}">{{$br->name}}</option>
                           @endforeach
                        </select>
                     </label>
                     <div class="header-navsearch" style="margin: 20px 20px 20px 0px"> 
                        <a href="#" class=" "></a>
                        <form class="form-inline mr-auto-add" action="{{route('search-product')}}" method="post">
                        @csrf
                          <div class="nav-search"> 
                              <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                              <button class="btn btn-primary-add" type="submit">
                                 <i class="fa fa-search"></i>
                              </button> 
                          </div>
                        </form>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 60px;">Mã</th>
                                 <th class="sorting" style="width: 60px;">Tên Người Đăng</th>
                                 <th class="sorting" style="width: 60px;">Tên Thương Hiệu</th>
                                 <th class="sorting" style="width: 115px;">Tên Sản Phẩm</th>
                                 <th class="sorting" style="width: 131px;">Mô Tả Sản Phẩm</th>
                                 <th class="sorting" style="width: 100px;">Ảnh Sản Phẩm</th>
                                 <th class="sorting" style="width: 105px;">Giá Sản Phẩm</th>
                                 <th class="sorting" style="width: 105px;">Số Lượng</th>
                                 <th class="sorting" style="width: 105px;">Kiểm Tra</th>
                                 <th class="sorting" style="width: 105px;">Tình Trạng Hàng</th>
                                 <th class="sorting" style="width: 105px;">Trạng Thái</th>
                                 <th class="sorting" style="width: 105px;">Lựa Chọn</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($all_product as $key => $show_product)
                              <tr role="row" class="odd">
                                 {{-- id --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_product->id}}
                                    </span>
                                 </td>
                                 {{-- user_id --}}
                                  <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                       <font style="vertical-align: inherit;color: #C70D03;">
                                        @foreach($user as $key => $us)
                                          {{$us->firstname}}
                                        @endforeach
                                       </font>
                                    </font>
                                 </td>
                                 {{-- brand_id --}}
                                  <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                       <font style="vertical-align: inherit;color: #C70D03;">
                                          {{$show_product->brand->name}}
                                       </font>
                                    </font>
                                 </td>
                                 {{-- tên sản phẩm --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$show_product->name}}
                                    </span>
                                 </td>
                                 {{-- mô tả --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$show_product->desc}}
                                    </span>
                                 </td>
                                 {{-- ảnh danh mục --}}
                                 <td style="width: 160px;text-align: center;">
                                    <img class="img" src="img_product/{{$show_product->img}}" 
                                    alt="hình ảnh"> 
                                 </td>
                                 {{-- giá --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$show_product->price}}
                                    </span>
                                 </td>
                                  {{-- số lượng --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$show_product->qty}}
                                    </span>
                                 </td>
                                 {{-- kiểm tra --}}
                                 <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">
                                         <?php
                                           if($show_product->check == 0)
                                           {
                                           ?>
                                            <span class="hienthi" id="0" style="color: #0DD902;">Hết Hàng</span>
                                           <?php
                                           }elseif($show_product->check == 1){
                                           ?>
                                            <span class="hienthi" id="1" style="color: #09B5C8;">Còn Hàng</span>
                                           <?php
                                           }
                                         ?> 
                                      </font>
                                   </font>
                                 </td>
                                 {{-- tình Trạng Hàng --}}
                                 <td style="text-align: center;">
                                    <font style="vertical-align: inherit;">
                                      <font style="vertical-align: inherit;">
                                         <?php
                                           if($show_product->check_new == 0)
                                           {
                                           ?>
                                            <span class="hienthi" id="0" style="color: #0DD902;">Đập Hộp</span>
                                           <?php
                                           }elseif($show_product->check_new == 1){
                                           ?>
                                            <span class="hienthi" id="1" style="color: #09B5C8;">Mới</span>
                                           <?php
                                           }elseif($show_product->check_new == 2){
                                           ?>
                                            <span class="hienthi" id="2" style="color: #FFA54F;">99%</span>
                                           <?php
                                           }
                                         ?> 
                                      </font>
                                   </font>
                                 </td>
                                 {{-- trạng thái --}}
                                 <td style="text-align:center;">
                                    <span>
                                       <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;">
                                             <?php
                                               if($show_product->status == 0)
                                               {
                                               ?>
                                               <a class="btn btn-xs btn-danger blog" 
                                                href="{{URL::to('unactive-product/'.$show_product->id)}}">
                                                  Ẩn
                                                </a>
                                               <?php
                                               }else{
                                               ?>
                                               <a class="btn btn-xs btn-green blog" 
                                                href="{{URL::to('active-product/'.$show_product->id)}}">
                                                Hiển Thị
                                                </a>
                                               <?php
                                               }
                                             ?>
                                          </font>
                                       </font>
                                    </span>
                                 </td>
                                 {{-- sửa xóa --}}
                                 <td class="r" style="text-align: center;width: 160px;">
                                    <a class="btn btn-xs btn-info blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    href="{{URL::to('/edit-product/'.$show_product->id)}}">
                                       <i class="fa fa-edit"style="margin-top: 6px"></i>Sửa
                                    </a>
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                    href="{{URL::to('/delete-product/'.$show_product->id)}}">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">          
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
  {{-- tìm kiếm theo danh mục(category) --}}
  document.getElementById('select-brand').addEventListener('change', function() {
    var value = $("#select-brand option:selected").val();
    $.ajax({
      url: '{{route('change.brand')}}',
      method: 'GET',
      data:{
       value:value,
     },
     success: function(data){
       $("table#example tbody").empty();
       $("table#example tbody").html(data);
       console.log(data);
     }
   })
  });
</script>
@endsection('admin_content')