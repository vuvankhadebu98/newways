@extends('admin.admin_newways')
@section('admin_content')

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Cập Nhật Slider</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('list-slider')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách slider</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        @foreach($edit_slider as $key => $edit_sl)
        <form role="form" action="{{URL::to('/update-slider/'.$edit_sl->id)}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Slider</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name"
                    value="{{$edit_sl->name}}">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{-- ảnh slider --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh Slider</font>
                   </label> 
                    <input type="file" name="img" class="form-control" id="img">
                    <img src="{{URL::to('public/img_slider/'.$edit_sl->img)}}" 
                    height="80" width="120" style="margin-left: 5px">
                </div>
                {{-- mô tả slider --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mô Tả Thương Hiệu</font>
                   </label> 
                    <textarea name="desc" id="desc" 
                    style="resize: none;"rows="8"class="form-control">
                    {!! $edit_sl->desc !!}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('desc') }}</p>
                </div>
                {{-- Trạng thái --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Trạng Thái</font>
                   </label> 
                   <select name="status" class="form-control m-bot15">
                       <option value="0" {{$edit_sl->status == 0 ? "selected" : ""}}>Ẩn</option>
                       <option value="1" {{$edit_sl->status == 1 ? "selected" : ""}}>Hiển Thị</option>   
                   </select>
                </div>
                {{-- cập nhật --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="update_slider" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Cập Nhật Slider</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
        @endforeach
    </div>   

@endsection('admin_content')
@section('js')
    <script>
        CKEDITOR.replace('desc');      
    </script>
@endsection