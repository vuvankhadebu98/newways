@extends('admin.admin_newways')
@section('admin_content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Cập Nhật Tài Khoản</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('all-user')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Tài Khoản</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        @foreach($edit_user as $key => $edit_us)
        <form role="form" action="{{URL::to('/update-user/'.$edit_us->admin_id)}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
              {{-- tên tài khoản --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Tài Khoản</font>
                   </label> 
                    <input type="text" name="admin_name" class="form-control" id="name" value="{{$edit_us->admin_name}}">
                    <p class="help is-danger">{{ $errors->first('admin_name') }}</p>
                </div>
                {{-- email --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Email</font>
                   </label> 
                    <input type="text" name="admin_email" class="form-control" id="name" value="{{$edit_us->admin_email}}"> 
                    <p class="help is-danger">{{ $errors->first('admin_email') }}</p>
                </div>
                {{-- password --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mật Khẩu</font>
                   </label> 
                    <input type="password" name="admin_password" class="form-control" value="{{$edit_us->admin_password}}">
                    <p class="help is-danger">{{ $errors->first('admin_password') }}</p>
                </div>
                {{-- img --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh Đại Diện</font>
                   </label> 
                    <input type="file" name="admin_img" class="form-control">
                    <img src="{{URL::to('public/img_user/'.$edit_us->admin_img)}}" 
                    height="90" width="120" style="margin-left: 5px">
                </div>
                {{-- phone --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Số Điện Thoại</font>
                   </label> 
                    <input type="number" name="admin_phone" class="form-control" value="{{$edit_us->admin_phone}}">
                    <p class="help is-danger">{{ $errors->first('admin_phone') }}</p>
                </div>
                {{-- thêm và hủy --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="add_user" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Cập Nhật Tài Khoản</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
        @endforeach
    </div>   

@endsection
