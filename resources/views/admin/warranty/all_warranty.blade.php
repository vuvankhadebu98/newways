@extends('admin.admin_newways')
@section('admin_content')

<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body" style="margin-top: 20px">
            <span class="card-title"> Danh Sách Chính Sách</span>
             <?php 
                 $message = Session::get('message');
                 if ($message) {
                   echo '<div class="alert alert-success">'. $message .'</div>';
                   Session::put('message', null);
                 }
              ?>
            <div class="table-responsive">
               <div id="multi_col_order_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                  <div class="row">
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 20px"> 
                        <a href="{{URL::to('/all-warranty')}}" class="btn" aria-expanded="false"> Tất Cả </a> 
                     </div>
                     <div class="btn-group hidden-phone" style="margin: 20px 20px 35px 0px"> 
                        <a href="{{URL::to('/add-warranty')}}" class="btn" aria-expanded="false"> Thêm Mới Chính Sách </a> 
                     </div>
                     <div class="header-navsearch" style="margin: 20px 20px 20px 0px"> 
                        <a href="#" class=" "></a>
                        <form class="form-inline mr-auto-add" action="{{route('search-warranty')}}" method="post">
                        @csrf
                          <div class="nav-search"> 
                              <input type="search" class="form-control header-search" placeholder="Search…" aria-label="Search" name="key"> 
                              <button class="btn btn-primary-add" type="submit">
                                 <i class="fa fa-search"></i>
                              </button> 
                          </div>
                        </form>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="multi_col_order_info">
                           <thead>
                              <tr role="row">
                                 <th class="sorting" style="width: 60px;">STT</th>
                                 <th class="sorting" style="width: 115px;">Tên Chính Sách</th>
                                 <th class="sorting" style="width: 100px;">Mô Tả Chính Sách</th>
                                 <th class="sorting" style="width: 131px;">Tình Trạng</th>
                                 <th class="sorting" style="width: 105px;">Lựa Chọn</th>
                              </tr>
                           </thead>
                           <tbody>
                            @php 
                            $i = 0;
                            @endphp
                              @foreach($all_warranty as $key => $show_warranty)
                              @php
                              $i++;
                              @endphp
                              <tr role="row" class="odd">
                                 {{-- stt --}}
                                 <td style="text-align: center;">
                                    <span>
                                       {{$i}}
                                    </span>
                                 </td>
                                 {{-- tên danh mục --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {{$show_warranty->name}}
                                    </span>
                                 </td>
                                 {{-- Mô tả bảo hành --}}
                                 <td style="text-align: center;">
                                    <span style="color: #C70D03">
                                       {!!$show_warranty->desc!!}
                                    </span>
                                 </td>
                                 {{-- trạng thái --}}
                                 <td style="text-align:center;">
                                    <span>
                                       <font style="vertical-align: inherit;">
                                          <font style="vertical-align: inherit;">
                                             <?php
                                               if($show_warranty->status == 0)
                                               {
                                               ?>
                                               <a class="btn btn-xs btn-danger blog" 
                                                href="{{URL::to('unactive-warranty/'.$show_warranty->id)}}">
                                                  Ẩn Bảo Hành
                                                </a>
                                               <?php
                                               }else{
                                               ?>
                                               <a class="btn btn-xs btn-green blog" 
                                                href="{{URL::to('active-warranty/'.$show_warranty->id)}}">
                                                Hiển Thị 
                                                </a>
                                               <?php
                                               }
                                             ?>
                                          </font>
                                       </font>
                                    </span>
                                 </td>
                                 {{-- sửa xóa --}}
                                 <td class="r" style="text-align: center;width: 160px;">
                                    <a class="btn btn-xs btn-info blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    href="{{URL::to('/edit-warranty/'.$show_warranty->id)}}">
                                       <i class="fa fa-edit"style="margin-top: -6px"></i>Sửa
                                    </a>
                                    <a class="btn btn-xs btn-danger blog" 
                                    style="font-size: 18px;width: 70px; height: 34px;"
                                    onclick="return confirm('bạn có chắc muốn xóa thư mục này không ?')" 
                                    href="{{URL::to('/delete-warranty/'.$show_warranty->id)}}">
                                       <i class="fa fa-times"style="margin-top: 6px" ></i>Xóa
                                    </a>
                                 </td>
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="row">  
                  {{$all_warranty->links()}}           
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection('admin_content')