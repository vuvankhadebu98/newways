@extends('admin.admin_newways')
@section('admin_content')

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate">Cập Nhật Chính Sách</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('tong-quan')}}" class="text-muted">
                                    <span style="color: #009C4C;">Tổng Quan</span>
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{URL::to('list-slider')}}" class="text-muted">
                                    <span style="color: #030084;">Danh Sách Chính Sách</span>
                                </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>    
    {{-- tên danh mục sản phẩm --}}
    <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
    ?>
    <div class="card">
        @foreach($edit_warranty as $key => $edit_wa)
        <form role="form" action="{{URL::to('/update-warranty/'.$edit_wa->id)}}" method="post"
          enctype="multipart/form-data">
          {{ csrf_field()}}
            <div class="card-body" style="margin-top: 20px">
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Tên Chính Sách</font>
                   </label> 
                    <input type="text" name="name" class="form-control" id="name"
                    value="{{$edit_wa->name}}">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                {{-- ảnh chính sách --}}
                {{-- <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Ảnh Chính Sách</font>
                   </label> 
                    <input type="file" name="img" class="form-control" id="img">
                    <img src="{{URL::to('public/img_slider/'.$edit_wa->img)}}" 
                    height="80" width="120" style="margin-left: 5px">
                </div> --}}
                {{-- mô tả slider --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Mô Tả Chính Sách</font>
                   </label> 
                    <textarea name="desc" id="desc" 
                    style="resize: none;"rows="8"class="form-control">
                    {!! $edit_wa->desc !!}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('desc') }}</p>
                </div>
                {{-- Nội Dung Chính Sách --}}
                <div class="form-group">
                    <label class="form-label">
                        <font style="vertical-align: inherit;">Nội Dung Chính Sách</font>
                   </label> 
                    <textarea name="content" id="content" 
                    style="resize: none;"rows="8"class="form-control">
                    {!! $edit_wa->content !!}
                    </textarea>
                    <p class="help is-danger">{{ $errors->first('content') }}</p>
                </div>
                {{-- Trạng thái --}}
                <div class="form-group">
                  <label class="form-label">
                         <font style="vertical-align: inherit;">Trạng Thái</font>
                   </label> 
                   <select name="status" class="form-control m-bot15">
                       <option value="0" {{$edit_wa->status == 0 ? "selected" : ""}}>Ẩn Chính Sách</option>
                       <option value="1" {{$edit_wa->status == 1 ? "selected" : ""}}>Hiển Thị Chính Sách</option>   
                   </select>
                </div>
                {{-- cập nhật --}}
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" name="update_slider" 
                            class="btn btn-primary waves-effect waves-light">
                             <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Cập Nhật Chính Sách</font>
                             </font>
                        </button>
                    </div>
                </div>   
            </div>
        </form>
        @endforeach
    </div>   

@endsection('admin_content')
@section('js')
    <script>
        CKEDITOR.replace('desc'); 
        CKEDITOR.replace('content');     
    </script>
@endsection