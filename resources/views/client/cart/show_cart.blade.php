@extends('client.welcome')
@section('content')

<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  	<li>
			  		<a href="{{URL::to('/')}}">Trang Chủ</a>
			  	</li>
			  	<li class="active">Giỏ Hàng Của Bạn</li>
			</ol>
		</div>
      <?php 
         $message = Session::get('message');
         if ($message) {
           echo '<div class="alert alert-success">'. $message .'</div>';
           Session::put('message', null);
         }
      ?>
      <?php 
         $error = Session::get('error');
         if ($error) {
           echo '<div class="alert alert-danger">'. $error .'</div>';
           Session::put('error', null);
         }
      ?>
		<div class="table-responsive cart_info">
         <form action="{{URL::to('/update-cart')}}" method="post">
            @csrf
   			<table class="table table-condensed">
   				<thead>
   					<tr class="cart_menu">
   						<td class="description">Hình Ảnh</td>
   						<td class="description">Tên Sản Phẩm</td>
   						<td class="description">Giá Sản Phẩm</td>
   						<td class="description">Số Lượng</td>
   						<td class="description">Tổng Số Tiền</td>
   						<td class="description-d"></td>
   					</tr>
   				</thead>
   				<tbody>
          @if(Session::get('cart')== true)
          @php
             $total = 0;
          @endphp

          @foreach(Session::get('cart') as $key => $cart)

          @php
             $subtotal = $cart['product_price'] * $cart['product_qty'];
             $total += $subtotal;
          @endphp
   					<tr>
                     {{-- ảnh --}}
   						<td class="cart_product-id">
   							<a href="">
   								<img src="{{asset('public/img_product/'.$cart['product_img'])}}"width="120" height="80"
   							  alt="{{$cart['product_name']}}"></a>
   						</td>
                     {{-- tên --}}
   						<td class="cart_description">
   							<h4><a href="">
   								{{$cart['product_name']}}	
   							</a></h4>
   							<p style="float: left;">ID:{{$cart['product_id']}}</p>
   						</td>
                  {{-- giá --}}
   						<td class="cart_price">
   							<p style="float: left;">
   								{{number_format($cart['product_price'],0,',','.')}} vnđ
   							</p>
   						</td>
                     {{-- số lượng --}}
   						<td class="cart_quantity">
   							<div class="cart_quantity_button">
   								<input class="cart_quantity_input" type="number" 
                           name="cart_quantity[{{$cart['session_id']}}]" value="{{$cart['product_qty']}}" min="1">
   							</div>
   						</td>
                     {{-- tổng tiền --}}
   						<td class="cart_total">
   							<p class="cart_total_price">
                           {{number_format($subtotal,0,',','.')}} vnđ
   							</p>
   						</td>
                     {{-- xóa đơn hàng --}}
   						<td class="cart_delete">
   							<a class="cart_quantity" href="{{url('/delete-cart/'.$cart['session_id'])}}"><i class="fa fa-times"></i></a>
   						</td>
   					</tr>                  
          @endforeach		
   				</tbody>
             {{-- cập nhật đợn hàng --}}
             <tr>
                 <td class="update-order-all" colspan="1">
                    <input type="submit" value="Cập Nhật Giỏ Hàng" name="update_qty" class="btn btn-info btn-sm-dd">
                 </td>
                {{--  --}}
                 <td class="delete-order-all" colspan="1">
                    {{-- xoá tất cả đơn hàng --}}
                    <a class="btn btn-default delete" href="{{url('/delete-all-order')}}">Xóa Tất Cả Đơn Hàng</a>
                 </td>
                 
                 <td colspan="2">
                   <ul>
                      <li style="font-size: 22px">Tổng Tiền:
                         <span style="color: #ff9c00;" >{{number_format($total,0,',','.')}} vnđ</span>
                      </li>
                      @if(Session::get('coupon'))
                       <li style="font-size: 18px">
                         <span>
                            @foreach(Session::get('coupon') as $key => $cou)
                                @if($cou['check'] == 1)
                                  Mã Giảm Giá: {{$cou['number']}} %
                                  <p>
                                    @php
                                      $total_coupon = ($total*$cou['number'])/100;
                                      echo '<p><li style="font-size: 18px;margin-top: -15px;">
                                      Tổng Giảm: '.number_format($total_coupon,0,',','.'). 'vnđ</li></p>';
                                    @endphp
                                  </p>
                                  <p>
                                    <li style="font-size: 22px;margin-top: -15px;">
                                      Thanh Toán :<span style="color: red;" >
                                      {{number_format($total-$total_coupon,0,',','.')}} vnđ </span>
                                    </li>
                                  </p>
                                @elseif($cou['check'] == 2)
                                  Mã Giảm Giá: {{number_format($cou['number'],0,',','.')}} vnđ
                                  <p>
                                    @php
                                      $total_coupon = $total-$cou['number'];
                                    @endphp
                                  </p>
                                  <p>
                                    <li style="font-size: 22px;margin-top: -15px;">
                                      Thanh Toán :<span style="color: #ff9c00;" >
                                      {{number_format($total_coupon,0,',','.')}} vnđ </span>
                                    </li>
                                  </p>
                                @endif
                            @endforeach
                         </span>
                      </li>
                      @endif
                   </ul>
                    @if(Session::get('customer'))
                    <a class="btn btn-default delete-gg" href="{{url('checkout')}}">Đặt Hàng</a>
                    @else
                    <a class="btn btn-default delete-gg" href="{{url('/login-checkout')}}">Đặt Hàng</a>
                    @endif
                 </td>
             </tr> 
             @else
             <tr>
                <td colspan="5" style="text-align: center;font-size: 20px">
                   @php
                      echo '<p>Giỏ hàng của bạn chưa có gì hết hãy thêm sản phẩm vào giỏ hàng</p>';
                   @endphp
                </td>
             </tr>   
             @endif
 			  </table>
      </form>
         {{-- vận chuyển --}}
         @if(Session::get('cart'))
          <td>
            <form action="{{url('/check-coupon')}}" method="post">
              @csrf
              <ul class="user_option">
                <li class="breadcrumb-item-vc" >Mã Giảm Giá</li>
                <input type="text" name="coupon" class="form-control" id="coupon" style="width: 275px">
                <input type="submit" value="Tính Mã Giảm Giá" name="check_coupon" class="btn btn-info btn-sm-dd-sm">
                @if(Session::get('coupon'))
                <a class="btn btn-default delete-gg" href="{{url('/unset-coupon')}}">Xóa Mã Giảm Giá</a>
                @endif
              </ul>
            </form>
          </td>
        @endif 
		</div>
	</div>
</section>
<div class="container ctvp">
      <h3 class="ctvp__title">
          <p> Có thể bạn sẽ thích </p>
      </h3>
      <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product">
                    <!-- item product -->
                @foreach($all_product as $key => $product)
                    <div class=" item__product">
                        <form>
                            @csrf
                            <input type="hidden" value="{{$product->id}}" class="cart_product_id_{{$product->id}}">
                            <input type="hidden" value="{{$product->name}}" class="cart_product_name_{{$product->id}}">
                            <input type="hidden" value="{{$product->img}}" class="cart_product_img_{{$product->id}}">
                            <input type="hidden" value="{{$product->price}}" class="cart_product_price_{{$product->id}}">
                            <input type="hidden" value="1" class="cart_product_qty_{{$product->id}}">
                            <span class="onsale">
                                <span class="saled">
                                    -17%
                                </span>
                                <br />
                                <span class="featured">
                                    Hot
                                </span>
                            </span>
                            <!-- group button on hover -->
                            <div class="group-button">
                                {{-- thêm giỏ hàng --}}
                                <div class="cart-add" name="cart-add" data-id="{{$product->id}}"> 
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </div>
                                {{--yêu thích hàng --}}
                                <div class="btn-wishliss">
                                    <i class="fa fa-heart group-button--icon"></i>
                                </div>
                                {{-- chia sẻ --}}
                                <div class="quick-view">
                                    <i class="fa fa-eye group-button--icon"></i>  
                                </div>
                            </div>
                            <!-- caption -->
                            <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
                                <div class="caption">
                                    <h3 class="name-product">{{$product->name}}</h3>
                                    <div class="price-product">
                                        <span class="price-product--cost">
                                            {{number_format($product->price).' '.'VNĐ'}}
                                        </span>

                                        <span class="price-product--sale">
                                            - {{number_format($product->price).' '.'VNĐ'}}
                                        </span>
                                    </div>
                                </div>
                            </a>
                            <img src="{{URL::to('public/img_product/'.$product->img)}}" class="product__img" alt="" />
                        </form>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>  

@endsection