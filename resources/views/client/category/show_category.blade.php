@extends('client.welcome')
@section('content')

    {{-- danh mục sản phẩm trên span --}}
    <div class="filter">
        <div class="manu">
            <a href="{{URL::to('/thuong-hieu-san-pham/2')}}" class="macbook">
                <img src="{{asset('public/frontend/img/Macbook44-b_41.png')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/1')}}" class="dell">
                <img src="{{asset('public/frontend/img/Dell44-b_34.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/3')}}"  class="hp">
                <img src="{{asset('public/frontend/img/HP-Compaq44-b_36.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/4')}}"  class="asus">
                <img src="{{asset('public/frontend/img/Asus44-b_35.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/5')}}"  class="lenovo">
                <img src="{{asset('public/frontend/img/Lenovo44-b_36.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/6')}}"  class="acer">
                <img src="{{asset('public/frontend/img/Acer44-b_37.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/7')}}"  class="huawei">
                <img src="{{asset('public/frontend/img/Huawei44-b_7.jpg')}}" style="width: 130px; height:27px;" >
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/8')}}"  class="msi">
                <img src="{{asset('public/frontend/img/MSI44-b_30.png')}}">
            </a>  
        </div>
    </div>
    {{-- các danh mục sản phẩm --}}
    <div class="container ctvc mt-5">
        <h3 class="ctvc__title">
            @foreach($category_name as $key => $name)
                {{$name->name}}
            @endforeach
        </h3>
       <div class="row ctvc__border">
        @foreach($category_by_id as $key => $product)
        <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
            <div class="col-md-3 col-5 col-sm-4 ctvc__item">
                <div class=" item__product">
                    <span class="onsale">
                        <span class="saled">
                            -17%
                        </span>
                        <br />
                        <span class="featured">
                            Hot
                        </span>
                    </span>
                    <!-- group button on hover -->
                    <div class="group-button">
                        {{-- thêm giỏ hàng --}}
                       {{--  <div class="add-cart">
                            <i class="fa fa-cart-plus group-button--icon"></i>
                            
                        </div> --}}
                        {{--yêu thích hàng --}}
                        <div class="btn-wishliss">
                            <i class="fa fa-heart group-button--icon"></i>
                        </div>
                        {{-- chia sẻ --}}
                        <div class="quick-view">
                            <i class="fa fa-eye group-button--icon"></i>
                        </div>
                    </div>
                    <img src="{{URL::to('public/img_product/'.$product->img)}}" class="img-fluid ctvc__img" alt="">
                    <div class="ctvc__item-content">
                        <h3 class="name-product">{{$product->name}}</h3>
                        <a href="#">Choppers & Graters</a>
                        <div class="price-product">
                            <span class="price-product--cost">
                                {{number_format($product->price).' '.'VNĐ'}}
                            </span>

                            <span class="price-product--sale">
                                - {{number_format($product->price).' '.'VNĐ'}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        @endforeach
            
            <div class="ctvc__item-content text-center mb-5" style="width: 100%">
                <a href="#">Shop all categories <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>


    {{-- danh mục sản phẩm --}}
    <div class="container ctvp">
        <h3 class="ctvp__title">
            @foreach($category_name as $key => $name)
                {{$name->name}}
            @endforeach
        </h3>
        <div class="ctvp__box">
            <div class="carousel-inner ctvp__box__slide">
                <div class="carousel-item active">
                    <div class=" row__item__product">
                        <!-- item product -->
                    @foreach($category_by_id as $key => $product)
                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
                        <div class=" item__product">
                            <span class="onsale">
                                <span class="saled">
                                    -17%
                                </span>
                                <br />
                                <span class="featured">
                                    Hot
                                </span>
                            </span>
                            <!-- group button on hover -->
                            <div class="group-button">
                                {{-- thêm giỏ hàng --}}
                                <div class="cart-add" name="cart-add" data-id="{{$product->id}}">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                    
                                </div>
                                {{--yêu thích hàng --}}
                                <div class="btn-wishliss">
                                    <i class="fa fa-heart group-button--icon"></i>
                                </div>
                                {{-- chia sẻ --}}
                                <div class="quick-view">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </div>
                            </div>
                            <!-- caption -->
                            <div class="caption">
                                <h3 class="name-product">{{$product->name}}</h3>
                                <div class="price-product">
                                    <span class="price-product--cost">
                                        {{number_format($product->price).' '.'VNĐ'}}
                                    </span>

                                    <span class="price-product--sale">
                                        - {{number_format($product->price).' '.'VNĐ'}}
                                    </span>
                                </div>
                            </div>
                            <img src="{{URL::to('public/img_product/'.$product->img)}}" class="product__img" alt="" />
                        </div>
                    </a>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fb-comments col-md-12" data-href="http://localhost/newways/chi-tiet-san-pham/7" data-numposts="20" data-width=""></div>
    <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-width="500" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
        {{-- sản phẩm nổi bật --}}
    <div class="container ctvp">
        <h3 class="ctvp__title">
            Sản phẩm nổi bật
        </h3>
        <div class="ctvp__box">
            <div class="carousel-inner ctvp__box__slide">
                <div class="carousel-item active">
                    <div class=" row__item__product">
                        <!-- item product -->
                    @foreach($all_product as $key=> $product)
                        <div class=" item__product">
                            <span class="onsale">
                                <span class="saled">
                                    -17%
                                </span>
                                <br />
                                <span class="featured">
                                    Hot
                                </span>
                            </span>
                            <!-- group button on hover -->
                            <div class="group-button">
                                <div class="add-cart">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </div>
                                <div class="btn-wishliss">
                                    <i class="fa fa-heart group-button--icon"></i>
                                </div>
                                <div class="quick-view">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </div>
                            </div>
                            <!-- caption -->
                            <div class="caption">
                                <h3 class="name-product">{{$product->name}}</h3>
                                <div class="price-product">
                                    <span class="price-product--cost">
                                        {{number_format($product->price).' '.'VNĐ'}}
                                    </span>

                                    <span class="price-product--sale">
                                        - {{number_format($product->price).' '.'VNĐ'}}
                                    </span>
                                </div>
                            </div>
                            <img src="{{URL::to('public/img_product/'.$product->img)}}" class="product__img" alt="" />
                        </div> 
                    @endforeach                         
                    </div>
                </div>
            </div>
        </div>
    </div>  
@endsection   