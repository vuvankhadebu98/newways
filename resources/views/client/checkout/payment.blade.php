@extends('client.welcome')
@section('content')

<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="#">Trang Chủ</a></li>
			  <li class="active">Thanh Toán Giỏ Hàng</li>
			</ol>
		</div><!--/breadcrums-->
		<div class="review-payment">
			<h2>Xem Lại Giỏ Hàng</h2>
		</div>
		<div class="table-responsive cart_info">
         <form action="{{URL::to('/update-cart')}}" method="post">
            @csrf
   			<table class="table table-condensed">
   				<thead>
   					<tr class="cart_menu">
   						<td class="description">Hình Ảnh</td>
   						<td class="description">Tên Sản Phẩm</td>
   						<td class="description">Giá Sản Phẩm</td>
   						<td class="description">Số Lượng</td>
   						<td class="description">Tổng Số Tiền</td>
   						<td class="description-d"></td>
   					</tr>
   				</thead>
   				<tbody>
          @if(Session::get('cart')== true)
          @php
             $total = 0;
          @endphp

          @foreach(Session::get('cart') as $key => $cart)

          @php
             $subtotal = $cart['product_price'] * $cart['product_qty'];
             $total += $subtotal;
          @endphp
   					<tr>
                     {{-- ảnh --}}
   						<td class="cart_product-id">
   							<a href="">
   								<img src="{{asset('public/img_product/'.$cart['product_img'])}}"width="120" height="80"
   							  alt="{{$cart['product_name']}}"></a>
   						</td>
                     {{-- tên --}}
   						<td class="cart_description">
   							<h4><a href="">
   								{{$cart['product_name']}}	
   							</a></h4>
   							<p style="float: left;">ID:{{$cart['product_id']}}</p>
   						</td>
                     {{-- giá --}}
   						<td class="cart_price">
   							<p style="float: left;">
   								{{number_format($cart['product_price'],0,',','.')}} vnđ
   							</p>
   						</td>
                     {{-- số lượng --}}
   						<td class="cart_quantity">
   							<div class="cart_quantity_button">
   								<input class="cart_quantity_input" type="number" 
                           name="cart_quantity[{{$cart['session_id']}}]" value="{{$cart['product_qty']}}" min="1">
   							</div>
   						</td>
                     {{-- tổng tiền --}}
   						<td class="cart_total">
   							<p class="cart_total_price">
                           {{number_format($subtotal,0,',','.')}} vnđ
   							</p>
   						</td>
                     {{-- xóa đơn hàng --}}
   						<td class="cart_delete">
   							<a class="cart_quantity" href="{{url('/delete-cart/'.$cart['session_id'])}}"><i class="fa fa-times"></i></a>
   						</td>
   					</tr>                  
          @endforeach		
   				</tbody>
             {{-- cập nhật đợn hàng --}}
             <tr>
                 <td class="update-order-all-2" colspan="1">
                    <input type="submit" value="Cập Nhật Giỏ Hàng" name="update_qty" class="btn btn-info btn-sm-dd-2">
                 </td>
                {{--  --}}
                 <td class="delete-order-all-2" colspan="1">
                    {{-- xoá tất cả đơn hàng --}}
                    <a class="btn btn-default delete-2" href="{{url('/delete-all-order')}}">Xóa Tất Cả Đơn Hàng</a>
                 </td>
                 
                 <td colspan="2">
                   <ul>
                      <li style="font-size: 22px">Tổng Tiền:
                         <span style="color: #ff9c00;" >{{number_format($total,0,',','.')}} vnđ</span>
                      </li>
                      @if(Session::get('coupon'))
                       <li style="font-size: 18px">
                         <span>
                            @foreach(Session::get('coupon') as $key => $cou)
                                @if($cou['check'] == 1)
                                  Mã Giảm Giá: {{$cou['number']}} %
                                  <p>
                                    @php
                                      $total_coupon = ($total*$cou['number'])/100;
                                      echo '<p><li style="font-size: 18px;margin-top: -15px;">
                                      Tổng Giảm: '.number_format($total_coupon,0,',','.'). 'vnđ</li></p>';
                                    @endphp
                                  </p>
                                  <p>
                                    <li style="font-size: 22px;margin-top: -15px;">
                                      Thanh Toán :<span style="color: red;" >
                                      {{number_format($total-$total_coupon,0,',','.')}} vnđ </span>
                                    </li>
                                  </p>
                                @elseif($cou['check'] == 2)
                                  Mã Giảm Giá: {{number_format($cou['number'],0,',','.')}} vnđ
                                  <p>
                                    @php
                                      $total_coupon = $total-$cou['number'];
                                    @endphp
                                  </p>
                                  <p>
                                    <li style="font-size: 22px;margin-top: -15px;">
                                      Thanh Toán :<span style="color: #ff9c00;" >
                                      {{number_format($total_coupon,0,',','.')}} vnđ </span>
                                    </li>
                                  </p>
                                @endif
                            @endforeach
                         </span>
                      </li>
                      @endif
                   </ul>
                 </td>
             </tr> 
             @else
             <tr>
                <td colspan="5" style="text-align: center;font-size: 20px">
                   @php
                      echo '<p>Giỏ hàng của bạn chưa có gì hết hãy thêm sản phẩm vào giỏ hàng</p>';
                   @endphp
                </td>
             </tr>   
             @endif
 			  </table>
      </form>
         {{-- vận chuyển --}}
         @if(Session::get('cart'))
          <td>
            <form action="{{url('/check-coupon')}}" method="post">
              @csrf
              <ul class="user_option-2">
                <li class="breadcrumb-item-vc" >Mã Giảm Giá</li>
                <input type="text" name="coupon" class="form-control" id="coupon" style="width: 275px">
                <input type="submit" value="Tính Mã Giảm Giá" name="check_coupon" class="btn btn-info btn-sm-dd-sm">
                @if(Session::get('coupon'))
                <a class="btn btn-default delete-gg" href="{{url('/unset-coupon')}}">Xóa Mã Giảm Giá</a>
                @endif
            </form>
          </td>
        @endif 
		</div>
		<form action="{{URL::to('order-place')}}" method="post">
			@csrf
			<div class="payment-options">
				<h4>Hình Thức Thanh Toán</h4>
				<span>
					<label><input name="payment_option" value="1" type="checkbox">Trả Bằng Thẻ ATM</label>
				</span>
				<span>
					<label><input name="payment_option" value="2" type="checkbox">Thanh Toán Khi Nhận Hàng</label>
				</span>
				<span>
					<label><input name="payment_option" value="3" type="checkbox">Thanh toán bằng Thẻ Visa</label>
				</span>	
				<br>
				<input type="submit" name="send_order_place" value="Đặt Hàng" class="btn btn-primary btn-sm" >
			</div>
		</form>
	</div>
</section> <!--/#cart_items-->
@endsection