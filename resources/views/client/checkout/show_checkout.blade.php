@extends('client.welcome')
@section('content')

<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="#">Trang Chủ</a></li>
			  <li class="active">Thanh Toán Giỏ Hàng</li>
			</ol>
		</div><!--/breadcrums-->

		<div class="register-req">
			<p>Làm Ơn Đăng Ký Hoặc Đăng Nhập Để Thanh Toán Giở Hàng và Xem Lại Lịch Sử Mua Hàng</p>
		</div><!--/register-req-->

		<div class="shopper-informations">
			<div class="row">
				{{-- ĐIỀN THÔNG TIN THANH TOÁN --}}
				<div class="col-sm-6">
					<?php 
				         $message = Session::get('message');
				         if ($message) {
				           echo '<div class="alert alert-success">'. $message .'</div>';
				           Session::put('message', null);
				         }
				      ?>
				      <?php 
				         $error = Session::get('error');
				         if ($error) {
				           echo '<div class="alert alert-danger">'. $error .'</div>';
				           Session::put('error', null);
				         }
				      ?>
					<div class="shopper-info">
						<p>Điền Thông Tin Thanh Toán</p>
						<form  method="post">
							@csrf
							<input type="text" name="name" class="name" placeholder="Họ và tên*">
							<input type="text" name="email" class="email" placeholder="Email*">
							<input type="text" name="address" class="address" placeholder="Địa chỉ nhận hàng*">
							<input type="text" name="phone" class="phone" placeholder="phone*">
							<textarea name="notes" class="notes" placeholder="điền ghi chú gửi hàng" rows="7"></textarea>
							{{-- fee --}}
							@if(Session::get('fee'))
								<input type="hidden" name="order_fee" class="order_fee" value="{{Session::get('fee')}}">
							@else
								<input type="hidden" name="order_fee" class="order_fee" value="20000">
							@endif
							{{-- coupon --}}
							@if(Session::get('coupon'))
								@foreach(Session::get('coupon') as $key => $cou)
								<input type="hidden" name="order_coupon" class="order_coupon" value="{{$cou['code']}}">
								@endforeach
							@else
								<input type="hidden" name="order_coupon" class="order_coupon" value="Không có mã giảm giá">
							@endif
							<div class="form-group">
					            <label class="form-label">
					                 <font style="vertical-align: inherit;">Chọn Hình Thức Thanh Toán</font>
					            </label> 
					            <select name="payment_select" id="payment_select" 
					            	class="form-control m-bot15 wards payment_select">
					                <option value="0">Thẻ Ngân Hàng</option>
					                <option value="1">Tiền Mặt</option>  
					            </select>
					        </div>
					        <input type="button" name="send_order_product" value="Xác nhận đơn hàng" 
					            class="btn btn-primary btn-sm send_order_product"  >	
						</form>
					</div>
				</div>
				{{-- tính phí vận chuyển --}}
				<div class="col-lg-6 ">
					<div class="shopper-info">
						<p>Chọn Địa Chỉ Tính Phí Vận Chuyển</p>
						<form>
						  @csrf
						    <div class="card-body" style="margin-top: 20px">
						      {{-- Thên thành phố --}}
						        <div class="form-group">
						          <label class="form-label">
						                 <font style="vertical-align: inherit;">Chọn Thành Phố</font>
						           </label> 
						           <select name="city" id="city" class="form-control m-bot15 choose city">
						               <option value="">--Chọn Tỉnh Thành Phố--</option>
						               @foreach($city as $key => $ci)
						                  <option value="{{$ci->matp}}">{{$ci->name}}</option> 
						               @endforeach  
						           </select>
						        </div>
						        {{-- chọn quận huyện --}}
						        <div class="form-group">
						          <label class="form-label">
						                 <font style="vertical-align: inherit;">Chọn Quận Huyện</font>
						           </label> 
						           <select name="province" id="province" class="form-control m-bot15 choose province">
						               <option value="">--Chọn Quận Huyện--</option>
						           </select>
						        </div>
						        {{-- chọn xã phường --}}
						        <div class="form-group">
						          <label class="form-label">
						                 <font style="vertical-align: inherit;">Chọn Xã Phường</font>
						           </label> 
						           <select name="wards" id="wards" class="form-control m-bot15 wards">
						               <option value="">--Chọn Xã Phường--</option>  
						           </select>
						        </div>
						    </div>
						    <input type="button" name="delivery_order" value="Tính phí vận chuyển" 
						    class="btn btn-primary btn-sm calculate_delivery" >
						</form>
					</div>
				</div>
				{{-- XEM LẠI ĐỢN HÀNG --}}
				<div class="review-payment" style="margin-left: 20px">
					<h2>Xem Lại Giỏ Hàng</h2>
				</div>
				<div class="col-sm-12 ">
					<div class="table-responsive cart_info">
			         <form action="{{URL::to('/update-cart')}}" method="post">
			            @csrf
			   			<table class="table table-condensed">
			   				<thead>
			   					<tr class="cart_menu">
			   						<td class="description">Hình Ảnh</td>
			   						<td class="description">Tên Sản Phẩm</td>
			   						<td class="description">Giá Sản Phẩm</td>
			   						<td class="description">Số Lượng</td>
			   						<td class="description">Tổng Số Tiền</td>
			   						<td class="description-d"></td>
			   					</tr>
			   				</thead>
			   				<tbody>
			          @if(Session::get('cart')== true)
			          @php
			             $total = 0;
			          @endphp

			          @foreach(Session::get('cart') as $key => $cart)

			          @php
			             $subtotal = $cart['product_price'] * $cart['product_qty'];
			             $total += $subtotal;
			          @endphp
			   					<tr>
			                     {{-- ảnh --}}
			   						<td class="cart_product-id">
			   							<a href="">
			   								<img src="{{asset('public/img_product/'.$cart['product_img'])}}"width="120" height="80"
			   							  alt="{{$cart['product_name']}}"></a>
			   						</td>
			                     {{-- tên --}}
			   						<td class="cart_description">
			   							<h4><a href="">
			   								{{$cart['product_name']}}	
			   							</a></h4>
			   							<p style="float: left;">ID:{{$cart['product_id']}}</p>
			   						</td>
			                     {{-- giá --}}
			   						<td class="cart_price">
			   							<p style="float: left;">
			   								{{number_format($cart['product_price'],0,',','.')}} đ
			   							</p>
			   						</td>
			                     {{-- số lượng --}}
			   						<td class="cart_quantity">
			   							<div class="cart_quantity_button">
			   								<input class="cart_quantity_input" type="number" 
			                           name="cart_quantity[{{$cart['session_id']}}]" value="{{$cart['product_qty']}}" min="1">
			   							</div>
			   						</td>
			                     {{-- tổng tiền --}}
			   						<td class="cart_total">
			   							<p class="cart_total_price">
			                           {{number_format($subtotal,0,',','.')}} đ
			   							</p>
			   						</td>
			                     {{-- xóa đơn hàng --}}
			   						<td class="cart_delete">
			   							<a class="cart_quantity" href="{{url('/delete-cart/'.$cart['session_id'])}}"><i class="fa fa-times"></i></a>
			   						</td>
			   					</tr>                  
			          @endforeach		
			   				</tbody>
			             {{-- cập nhật đợn hàng --}}
			             <tr>
			                 <td class="update-order-all" colspan="1">
			                    <input type="submit" value="Cập Nhật Giỏ Hàng" name="update_qty" class="btn btn-info btn-sm-dd">
			                 </td>
			                {{--  --}}
			                 <td class="delete-order-all" colspan="1">
			                    {{-- xoá tất cả đơn hàng --}}
			                    <a class="btn btn-default delete" href="{{url('/delete-all-order')}}">Xóa Tất Cả Đơn Hàng</a>
			                 </td>
			                 
			                 <td colspan="2">
			                   <ul>
			                   	 {{-- tổng tiền --}}
			                      <li style="font-size: 22px">Tổng Tiền:
			                         <span style="color: #ff9c00;" >{{number_format($total,0,',','.')}} đ</span>
			                      </li>
				                  {{-- mã giảm giá --}}
			                      @if(Session::get('coupon'))
			                       <li style="font-size: 18px">
			                         <span>
			                            @foreach(Session::get('coupon') as $key => $cou)
			                                @if($cou['check'] == 1)
			                                  Mã Giảm Giá: {{$cou['number']}} %
			                                  <p>
			                                    @php
			                                      $total_coupon = ($total*$cou['number'])/100;
			                                    @endphp
			                                  </p>
			                                  <p>
			                                  	@php
			                                      $total_after_coupon = $total-$total_coupon;
			                                    @endphp
			                                  </p>
			                                @elseif($cou['check'] == 2)
			                                  Mã Giảm Giá: {{number_format($cou['number'],0,',','.')}} đ
			                                  <p>
			                                    @php
			                                      $total_coupon = $total-$cou['number'];		                                      
			                                    @endphp
			                                  </p>
			                                  @php
			                                      $total_after_coupon = $total_coupon;
			                                  @endphp
			                                @endif
			                            @endforeach
			                         </span>
			                      </li>
			                      @endif
			                      {{-- phí vận chuyển --}}
			                       @if(Session::get('fee'))
				                    <li style="font-size: 18px">Phí vận chuyển 
				                    	<span>{{number_format(Session::get('fee'),0,',','.')}} vnđ</span> 
				                    	<?php $total_after_fee = $total + Session::get('fee'); ?>
				                    	<a class="cart_quantity" href="{{url('/delete-fee')}}"><i class="fa fa-times"></i></a>
				                    </li>
				                  @endif
				                  <li style="font-size: 22px">Tổng thanh toán :
					                  <span style="font-size: 22px;color: red;">
						                  @php
						                  	if (Session::get('fee') && !Session::get('coupon')) {
						                  		$total_after = $total_after_fee;
						                  		echo number_format($total_after,0,',','.'). ' đ';

						                  	}elseif (!Session::get('fee') && Session::get('coupon')) {
						                  		$total_after = $total_after_coupon;
						                  		echo number_format($total_after,0,',','.'). ' đ';

						                  	}elseif (Session::get('fee') && Session::get('coupon')) {
						                  		$total_after = $total_after_coupon;
						                  		$total_after = $total_after + Session::get('fee');
						                  		echo number_format($total_after,0,',','.'). ' đ';

						                  	}elseif (!Session::get('fee') && !Session::get('coupon')) {
						                  		$total_after = $total;
						                  		echo number_format($total_after,0,',','.'). ' đ';

						                  	}
						                  @endphp
						              </span> 
					               </li>
			                   </ul> 
			             </tr> 
			             @else
			             <tr>
			                <td colspan="5" style="text-align: center;font-size: 20px">
			                   @php
			                      echo '<p>Giỏ hàng của bạn chưa có gì hết hãy thêm sản phẩm vào giỏ hàng</p>';
			                   @endphp
			                </td>
			             </tr>   
			             @endif
			 			  </table>
			      		</form>
			         {{-- vận chuyển --}}
			         @if(Session::get('cart'))
			          <td>
			            <form action="{{url('/check-coupon')}}" method="post">
			              @csrf
			              <ul class="user_option">
			                <li class="breadcrumb-item-vc" style="margin-top: -5px">Mã Giảm Giá</li>
			                <input type="text" name="coupon" class="form-control" id="coupon" style="width: 275px">
			                <input type="submit" value="Tính Mã Giảm Giá" name="check_coupon" class="btn btn-info btn-sm-dd-sm">
			                @if(Session::get('coupon'))
			                <a class="btn btn-default delete-gg" href="{{url('/unset-coupon')}}">Xóa Mã Giảm Giá</a>
			                @endif
			            </form>
			          </td>
			        @endif 
					</div>
				</div>					
			</div>
		</div>
	</div>
</section> <!--/#cart_items-->
@endsection
