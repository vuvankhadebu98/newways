@extends('client.welcome')
@section('content')

<div class="container ctvc mt-5">
   <div class="row" style="margin-top: 50px">
      <div class="col-sm-6" style="margin-left: 70px">
         <div class="contact-form">
            <h2 class="title text-center">Liên Hệ</h2>
            @if(session('thongbao'))
                     <div class="alert btn-success">
                        {{session('thongbao')}} 
                     </div>
                  @endif
            <div class="status alert alert-success" style="display: none"></div>
            <form action="{{URL::to('save-contact')}}" enctype="multipart/form-data" method="post">
               @csrf
               <div class="form-group col-md-5">
                  <input type="text" name="name" class="form-control" required="required" placeholder="Tên">
               </div>
               <div class="form-group col-md-7">
                  <input type="email" name="email" class="form-control" required="required" placeholder="Email">
               </div>
               <div class="form-group col-md-12">
                  <input type="text" name="title" class="form-control" required="required" placeholder="Tiêu đề bạn muốn nói">
               </div>
               <div class="form-group col-md-12">
                  <textarea name="content" id="content" required="required" class="form-control" rows="9" placeholder="Nhắn tin của bạn vào đây"></textarea>
               </div>
               <div class="form-group col-md-12">
                  <input type="submit" class="btn btn-primary pull-right" value="Gửi Tin">
               </div>
            </form>
         </div>
      </div>
      <div class="col-sm-4" style="margin-left: 50px">
         <div class="contact-info">
            <h2 class="title text-center">Thông Tin Liên Lạc</h2>
            <address>
               <p>NEW-Ways Inc.</p>
               <p>935 W. Webster Ave New Streets Chicago, IL 60614, NY</p>
               <p>Viện Nam VNĐ</p>
               <p>Mobile: +84 65 337 631</p>
               <p>Fax: 1-714-252-0026</p>
               <p>Email: vuvankhadebu98@gmail.com</p>
            </address>
            <div class="social-networks">
               <h2 class="title text-center" style="margin-top: 10px">Mạng Xã Hội</h2>
               <ul>
                  <li>
                     <a href="#"><i class="fa fa-facebook"></i></a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-twitter"></i></a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-google-plus"></i></a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-youtube"></i></a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection