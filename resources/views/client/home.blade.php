
@extends('client.welcome')
@section('content')
    <div class="container-fluid mega__slide">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-11">
                    <div id="carouselExampleControls" class="carousel slide mega__slide__box" data-ride="carousel">
                        <div class="carousel-inner">
                            @php 
                                $i = 0;
                            @endphp
                            @foreach($product_slider as $key =>$product_sl)
                            @php
                                $i++;
                            @endphp
                            <div class="carousel-item {{$i==1 ? 'active' : ''}}">
                                <div class="slide__text">
                                    <h5 class="slide__text--title" style="width: 250px;">
                                        {!!$product_sl->desc!!}
                                    </h5>
                                    <h3 class="slide__text--title">
                                        {{$product_sl->name}}
                                    </h3>
                                    <p class="slide__text--price">
                                        Chỉ Với
                                    <span class="slide__text--price--dolar">{{number_format($product_sl->price,0,',','.')}}. <sup>đ</sup></span>
                                    </p>
                                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product_sl->id)}}" class="btn slide__text--btn">
                                        Mua Ngay 
                                    </a>
                                </div>
                                <a href="{{URL::to('/chi-tiet-san-pham/'.$product_sl->id)}}">
                                <img src="public/img_product/{{$product_sl->img}}" class=" w-10" style="height: 400px; width: 790px" alt="..." />
                                </a>                                        
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next next-img" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
    </div>
    {{-- danh mục sản phẩm trên span --}}
    <div class="filter">
        <div class="manu">
            <a href="{{URL::to('/thuong-hieu-san-pham/2')}}" class="macbook">
                <img src="{{asset('public/frontend/img/Macbook44-b_41.png')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/1')}}" class="dell">
                <img src="{{asset('public/frontend/img/Dell44-b_34.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/3')}}"  class="hp">
                <img src="{{asset('public/frontend/img/HP-Compaq44-b_36.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/4')}}"  class="asus">
                <img src="{{asset('public/frontend/img/Asus44-b_35.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/5')}}"  class="lenovo">
                <img src="{{asset('public/frontend/img/Lenovo44-b_36.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/6')}}"  class="acer">
                <img src="{{asset('public/frontend/img/Acer44-b_37.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/7')}}"  class="huawei">
                <img src="{{asset('public/frontend/img/Huawei44-b_7.jpg')}}" style="width: 130px; height:27px;" >
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/8')}}"  class="msi">
                <img src="{{asset('public/frontend/img/MSI44-b_30.png')}}">
            </a>  
        </div>
    </div>
        <!-- banner column -->
        <div class="container">
            <div class="row banner__column">
                @foreach($slider as $key =>$slide)
                <div class="col-md-4 banner__column-item">
                    <a href="{{URL::to('/danh-muc-san-pham/'.$slide->id)}}" class="banner__column-item--link">
                       <img src="public/img_slider/{{$slide->img}}" class="img-fluid1 banner__column-item--img" alt="banner product" />
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <!-- danh mục sản phẩm -->
        <div class="container ctvc mt-5">
            <h3 class="ctvc__title">
                Danh mục sản phẩm
            </h3>
           <div class="row ctvc__border">
                @foreach($category as $key =>$cate)
                <div class="col-md-2 col-6 col-sm-4 ctvc__item">
                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product_sl->id)}}">
                    <img src="public/img_category/{{$cate->img}}" class="img-fluid ctvc__img-gd" alt="{{$cate->meta_desc}}" style="margin-top: 5px">
                    </a>
                    <div class="ctvc__item-content">
                        <a href="{{URL::to('/danh-muc-san-pham/'.$cate->id)}}">{{$cate->name}}</a>
                        <span>99 Mặt Hàng</span>
                    </div>
                </div>
                @endforeach
                <div class="ctvc__item-content text-center" style="width: 100%">
                    <a href="#">Tất cả danh mục<i class="fa fa-chevron-right"></i></a>
                </div>
           </div>
        </div>
         <!-- ctvp -->
        <div class="container ctvp">
            <h3 class="ctvp__title" style="margin-top: 30px">
                Sản phẩm nổi bật
            </h3>
            <div class="ctvp__box">
                <div class="carousel-inner ctvp__box__slide">
                    <div class="carousel-item active">
                        <div class=" row__item__product">
                            <!-- item product -->
                        @foreach($all_product as $key => $product)
                            <div class=" item__product">
                                <form>
                                    @csrf
                                    <input type="hidden" value="{{$product->id}}" class="cart_product_id_{{$product->id}}">
                                    <input type="hidden" value="{{$product->name}}" class="cart_product_name_{{$product->id}}">
                                    <input type="hidden" value="{{$product->img}}" class="cart_product_img_{{$product->id}}">
                                    <input type="hidden" value="{{$product->qty}}" class="cart_product_quantity_{{$product->id}}">
                                    <input type="hidden" value="{{$product->price}}" class="cart_product_price_{{$product->id}}">
                                    <input type="hidden" value="1" class="cart_product_qty_{{$product->id}}">
                                    <span class="onsale">
                                        <span class="saled">
                                            -17%
                                        </span>
                                        <br />
                                        <span class="featured">
                                            Hot
                                        </span>
                                    </span>
                                    <!-- group button on hover -->
                                    <div class="group-button">
                                        {{-- thêm giỏ hàng --}}
                                        <div class="cart-add" name="cart-add" data-id="{{$product->id}}"> 
                                            <i class="fa fa-cart-plus group-button--icon"></i>
                                        </div>
                                        {{--yêu thích hàng --}}
                                        <div class="btn-wishliss">
                                            <i class="fa fa-heart group-button--icon"></i>
                                        </div>
                                        {{-- chia sẻ --}}
                                        
                                        <div class="quick-view">
                                            <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
                                            <i class="fa fa-eye group-button--icon"></i>  
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- caption -->
                                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
                                        <div class="caption">
                                            <h3 class="name-product">{{$product->name}}</h3>
                                            <div class="price-product">
                                                <span class="price-product--cost">
                                                    {{number_format($product->price).' '.'VNĐ'}}
                                                </span>

                                                <span class="price-product--sale">
                                                    - {{number_format($product->price).' '.'VNĐ'}}
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
                                        <img src="{{URL::to('public/img_product/'.$product->img)}}" class="product__img" alt="" />
                                    </a>
                                </form>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- các sản phẩm --}}
        <div class="container ctvc mt-5">
            <h3 class="ctvc__title">
                Các sản Phẩm
            </h3>
           <div class="row ctvc__border">
            @foreach($product_all_home as $key => $product_all_h)
                <div class="ctvc__item product_home_index">
                    <div class=" item__product">
                        <form>
                            @csrf
                            <input type="hidden" value="{{$product_all_h->id}}" class="cart_product_id_{{$product_all_h->id}}">
                            <input type="hidden" value="{{$product_all_h->name}}" class="cart_product_name_{{$product_all_h->id}}">
                            <input type="hidden" value="{{$product_all_h->img}}" class="cart_product_img_{{$product_all_h->id}}">
                             <input type="hidden" value="{{$product_all_h->qty}}" class="cart_product_quantity_{{$product_all_h->id}}">
                            <input type="hidden" value="{{$product_all_h->price}}" class="cart_product_price_{{$product_all_h->id}}">
                            <input type="hidden" value="1" class="cart_product_qty_{{$product_all_h->id}}">
                            <span class="onsale">
                                <span class="saled">
                                    -17%
                                </span>
                                <br />
                                <span class="featured">
                                    Hot
                                </span>
                            </span>
                            <!-- group button on hover -->
                            <div class="group-button">
                                {{-- thêm giỏ hàng --}}
                                <div class="cart-add" name="cart-add" data-id="{{$product_all_h->id}}"> 
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </div>
                                {{--yêu thích hàng --}}
                                <div class="btn-wishliss">
                                    <i class="fa fa-heart group-button--icon"></i>
                                </div>
                                {{-- chia sẻ --}}
                                
                                <div class="quick-view">
                                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product_all_h->id)}}">
                                    <i class="fa fa-eye group-button--icon"></i>  
                                    </a>
                                </div>
                                
                            </div>
                            <!-- caption -->
                            <a href="{{URL::to('/chi-tiet-san-pham/'.$product_all_h->id)}}">
                                <div class="caption">
                                    <h3 class="name-product">{{$product_all_h->name}}</h3>
                                    <div class="price-product">
                                        <span class="price-product--cost">
                                            {{number_format($product_all_h->price).' '.'VNĐ'}}
                                        </span>

                                        <span class="price-product--sale">
                                            - {{number_format($product_all_h->price).' '.'VNĐ'}}
                                        </span>
                                    </div>
                                </div>
                            </a>
                            <a href="{{URL::to('/chi-tiet-san-pham/'.$product_all_h->id)}}">
                                <img src="{{URL::to('public/img_product/'.$product_all_h->img)}}" class="product__img" alt="" />
                            </a>
                        </form>
                    </div>
                </div>

            @endforeach
                <div class="ctvc__item-content text-center" style="width: 100%">
                    {{-- <a href="#">Shop all categories <i class="fa fa-chevron-right"></i></a> --}}
                    {{$product_all_home->links()}}
                </div>
            </div>
        </div>
    <!-- Blog -->
        <div class="container blog">
            <h3 class="blog__title">
                Blog
            </h3>
            <div class="blog__box">
                    <div class="carousel-inner ctvp__box__slide">
                        <div class="carousel-item active">
                            <div class="row__item__blog">
                            <!-- item blog---------------------------------------------------- -->
                            @foreach($news_home_2 as $key => $news_h_2)
                                <div class="item__blog">
                                    <div class="item__blog--box">
                                        <a href="{{URL::to('chi-tiet-tin-tuc/'.$news_h_2->id)}}">
                                            <img src="public/img_news/{{$news_h_2->img}}" class="img-fluid blog__img" alt="" style="height: 170px" />
                                        </a>
                                        <div class="blog__time">
                                            <div class="blog__time--date">
                                                <img src="{{('public/frontend/img/icon-calender.png')}}" class="img-fluid icon__blog" alt="" srcset="" />
                                                <p class="blog__time--date">{{Carbon\Carbon::parse($news_h_2->created_at)->format('d/m/Y') }}</p>
                                            </div>
                                            <div class="blog__time--comment">
                                                <img src="{{('public/frontend/img/commetn-calender.png')}}" class="img-fluid icon__blog" alt="" />
                                                <p class="comment__amount">
                                                    3
                                                </p>
                                            </div>
                                        </div>
                                        <h4 class="blog__title" style="height: 50px">
                                            <a href="{{URL::to('chi-tiet-tin-tuc/'.$news_h_2->id)}}" class="blog__title">
                                                {!!$news_h_2->desc!!}
                                            </a>
                                        </h4>
                                        <a href="{{URL::to('chi-tiet-tin-tuc/'.$news_h_2->id)}}" class="blog__link">
                                           Đọc thêm
                                            <i class="fa fa-chevron-right"></i>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                                
                            </div>
                        </div>
                    </div>
            </div>
        </div>

@endsection   