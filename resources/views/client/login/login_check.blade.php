@extends('client.welcome')
@section('content')

<section class="login-bast" id="form">
   <!--form-->
   <div class="container">
      <div class="row">
         <div class="col-sm-4 col-sm-offset-1">
            <div class="login-form">
               <!--login form-->
               <h2>Đăng Nhập Tài Khoản</h2>
               <form action="{{URL::to('login-customer')}}" method="post">
                  @csrf
                  {{-- email người dùng --}}
                  <input type="email" name="email" placeholder="email của bạn">
                  {{-- mật khẩu người dùng --}}
                  <input type="password" name="password" placeholder="password">
                  <span>
                  {{-- nút nhớ mật khẩu --}}
                  <input type="checkbox" class="checkbox"> 
                  Nhớ Mật Khẩu
                  </span>
                  {{-- nút đăng nhập --}}
                  <button type="submit" class="btn btn-default">Đăng Nhập</button>
               </form>
            </div>
            <!--/login form-->
         </div>
         <div class="col-sm-1">
            <h2 class="or">Hoặc</h2>
         </div>
         <div class="col-sm-5">
            <div class="signup-form">
               <!--sign up form-->
               <h2>Tạo Tài Khoản Mới!</h2>
               <form action="{{URL::to('add-customer')}}" method="post">
                  @csrf
                  {{-- họ và tên người dùng --}}
                  <input type="text" name="name" placeholder="Họ và tên">
                  {{-- giới tính người dùng --}}
                  <select name="sex" style="margin-bottom: 15px">
                     <option value="">Giới tính</option>
                     <option value="1">Nam</option>
                     <option value="2">Nữ</option>   
                  </select>
                  {{-- email người dùng --}}
                  <input type="email" name="email" placeholder="Địa chỉ Email">
                  {{-- mật khẩu người dùng --}}
                  <input type="password" name="password" placeholder="Mật khẩu">
                  {{-- số điện thoại --}}
                  <input type="number" name="phone" placeholder="Số điện thoại">
                  {{-- địa chỉ --}}
                  <input type="text" name="address" placeholder="Địa chỉ">
                  {{-- submit dăng ký --}}
                  <button type="submit" name="add" class="btn btn-default">Đăng Ký</button>
               </form>
            </div>
            <!--/sign up form-->
         </div>
      </div>
   </div>
</section>

@endsection