@extends('client.welcome')
@section('content')
<div class="card">
	<div class="col-sm-12">
	   <div class="blog-post-area">
		     <h2 class="title text-center">
		      	<font style="vertical-align: inherit;">
		      		<font style="vertical-align: inherit;">Mới nhất từ ​​tin tức của chúng tôi</font>
		      	</font>
		    </h2>
		    @foreach($news_home as $key =>$news_h)
		    <div class="single-blog-post col-lg-4" style="margin-top: 50px;">
		        <h3>
		         	<font style="vertical-align: inherit;">
		         		<font style="vertical-align: inherit;">{{$news_h->name}}</font>
		         	</font>
		        </h3>
		        <div class="post-meta">
		            <ul>
		                <li>
			               	<i class="fa fa-user"></i>
			               	<font style="vertical-align: inherit;">
			               		<font style="vertical-align: inherit;">{{$news_h->author}}</font>
			               	</font>
		                </li>
		                <li>
		               		<i class="fa fa-clock-o"></i>
		               		<font style="vertical-align: inherit;">
		               			<font style="vertical-align: inherit;">{{Carbon\Carbon::parse($news_h->created_at)->format('h:m:s') }}</font>
		               		</font>
		               	</li>
		                <li>
			               	<i class="fa fa-calendar"></i>
			               	<font style="vertical-align: inherit;">
			               		<font style="vertical-align: inherit;">{{Carbon\Carbon::parse($news_h->created_at)->format('d/m/Y') }}</font>
			               	</font>
		                </li>
		            </ul>
		            
		        </div>
		        <a href="{{URL::to('chi-tiet-tin-tuc/'.$news_h->id)}}">
		         	<img src="public/img_news/{{$news_h->img}}" alt="" width="100%" height="240px">
		        </a>
		        <div class="post-meta" style="float: left; margin-top: -20px">
		        	Đánh Giá :
			        <span style="margin-left: 3px">
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star-half-o"></i>
		            </span>
		        </div>
		        <div class="post-meta" style="float: left;height: 50px;width: 100%">
			        <span style="float: left;">
			            {!!$news_h->desc!!}
		            </span>
			    </div>
			    <div style="float: left">
			        <a class="btn btn-primary" href="{{URL::to('chi-tiet-tin-tuc/'.$news_h->id)}}" style="margin-top:-17px">
			        	<font style="vertical-align: inherit;">
			        		<font style="vertical-align: inherit;">Đọc thêm</font>
			        	</font>
			        </a>
		        </div>
	     	</div>
	     	@endforeach

	     	{{-- phân trang --}}
	      <div class="pagination-area col-lg-12">
	         <ul class="pagination">
	         </ul>
	      </div>
	   </div>
	</div>
</div>
@endsection