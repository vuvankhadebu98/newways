@extends('client.welcome')
@section('content')

@foreach($deltails_product as $key => $deltails_product)
{{-- danh mục sản phẩm trên span --}}
    <div class="filter">
        <div class="manu">
            <a href="{{URL::to('/thuong-hieu-san-pham/2')}}" class="macbook">
                <img src="{{asset('public/frontend/img/Macbook44-b_41.png')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/1')}}" class="dell">
                <img src="{{asset('public/frontend/img/Dell44-b_34.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/3')}}"  class="hp">
                <img src="{{asset('public/frontend/img/HP-Compaq44-b_36.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/4')}}"  class="asus">
                <img src="{{asset('public/frontend/img/Asus44-b_35.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/5')}}"  class="lenovo">
                <img src="{{asset('public/frontend/img/Lenovo44-b_36.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/6')}}"  class="acer">
                <img src="{{asset('public/frontend/img/Acer44-b_37.jpg')}}">
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/7')}}"  class="huawei">
                <img src="{{asset('public/frontend/img/Huawei44-b_7.jpg')}}" style="width: 130px; height:27px;" >
            </a>
            <a href="{{URL::to('/thuong-hieu-san-pham/8')}}"  class="msi">
                <img src="{{asset('public/frontend/img/MSI44-b_30.png')}}">
            </a>  
        </div>
    </div>
    {{-- chi tiết sản phẩm  --}}
    <div class="card">
      <div class="container-fliud">
        
        <div class="wrapper row">
          {{--  --}}
          <div class="col-sm-6">
             <div class="view-product" style="margin-left: 20px">
                <img src="{{URL::to('public/img_product/'.$deltails_product->img)}}" alt="">
             </div>
             <div id="similar-product" class="carousel slide" data-ride="carousel">
              <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        @foreach($show_product as $key =>$product_al)
                          <a href=""><img src="public/img_product/{{$product_al->img}}" alt="" style="width: 120px;height: 80px"></a>
                        @endforeach
                    </div>
                    <div class="item">
                      @foreach($show_product as $key =>$product_al)
                        <a href=""><img src="public/img_product/{{$product_al->img}}" alt="" style="width: 120px;height: 80px" ></a>
                      @endforeach
                    </div>
                    <div class="item">
                      @foreach($show_product as $key =>$product_al)
                        <a href=""><img src="public/img_product/{{$product_al->img}}" alt="" style="width: 120px;height: 80px" ></a>
                      @endforeach
                    </div>
                </div>
                  <!-- Controls -->
                  <a class="left item-control" href="#similar-product" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                  </a>
                  <a class="right item-control" href="#similar-product" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                  </a>
            </div>    
         </div>
        {{--  --}}
        <div class="col-md-6" style="float: right;">
            <div class="product-information">
                <!--/product-information-->
                <img src="public/frontend/images/product-details/new.jpg" class="newarrival" alt="">
                <h2>{{$deltails_product->name}}</h2>
                <h5 style="">ID: {{$deltails_product->id}}</h5>
                <img src="public/frontend/images/product-details/rating.png" alt="">
                {{-- form --}}
                <form action="{{URL::to('/show-cart')}}" method="post">
                    {{csrf_field()}}
                    <span>
                        <span>{{number_format($deltails_product->price)." ".'VNĐ'}}</span>
                        <label style="margin-top: -40px">Quantity:</label>
                        @csrf
                        <input type="hidden" value="{{$deltails_product->id}}" class="cart_product_id_{{$deltails_product->id}}">
                        <input type="hidden" value="{{$deltails_product->name}}" class="cart_product_name_{{$deltails_product->id}}">
                        <input type="hidden" value="{{$deltails_product->img}}" class="cart_product_img_{{$deltails_product->id}}">
                        <input type="hidden" value="{{$deltails_product->qty}}" class="cart_product_quantity_{{$deltails_product->id}}">
                        <input type="hidden" value="{{$deltails_product->price}}" class="cart_product_price_{{$deltails_product->id}}">
                        <input name="qty" type="number" min="1" value="1" class="cart_product_qty_{{$deltails_product->id}}">
                        <button type="button" class="btn btn-fefault cart-add" name="cart-add" data-id="{{$deltails_product->id}}">
                            <i class="fa fa-shopping-cart"></i>
                            Thêm vào Giỏ hàng
                        </button>
                    </span>
                </form>
                {{--  --}}
                <div>
                    <div style="float: left;">Tình Trạng :</div>
                    <font style="vertical-align: inherit;">
                         <?php
                           if($deltails_product->check == 0)
                           {
                           ?>
                            <div class="hienthi" id="0" style="color: #0DD902;"> Hết Hàng</div>
                           <?php
                           }elseif($deltails_product->check == 1){
                           ?>
                            <div class="hienthi" id="1" style="color: #09B5C8;"> Còn Hàng</div>
                           <?php
                           }
                         ?> 
                    </font>
                </div>
                <div>
                    <div style="float: left;">Hàng :</div>
                    <?php
                       if($deltails_product->check_new == 0)
                       {
                       ?>
                        <div class="hienthi" id="0" style="color: #0DD902;"> Đập Hộp</div>
                       <?php
                       }elseif($deltails_product->check_new == 1){
                       ?>
                        <div class="hienthi" id="1" style="color: #09B5C8;"> Mời</div>
                       <?php
                       }elseif($deltails_product->check_new == 2){
                       ?>
                        <div class="hienthi" id="1" style="color: #FFA54F;"> 99%</div>
                       <?php
                       }
                    ?> 
                </div>
                <div ><div>Mô Tả Sản Phẩm :</div>
                    {{$deltails_product->desc}} 
                </div>
                {{-- <a href=""><img src="public/frontend/images/product-details/share.png" class="share img-responsive" alt=""></a>
                <div id="fb-root"></div> --}}
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=341232103073116&autoLogAppEvents=1" nonce="nIlUW5RJ"></script>
                <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>

             </div>
             <!--/product-information-->
            </div>
        </div>
      </div>
    </div>
    {{--  --}}
    <div class="category-tab shop-details-tab" style="text-align: content;"><!--category-tab-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#details" data-toggle="tab" data-target="#details">Mô Tả Sản Phẩm</a></li>
                <li><a href="#companyprofile" data-toggle="tab" data-target="#companyprofile">Chi Tiết Sản Phẩm</a></li>
                <li><a href="#reviews" data-toggle="tab" data-target="#reviews" >Đánh Giá</a>
                <li><a href="#binhluan" data-toggle="tab" data-target="#binhluan" >Bình Luận</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" style="float: left; ">
            {{-- chi tiết --}}
            <div class="tab-pane fade active in col-sm-10" id="details" >
                <p>{!!$deltails_product->desc!!}</p>
            </div>
            {{-- hồ sơ công ty --}}
            <div class="tab-pane fade" id="companyprofile" style="display:none;">
                <p>{!!$deltails_product->content!!}</p>
            </div>
            {{-- đánh giá --}}
            <div class="tab-pane fade" id="reviews" style="display:none;">
                <div class="col-sm-11">
                    <ul>
                        <li><a href=""><i class="fa fa-user"></i>EUGEN</a></li>
                        <li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
                        <li><a href=""><i class="fa fa-calendar-o"></i>31 DEC 2014</a></li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <p><b>Write Your Review</b></p>
                    
                    <form action="#">
                        <span>
                            <input type="text" placeholder="Your Name"/>
                            <input type="email" placeholder="Email Address"/>
                        </span>
                        <textarea name="" ></textarea>
                        <b>Rating: </b> <img class="danhgia"src="public/frontend/images/product-details/rating.png" alt="" />
                        <button type="button" class="btn btn-default pull-right">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
            {{-- bìnhluan --}}
             <div class="tab-pane fade" id="binhluan" style="display:none;">
                <div class="col-sm-12">
                    <div id="fb-root"></div>
                        <script async defer crossorigin="anonymous" src="http://localhost:800/newways/chi-tiet-san-pham/" nonce="yi36j1qc"></script>
                        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="10" data-width="1300"></div>
                </div>
            </div>
        </div>
    </div>
    {{-- bình luận tổng thể --}}
    <div class="fb-comments" data-href="http://localhost/newways/chi-tiet-san-pham/7" data-numposts="20" data-width="1300"></div>
    <div class="col-sm-12">
        <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-width="1300" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div>
    </div>
    @endforeach
<!-- phẩm đẹp -->
{{-- sản phẩm nổi bật --}}
    <div class="container ctvp">
            <h3 class="ctvp__title" style="margin-top: 60px">
                Sản phẩm nổi bật
            </h3>
            <div class="ctvp__box">
                <div class="carousel-inner ctvp__box__slide">
                    <div class="carousel-item active">
                        <div class=" row__item__product">
                            <!-- item product -->
                        @foreach($all_product as $key => $product)
                            <div class=" item__product">
                                <form>
                                    @csrf
                                    <input type="hidden" value="{{$product->id}}" class="cart_product_id_{{$product->id}}">
                                    <input type="hidden" value="{{$product->name}}" class="cart_product_name_{{$product->id}}">
                                    <input type="hidden" value="{{$product->img}}" class="cart_product_img_{{$product->id}}">
                                    <input type="hidden" value="{{$product->qty}}" class="cart_product_quantity_{{$product->id}}">
                                    <input type="hidden" value="{{$product->price}}" class="cart_product_price_{{$product->id}}">
                                    <input type="hidden" value="1" class="cart_product_qty_{{$product->id}}">
                                    <span class="onsale">
                                        <span class="saled">
                                            -17%
                                        </span>
                                        <br />
                                        <span class="featured">
                                            Hot
                                        </span>
                                    </span>
                                    <!-- group button on hover -->
                                    <div class="group-button">
                                        {{-- thêm giỏ hàng --}}
                                        <div class="cart-add" name="cart-add" data-id="{{$product->id}}"> 
                                            <i class="fa fa-cart-plus group-button--icon"></i>
                                        </div>
                                        {{--yêu thích hàng --}}
                                        <div class="btn-wishliss">
                                            <i class="fa fa-heart group-button--icon"></i>
                                        </div>
                                        {{-- chia sẻ --}}
                                        <div class="quick-view">
                                            <i class="fa fa-eye group-button--icon"></i>  
                                        </div>
                                    </div>
                                    <!-- caption -->
                                    <a href="{{URL::to('/chi-tiet-san-pham/'.$product->id)}}">
                                        <div class="caption">
                                            <h3 class="name-product">{{$product->name}}</h3>
                                            <div class="price-product">
                                                <span class="price-product--cost">
                                                    {{number_format($product->price).' '.'VNĐ'}}
                                                </span>

                                                <span class="price-product--sale">
                                                    - {{number_format($product->price).' '.'VNĐ'}}
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                    <img src="{{URL::to('public/img_product/'.$product->img)}}" class="product__img" alt="" />
                                </form>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- test --}}
@endsection