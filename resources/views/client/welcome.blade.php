<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- link BS4 -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
        <!-- link fontawesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <!-- My css -->
        <link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}" />
        <link rel="stylesheet" href="{{asset('public/frontend/css/reset.css')}}" />
        <link rel="stylesheet" href="{{asset('public/frontend/css/reponsive-menu.css')}}">
        {{-- seo --}}
        {{-- <meta name="description" content="{!!$meta_desc!!}">
        <meta name="keywords" content="{{$meta_keywords}}">
        <meta name="robots" content="INDEX,FOLLOW">
        <link rel="canonical" href="{{$url_canonical}}">
        <meta name="author" content=""> --}}
        {{-- seo facebook --}}
        {{-- <meta property="og:images" content="{{$img_og}}">
        <meta property="og:site_name" content="http://localhost/newways/">
        <meta property="og:description" content="{{$meta_desc}}">
        <meta property="og:title" content="{{$meta_title}}">
        <meta property="og:url" content="{{$url_canonical}}">
        <meta property="og:type" content="website"> --}}
        {{-- end facebook --}}

        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="public/backend/assets/images/favicon.png">
        
        {{-- end seo --}}
        <title>home{{-- {{$meta_title}} --}}</title>

        <base href="{{ asset('client') }}">
        <link rel="stylesheet" href="{{asset('public/frontend/css/ctsp_product.css')}}">
        <link rel="stylesheet" href="{{asset('public/frontend/slick/slick/slick.css')}}">
        <link rel="stylesheet" href="{{asset('public/frontend/slick/slick/slick-theme.css')}}">
        <link href="public/frontend/css/sweetalert.css" rel="stylesheet">
        <link href="public/frontend/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/frontend/css/font-awesome.min.css" rel="stylesheet">
        <link href="public/frontend/css/prettyPhoto.css" rel="stylesheet">
        <link href="public/frontend/css/price-range.css" rel="stylesheet">
        <link href="public/frontend/css/animate.css" rel="stylesheet">
        <link href="public/frontend/css/main.css" rel="stylesheet">
        <link href="public/frontend/css/responsive.css" rel="stylesheet">
        <link rel="shortcut icon" href="public/frontend/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="public/frontend/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="public/frontend/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="public/frontend/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="public/frontend/images/ico/apple-touch-icon-57-precomposed.png">
    </head>
    <body>
        <!-- topbar -->
        <div class="container-fluid top__bar--box">
            <div class="container">
                <div class="row top__bar">
                    <div class="col-md-5 top__bar-left">
                        <p class="top__bar-left--phone">
                            <i class="fa fa-phone icon__phone"></i>
                            <span class="top__bar-left--text">
                                Hotline: 0965337631
                            </span>
                            <span> Email: vuvankhadebu98@gmail.com</span>
                        </p>
                    </div>
                    <div class="col-md-1 top__bar-middle">
                       
                    </div>
                    <div class="col-md-6 top__bar-right">
                        <div class="top__bar-right--social">
                            <div class="share_face">
                                <div class="share_ig" style="float: left; margin-top: 12px">
                                    <a href=""  ><i class="fa fa-instagram"></i></a>
                                    <a href=""  ><i class="fa fa-twitter"></i></a>
                                </div>
                                <div class="fb-share-button" data-href="http://localhost/newways/" data-layout="button_count" data-size="small">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{-- {{$url_canonical}} --}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a>
                                </div>
                                
                            </div>    
                        </div>
                        <div class="top__bar-right--language">
                            <span>
                                <img src="{{asset('public/frontend/img/en.png')}}" class="img-fluid img__language" alt="USA" />
                                : English
                                {{-- <i class="fa fa-angle-down"></i> --}}
                                <img src="{{asset('public/frontend/img/vn.png')}}" class="img-fluid img__language" alt="USA" />
                                : Việt Nam
                            </span>
                        </div>
                        <div class="dropdown">
                        <?php
                            $customer_id = Session::get('id');
                            if ($customer_id != null) {   
                        ?>
                            <a href="#" class="nav-link pr-0 leading-none user-img" data-toggle="dropdown" aria-expanded="false">
                           <img src="public/img_user/MIS_GS63_7re37.jpg" alt="profile-img" class="avatar avatar-md brround">
                           </a>
                           <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="bottom-end" style="position: absolute; transform: translate3d(100px, 56px, 0px); top: 0px; left: 0px; will-change: transform;"> 
                              <a class="dropdown-item" href=""> 
                              <i class="dropdown-icon icon icon-user"></i> 
                              Thông tin tài khoản 
                              </a> 
                              <a class="dropdown-item" href="editprofile.html"> 
                              <i class="dropdown-icon  icon icon-settings"></i>
                              Cài đặt tài khoản 
                              </a>
                              <a class="dropdown-item" href="{{URL::to('logout-checkout')}}">
                              <i class="dropdown-icon icon icon-power"></i> 
                              Đăng Xuất
                              </a>
                           </div>
                        <?php
                        }else{
                        ?>
                            <div class="login" >
                                <a class="registration" href="{{URL::to('/login-checkout')}}" >Đăng Ký </a> | 
                                <a class="login-customer" href="{{URL::to('/login-checkout')}}">Đăng Nhập</a>
                            </div>
                        <?php
                        }
                        ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- header-main -->
        <div class="container header-main">
            <div class="row header-main">
                <!-- logo -->
                <div class="col-md-3 header__logo">
                    <a href="{{URL::to('trang-chu')}}" class="header__logo--link">
                        <img src="{{asset('public/frontend/img/logo-new-ways.jpg')}}" class="img-fluid header__logo--img" alt="logo" style="margin-top: 13px" />
                    </a>
                </div>
                <!-- box search -->
                <div class="col-md-5 header__search">
                    <form action="{{URL::to('tim-kiem')}}" method="post">
                        @csrf
                        <div class="box__search">
                            <input type="text" class="import__search" name="keywords_submit" placeholder="Bạn muốn tìm gì ?" />
                            <button type="submit" class="submit__search" name="search_items">
                                <i class="fa fa-search submit__search--icon"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 header__right">
                    <a href="" class="top-wishlist">
                        <i class="fa fa-heart header__right--icon"></i>

                        <span class="header__right--number1">0</span>
                    </a>
                    <a href="{{URL::to('show-cart')}}" class="top-cart">
                        <i class="fa fa-shopping-cart header__right--icon"></i>
                        <span class="header__right--number2">0</span>
                        <p class="top-cart--info">
                            Giỏ hàng
                        </p>
                    </a>
                    <?php
                        $customer_id = Session::get('id');
                        $shipping_id = Session::get('id');
                        if ($customer_id != null && $shipping_id==null) 
                        {   
                    ?>  
                        <a href="{{URL::to('show-cart')}}" class="top-cart" style="margin-left: 85px">
                            <i class="fa fa-truck banner--icon header__right--icon"></i>
                            <p class="top-cart--info" style="margin-left: 97px">
                                Thanh Toán
                            </p>
                        </a>
                    <?php
                        }elseif($customer_id != null && $shipping_id!=null)
                        {
                    ?>
                        <a href="{{URL::to('/checkout')}}" class="top-cart" style="margin-left: 85px">
                            <i class="fa fa-truck banner--icon header__right--icon"></i>
                            <p class="top-cart--info" style="margin-left: 97px">
                                Thanh Toán
                            </p>
                        </a>
                    <?php
                        }else
                        {
                    ?>
                        <a href="{{URL::to('login-checkout')}}" class="top-cart" style="margin-left: 85px">
                            <i class="fa fa-truck banner--icon header__right--icon"></i>
                            <p class="top-cart--info" style="margin-left: 97px">
                                Thanh Toán
                            </p>
                        </a>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <!-- menu -->
        <div class="container-fluid main-menu">
            <!-- main menu -->
            <div class="container main-menu">
                <div class="row">
                    <ul class="top__menu col-md-9">
                        <li class="menu__item">
                            <a href="{{URL::to('/trang-chu')}}" class="menu__link menu__link--home"> Trang chủ</a>
                        </li>
                        {{-- danh mục sản phẩm --}}
                        <li class="menu__item">
                            <span class="menu__link"> Danh mục sản phẩm </span>
                            <ul class="mega__menu">
                                <div class="row row__mega__menu">
                                @foreach($category as $key => $cate_home)
                                    <div class="col-md-5 column__mega__menu">
                                        <li class="mega__menu--item">
                                            <a href="{{URL::to('/danh-muc-san-pham/'.$cate_home->id)}}" class="mega__menu--link mega__menu--title">
                                                {{$cate_home->name}}
                                            </a>
                                        </li>
                                    </div>
                                @endforeach
                                </div>
                            </ul>
                        </li>
                        {{-- rât tốt --}}
                        {{-- thương hiệu sản phẩm --}}
                        <li class="menu__item">
                            <span class="menu__link">Thương hiệu</span>
                            <ul class="mega__menu" style="width: 300px">
                                <div class="row row__mega__menu">
                                @foreach($brand_home as $key => $brand_h)
                                    <div class="col-md-4 column__mega__menu" style="margin-left: 35px">
                                        <li class="mega__menu--item">
                                            <a href="{{URL::to('thuong-hieu-san-pham/'.$brand_h->id)}}" class="mega__menu--link mega__menu--title">
                                                {{$brand_h->name}}
                                            </a>
                                        </li>
                                    </div>
                                @endforeach
                                </div>
                            </ul>
                        </li>
                        {{-- tin tức --}}
                        <li class="menu__item">
                            <a href="{{URL::to('list-news')}}" class="menu__link"> Tin tức </a>
                        </li>
                        {{-- chính sách --}}
                        <li class="menu__item">
                            <span class="menu__link"> Chính sách </span>
                            <ul class="sub__menu ">
                            @foreach($warranty_home as $key =>$warranty_h)
                                <li class="sub__menu--link">
                                    <a href="{{URL::to('chinh-sach/'.$warranty_h->id)}}" class="sub__menu--link">
                                        {{$warranty_h->name}}
                                    </a>
                                </li>
                            @endforeach
                            </ul>
                        </li>
                        {{-- liên hệ --}}
                        <li class="menu__item">
                            <a href="{{URL::to('list-contact-home')}}" class="menu__link"> Liên hệ </a>
                        </li>
                    </ul>
                    {{-- <div class="col-md-3">
                    </div> --}}
                </div>
            </div>
        </div>
    
         @yield('content')
         
        <!-- footer -->
        <!-- add 'footer' snippet in css -->
        <div class="footer-v1 bg-img footer__wrap">
            <div class="footer no-margin">
                <div class="container">
                    <div class="row pt-40 pt-20 pl-3">
                        <div class="col-md-5 footer__column">
                            <div class="headline">
                                <h3 class="headline--title">
                                    About Us:
                                </h3>
                                <p class="headline--detail">
                                    Soft Mart chuyên cung đồ công nghệ đạt tiêu chuẩn hàng đầu . Shop đã phục vụ mọi lứa tuổi từ trẻ đến già đến nay đã được 10 năm và được mọi người quan tâm, ủng hộ.

                                    Soft Mart cung cấp đầy đủ các sản phẩm công nghệ đạt chất lượng tiêu chuẩn. Các sản phẩm đều sản xuất bởi các hãng thời trang có thương hiệu nổi tiếng.

                                    Soft Mart luôn phục vụ nhiệt tình mọi người, rất mong nhận được sự ủng hộ và đóng góp ý kiến để nâng cao chất lượng dịch vụ tốt hơn! 
                                </p>
                            </div>
                            <ul class="list-unstyled link-list">
                                <li>
                                    <a href="#">Địa chỉ: CT2 Constrexim Thái Hà, Phạm Văn Đồng, Bắc Từ Liêm, Hà Nội</a>
                                </li>
                                <li><a href="#">Số điện thoại: 0927151535</a></li>
                                <li>
                                    <a href="#" class="footer__social">
                                        <i class="fa fa-facebook footer__social--icon"></i>
                                        <i class="fa fa-instagram footer__social--icon"></i>
                                        <i class="fa fa-twitter-square footer__social--icon"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-6 footer__column">
                            <div class="headline">
                                <h3 class="headline--title">
                                    Chính sách
                                </h3>
                            </div>
                            <ul class="list-unstyled link-list">
                                <li><a href="#">Bảo hành</a></li>
                                <li><a href="">Cài đặt</a></li>
                                <li><a href="">Nâng cấp</a></li>
                                <li><a href="">Gia hạn</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-6 footer__column">
                            <div class="headline">
                                <h3 class="headline--title">
                                    Danh mục phần mềm
                                </h3>
                            </div>
                            <ul class="list-unstyled link-list">
                                <li><a href="#"> Phần mềm A </a></li>
                                <li><a href="#">Phần mềm B </a></li>
                                <li><a href="#">Phần mềm C</a></li>
                                <li><a href="#">Phần mềm D</a></li>
                                <a href="" class="footer__readmore--category"> Xem tất cả <i class="fa fa-chevron-right"></i></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer bottom -->
            <div class="container-fluid footer__bottom-hr">
                <div class="container">
                    <div class="row footer__bottom">
                        <div class="col-md-6 pt-20">
                            <p class="footer__coppyright">
                                Copyright © 2020 .
                            </p>
                        </div>
                        <div class="col-md-6 footer__flow pt-20">
                            <div class="footer__flow--bell">
                                <p class="footer__flow--text">
                                    Đăng ký để nhận thông tin.
                                </p>
                            </div>
                            <div class="footer__flow--box">
                                <input type="text" class="footer__flow--form" placeholder="Nhập Email của bạn ..." />
                                <button type="submit" class="footer__flow--btn">
                                    <i class="fa fa-envelope footer__flow--icon"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="public/frontend/js/jquery.js"></script>
<script src="public/frontend/js/bootstrap.min.js"></script>
<script src="public/frontend/js/jquery.scrollUp.min.js"></script>
<script src="public/frontend/js/price-range.js"></script>
<script src="public/frontend/js/jquery.prettyPhoto.js"></script>
<script src="public/frontend/js/main.js"></script>
<script src="public/frontend/js/sweetalert.min.js"></script>
{{-- zalo --}}
<div class="zalo-chat-widget" data-oaid="4258329992417286737" data-welcome-message="Rất vui khi được hỗ trợ bạn!" data-autopopup="0" data-width="350" data-height="420"></div>

<script src="https://sp.zalo.me/plugins/sdk.js"></script>
{{-- chính sủa zalo --}}
{{-- share face --}}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=341232103073116&autoLogAppEvents=1" nonce="s60guXbK"></script>
{{-- end facebook --}}
<script sype="text/javascript">
    $(document).ready(function(){
        $('.send_order_product').click(function(){
            swal({
              title: "Xác Nhận Đơn Hàng?",
              text: "Đơn hàng sẽ không được hoàn trả khi đặt, bạn có chắc muốn đặt!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Cảm Ơn, Mua Hàng!",
              cancelButtonText: "Không, hủy bỏ!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    var name = $('.name' ).val();
                    var email = $('.email' ).val();
                    var address = $('.address' ).val();
                    var phone = $('.phone').val();
                    var notes = $('.notes' ).val();
                    var method = $('.payment_select' ).val();
                    var order_fee = $('.order_fee').val();
                    var order_coupon = $('.order_coupon').val();
                    var _token = $('input[name="_token"]').val();

                    $.ajax({
                        url: '{{url('/confirm-order')}}',
                        method: 'POST',
                        data:{name:name,
                            email:email,
                            address:address,
                            phone:phone,
                            notes:notes,
                            method:method,
                            order_fee:order_fee,
                            order_coupon:order_coupon,
                            _token:_token},
                        success:function(data){
                            swal("Đơn Hàng!", "Đơn hàng của bạn đã được gửi thành công.", "success");
                        }
                    });
                    window.setTimeout(function(){
                        location.reload();
                    } ,1000);
                }else {
                    swal("Đóng", "Đơn hàng chưa được gửi, làm ơn hàn tất đơn hàng", "error");
                }
            }); 
        });
    });
</script> 
<script type="text/javascript">
    $(document).ready(function(){
        $('.cart-add').click(function(){
            var id = $(this).data('id');
            var cart_product_id = $('.cart_product_id_' + id).val();
            var cart_product_name = $('.cart_product_name_' + id).val();
            var cart_product_img = $('.cart_product_img_' + id).val();
            var cart_product_quantity = $('.cart_product_quantity_' + id).val();
            var cart_product_price = $('.cart_product_price_' + id).val();
            var cart_product_qty = $('.cart_product_qty_' + id).val();
            var _token = $('input[name="_token"]').val();
            if (parseInt(cart_product_qty) > parseInt(cart_product_quantity)) {
                alert('Làm ơn đặt nhỏ hơn ' + cart_product_quantity);
            }else {
                $.ajax({
                    url: '{{url('/add-cart-ajax')}}',
                    method: 'POST',
                    data:{cart_product_id:cart_product_id,
                        cart_product_name:cart_product_name,
                        cart_product_img:cart_product_img,
                        cart_product_quantity:cart_product_quantity,
                        cart_product_price:cart_product_price,
                        cart_product_qty:cart_product_qty,
                        _token:_token},
                    success:function(data){
                        swal({
                            title: "Đã thêm sản phẩm vào giỏ hàng",
                            text: "Bạn có thể mua hàng tiếp hoặc tới giỏ hàng để tiến hành thanh toán",
                            showCancelButton: true,
                            cancelButtonText: "Xem tiếp",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Đi đến giỏ hàng",
                            closeOnConfirm: false
                        },
                        function() {
                            window.location.href = "{{url('/show-cart')}}";
                        });
                    }
                });
            }
        });
    });
</script>   

<script type="text/javascript">
    function openNav() {
      document.getElementById("mySidepanel").style.width = "250px";
    }

    function closeNav() {
      document.getElementById("mySidepanel").style.width = "0";
    }
</script>

<script>
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active-1");
      var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
          dropdownContent.style.display = "none";
        } 
        else {
          dropdownContent.style.display = "block";
        }
      });
    }
</script>
<script sype="text/javascript">
    // chọn tỉnh,quận huyện,xã phường.........
    $(document).ready(function(){
        $('.choose').on('change',function(){
        var action = $(this).attr('id');
        var ma_id = $(this).val();
        var _token = $('input[name="_token"]').val();
        var result = '';
        if (action=='city'){
            result = 'province';
        }
        else {
            result = 'wards';
        }
        $.ajax({
            url : '{{url('/select-delivery-home')}}',
            method: 'POST',
            data:{action:action,ma_id:ma_id,_token:_token},
            success:function(data){
                $('#'+result).html(data);
            }
        });
        });
    }); 
</script>
{{-- phí vận chuyển --}}
<script sype="text/javascript">
    $(document).ready(function(){
        $('.calculate_delivery').click(function(){
            var matp = $('.city').val();
            var maqh = $('.province').val();
            var xaid = $('.wards').val();
            var _token = $('input[name="_token"]').val();
            if (matp == '' && maqh == '' && xaid == ''){
                alert('Bạn chưa chọn địa chỉ tính phí vận chuyển');
            }
            else{
                $.ajax({
                url : '{{url('/calculate-fee')}}',
                method: 'POST',
                data:{matp:matp,maqh:maqh,xaid:xaid,_token:_token},
                success:function(){
                    location.reload();
                    }
                });
            }
        });
    });
</script>   
<script src="{{asset('public/frontend/slick/slick/slick.js')}}"></script>

<script type="text/javascript">
    $('.row__item__product').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      dots:true,
      autoplaySpeed: 2000,
      responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 550,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 450,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }

          ]
});
</script>

<script type="text/javascript">
    $('.row__item__blog').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      dots:true,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 2000,
      responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 550,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 450,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }

          ]
});
</script>

</body>
</html>
