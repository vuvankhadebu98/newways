<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//client...........................................................................................................................

// Route::get('/', function () {
//     return view('client.welcome');
// });

//frontend..................................................
Route::group(['namespace' => 'client'], function (){
	
	// homecontroller
	Route::group(['namespace' => 'home'], function () {
		Route::get('/', 'HomeController@index');
		Route::get('/trang-chu','HomeController@index');
		Route::post('tim-kiem','HomeController@search');
		// lõi 404 .........................................
		Route::get('404','HomeController@error_home');

	});	

	// danh-muc-sản-phảm trang chủ------------------------
	Route::group(['namespace' => 'category'], function () {
		Route::get('/danh-muc-san-pham/{id}','CategoryController@show_category_home');
	});
	// thuong-hieu-san-pham trang chủ
	Route::group(['namespace' => 'brand'], function () {
		Route::get('/thuong-hieu-san-pham/{id}','BrandController@show_brand_home');
	});
	// product....................................................
	Route::group(['namespace' => 'product'], function () {
		Route::get('/chi-tiet-san-pham/{id}','ProductController@details_product');
	});
	// warranty....................................................
	Route::group(['namespace' => 'warranty'], function () {
		Route::get('/chinh-sach/{id}','WarrantyController@detail_warranty');
	});
	// cart............................................................	
	Route::group(['namespace' => 'cart'], function () {
		Route::get('/show-cart','CartController@show_cart');
		Route::get('/delete-cart/{session_id}','CartController@delete_cart');
		Route::get('/delete-all-order','CartController@delete_all_order');
		Route::post('/save-cart','CartController@save_cart');
		Route::post('/add-cart-ajax','CartController@add_cart_ajax');
		Route::post('/update-cart','CartController@update_cart');
		// coupon.........................................................
		Route::post('check-coupon','CartController@check_coupon');
		Route::get('/unset-coupon','CartController@unset_coupon');
	});	
	// checkout....................................................................................
	Route::group(['namespace' => 'checkout'], function () {
		Route::get('/login-checkout','CheckoutController@login_checkout');
		Route::get('/logout-checkout','CheckoutController@logout_checkout');
		Route::get('/checkout','CheckoutController@checkout');
		// customer................................................................................
		Route::post('/login-customer','CheckoutController@login_customer');
		Route::post('/add-customer','CheckoutController@add_customer');
		Route::post('/save-checkout-customer','CheckoutController@save_checkout_customer');
		// payment........................................................................................
		Route::get('/payment','CheckoutController@payment');
		// delete-feeship.........................................................................
		Route::get('/delete-fee','CheckoutController@delete_fee');
		// order_place.............................................................................
		Route::post('order-place','CheckoutController@order_place');
		// select-delivery-home
		Route::post('select-delivery-home','CheckoutController@select_delivery_home');
		// calculate-fee
		Route::post('calculate-fee','CheckoutController@calculate_fee');
		// confirm-order
		Route::post('confirm-order','CheckoutController@confirm_order');
	});
	// concact..............................................................................
	Route::group(['namespace' => 'contact'], function () {
		//list_contact..............................................
		Route::get('list-contact-home','ContactController@list_contact');
		// save_contact......................................
		Route::post('save-contact','ContactController@save_contact');
	});
	// news..............................................................................
	Route::group(['namespace' => 'news'], function () {
		//list_contact..............................................
		Route::get('list-news','NewsController@list_news');
		// chi-tiet-tin-tức..............................
		Route::get('/chi-tiet-tin-tuc/{id}','NewsController@show_detail_news');
	});

});



//admin..............................................................................................................................

Route::group(['namespace' => 'admin'], function () {

	//user..................................................................................................
	Route::group(['namespace' => 'user'], function () {
		// login........................................................
		Route::get('login-user','UserController@login_user');
		// admin-login....................................................
		Route::post('admin-login','UserController@admin_login');
		// logout.........................................................
		Route::get('logout','UserController@logout');
		// tổng quan......................................................
		Route::get('tong-quan','UserController@tong_quan');	

		//Login facebook
		Route::get('/login-facebook','UserController@login_facebook');
		Route::get('/login-user/callback','UserController@callback_facebook');
		//Login  google
		Route::get('/login-google','UserController@login_google');
		Route::get('/google/callback','UserController@callback_google');

		// ...............................................................................
		// thêm sửa xóa brand...........................................
		Route::get('add-user','UserController@add_user');
		Route::get('all-user','UserController@all_user');
		Route::get('edit-user/{admin_id}','UserController@edit_user');
		Route::get('delete-user/{admin_id}','UserController@delete_user');
		// lưu,cập nhật brand...........................................
		Route::post('save-user','UserController@save_user');
        Route::post('update-user/{admin_id}','UserController@update_user');
        //hiển thị, không hiển thị blog.........
        Route::get('active-user/{admin_id}','UserController@active_user');
        Route::get('unactive-user/{admin_id}','UserController@unactive_user');
        // chức năng tìm kiếm....................
        Route::post('search-user','UserController@search_user')->name('search-user');

	});

	//brand................................................................................................
	Route::group(['namespace' => 'brand'], function () {
		// thêm sửa xóa brand...........................................
		Route::get('add-brand','BrandController@add_brand');
		Route::get('all-brand','BrandController@all_brand');
		Route::get('edit-brand/{id}','BrandController@edit_brand');
		Route::get('delete-brand/{id}','BrandController@delete_brand');
		// lưu,cập nhật brand...........................................
		Route::post('save-brand','BrandController@save_brand');
        Route::post('update-brand/{id}','BrandController@update_brand');
        //hiển thị, không hiển thị blog.........
        Route::get('active-brand/{id}','BrandController@active_brand');
        Route::get('unactive-brand/{id}','BrandController@unactive_brand');
        // chức năng tìm kiếm....................
        Route::post('search-brand','BrandController@search_brand')->name('search-brand');
	});

	//category................................................................................................
	Route::group(['namespace' => 'category'], function () {
		// thêm sửa xóa category...........................................
		Route::get('add-category','CategoryController@add_category');
		Route::get('all-category','CategoryController@all_category');
		Route::get('edit-category/{id}','CategoryController@edit_category');
		Route::get('delete-category/{id}','CategoryController@delete_category');
		// lưu,cập nhật category...........................................
		Route::post('save-category','CategoryController@save_category');
        Route::post('update-category/{id}','CategoryController@update_category');
        //hiển thị, không hiển thị blog.........
        Route::get('active-category/{id}','CategoryController@active_category');
        Route::get('unactive-category/{id}','CategoryController@unactive_category');
        // chức năng tìm kiếm....................
        Route::post('search-category','CategoryController@search_category')->name('search-category');
	});
	//product................................................................................................
	Route::group(['namespace' => 'product'], function () {
		// tìm kiếm danh mục và thương hiệu sản phẩm theo ajax ............................
        Route::get('load-list-brand','ProductController@load_brand_product')->name('change.brand');
		// thêm sửa xóa product...........................................
		Route::get('add-product','ProductController@add_product');
		Route::get('all-product','ProductController@all_product');
		Route::get('edit-product/{id}','ProductController@edit_product');
		Route::get('delete-product/{id}','ProductController@delete_product');
		// lưu,cập nhật product...........................................
		Route::post('save-product','ProductController@save_product');
        Route::post('update-product/{id}','ProductController@update_product');
        //hiển thị, không hiển thị blog.........
        Route::get('active-product/{id}','ProductController@active_product');
        Route::get('unactive-product/{id}','ProductController@unactive_product');
        // chức năng tìm kiếm....................
        Route::post('search-product','ProductController@search_product')->name('search-product');
        // xuất file,và lấy file
        Route::post('export-csv','ProductController@export_csv');
		Route::post('import-csv','ProductController@import_csv');

	});
	//order................................................................................................
	Route::group(['namespace' => 'order'], function () {
		// lọc order bàng ajax-active
		Route::get('load-order-active','OrderController@select_active_order')->name('load.order.active');
		// lọc order bằng ajax-unactive
        Route::get('load-order-unactive','OrderController@select_unactive_order')->name('load.order.unactive');
        // lọc order bằng ajax-no-active
        Route::get('load-order-no-active','OrderController@select_no_active_order')->name('load.order.no.active');
        // view_order
		Route::get('view-order','OrderController@view_order');
        // show-order.............................
        Route::get('show-order/{code}','OrderController@show_order');
        //tìm kiếm
        Route::post('search-order','OrderController@search_order')->name('search-order');
        // in đơn hàng(print_order)..............................
        Route::get('print-order/{checkout_code}','OrderController@print_order');
        // update_order_qty)..............................
        Route::post('update-order-qty','OrderController@update_order_qty');
        // update_qty..............................
        Route::post('update-qty','OrderController@update_qty');
	});
	//coupon................................................................................................
	Route::group(['namespace' => 'coupon'], function () {
		// thêm sửa xóa product...........................................
		Route::get('add-coupon','CouponController@add_coupon');
		Route::get('all-coupon','CouponController@all_coupon');
		Route::get('edit-coupon/{id}','CouponController@edit_coupon');
		Route::get('delete-coupon/{id}','CouponController@delete_coupon');
		// lưu,cập nhật coupon...........................................
		Route::post('save-coupon','CouponController@save_coupon');
        Route::post('update-coupon/{id}','CouponController@update_coupon');
        //hiển thị, không hiển thị blog.........
        Route::get('active-coupon/{id}','CouponController@active_coupon');
        Route::get('unactive-coupon/{id}','CouponController@unactive_coupon');
        // chức năng tìm kiếm....................
        Route::post('search-coupon','CouponController@search_coupon')->name('search-coupon');
	});
	//delivery................................................................................................
	Route::group(['namespace' => 'delivery'], function () {
		// thêm delivery...........................................
		Route::get('add-delivery','DeliveryController@add_delivery');
		
		// lưu,cập nhật delivery...........................................
		Route::post('save-delivery','DeliveryController@save_delivery');
        Route::get('update-delivery','CouponController@update_delivery');
 		// hiển thị delivery..................................
		Route::post('/select-delivery','DeliveryController@select_delivery');
		Route::post('/select-feeship','DeliveryController@select_feeship');
	});
	// contact......................................................................................................
	Route::group(['namespace' => 'contact'], function(){
		// lọc các contact đã được xử lý.............................................
		Route::get('load-contact-active','ContactController@select_active_contact')->name('load.contact.active');
		// lọc các contact chưa được xử lý
		Route::get('load-contact-unactive','ContactController@select_unactive_contact')->name('load.contact.unactive');
		 // chức năng tìm kiếm....................
        Route::post('search-contact','ContactController@search_contact')->name('search-contact');
		// hiển thị danh sách contact
		Route::get('list-contact','ContactController@list_contact');
		// xóa liên hệ
		Route::get('delete-contact/{id}','ContactController@delete_contact');
		//xử lý liên hệ và đang xử lý chưa liên hệ.........
        Route::get('active-contact/{id}','ContactController@active_contact');
        Route::get('unactive-contact/{id}','ContactController@unactive_contact');

	});

	// news(tin tức)......................................................................................................
	Route::group(['namespace' => 'news'], function(){
		// // lọc các contact đã được xử lý.............................................
		// Route::get('load-contact-active','ContactController@select_active_contact')->name('load.contact.active');
		// // lọc các contact chưa được xử lý
		// Route::get('load-contact-unactive','ContactController@select_unactive_contact')->name('load.contact.unactive');
		//  // chức năng tìm kiếm....................
  //       Route::post('search-contact','ContactController@search_contact')->name('search-contact');
		// thêm sửa xóa tin tức...........................................
		Route::get('add-news','NewsController@add_news');
		Route::get('all-news','NewsController@all_news');
		Route::get('edit-news/{id}','NewsController@edit_news');
		Route::get('delete-news/{id}','NewsController@delete_news');
		// lưu,cập nhật news...........................................
		Route::post('save-news','NewsController@save_news');
        Route::post('update-news/{id}','NewsController@update_news');
        //hiển thị, không hiển thị news.........
        Route::get('active-news/{id}','NewsController@active_news');
        Route::get('unactive-news/{id}','NewsController@unactive_news');
        // chức năng tìm kiếm....................
        Route::post('search-news','NewsController@search_news')->name('search-news');
	});

	// slider.....................................................................................................................
	Route::group(['namespace' => 'slider'], function(){
		Route::post('search_slider','SliderController@search_slider')->name('search-slider');
		// add_slider...................................
		Route::get('add-slider','SliderController@add_slider');
		// list_slider..................................
		Route::get('list-slider','SliderController@list_slider');
		// edit_slider.......................................
		Route::get('edit-slider/{id}','SliderController@edit_slider');
		// delete_slider..................................
		Route::get('delete-slider/{id}','SliderController@delete_slider');
		//hiển thị, không hiển thị blog.........
        Route::get('active-slider/{id}','SliderController@active_slider');
        Route::get('unactive-slider/{id}','SliderController@unactive_slider');
		// save_slider....................................
		Route::post('save-slider','SliderController@save_slider');
		// update_slider..................................
		Route::post('update-slider/{id}','SliderController@update_slider');
	});
	// warranty.............................................................................................................
	Route::group(['namespace' => 'warranty'], function(){
		Route::post('search_warranty','WarrantyController@search_warranty')->name('search-warranty');
		// add_slider...................................
		Route::get('add-warranty','WarrantyController@add_warranty');
		// list_slider..................................
		Route::get('all-warranty','WarrantyController@all_warranty');
		// edit_slider.......................................
		Route::get('edit-warranty/{id}','WarrantyController@edit_warranty');
		// delete_slider..................................
		Route::get('delete-warranty/{id}','WarrantyController@delete_warranty');
		//hiển thị, không hiển thị blog.........
        Route::get('active-warranty/{id}','WarrantyController@active_warranty');
        Route::get('unactive-warranty/{id}','WarrantyController@unactive_warranty');
		// save_slider....................................
		Route::post('save-warranty','WarrantyController@save_warranty');
		// update_slider..................................
		Route::post('update-warranty/{id}','WarrantyController@update_warranty');
	});
	// authentication roles
	Route::group(['namespace' => 'author'],function(){
		Route::get('/register-author','AuthorController@register_author');
		
	})

});